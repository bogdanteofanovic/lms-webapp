import { AbstractEntity } from '../abstract-entity/abstract-entity';
import {SubjectExaminationDetails} from "../subject-examination-details/subject-examination-details";
import {FsFile} from "../file/fs-file/fs-file";

export class SubjectExaminationMaterial extends AbstractEntity{
  subjectExaminationDetails: SubjectExaminationDetails;
  fsFile: FsFile;

  constructor(){
    super();
    this.subjectExaminationDetails = null;
    this.fsFile = null;
  }

}
