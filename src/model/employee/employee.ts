import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { Address } from '../adress/address';
import { User } from '../user/user';
import { EmployeeType } from '../employee-type/employee-type';

export class Employee extends AbstractEntity{

    name: string;
    lastName: string;
    address: Address;
    user: User;
    employeeType: EmployeeType;

    constructor() {
        super();
        this.name = '';
        this.lastName = '';
        this.address = null;
        this.user = null;
        this.employeeType = null;
    }


}
