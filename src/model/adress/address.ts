import {AbstractEntity} from "../abstract-entity/abstract-entity";
import {City} from "../city/city";

export class Address extends AbstractEntity{

  name:string;
  number:string;
  city:City;

  constructor() {
    super();
    this.name = '';
    this.number = '';
    this.city = new City();
  }

}
