import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { Time } from '@angular/common';
import { Classroom } from '../classroom/classroom';
import { SubjectRealisation } from '../subject-realisation/subject-realisation';

export class ScheduleRealisation extends AbstractEntity{

    endTime: Time;
    startTime: Time;
    classroom: Classroom;
    subjectRealisation: SubjectRealisation;

    constructor(){
        super();
        this.endTime = null;
        this.startTime = null;
        this.classroom = null;
        this.subjectRealisation = null;
    }
}
