import {AbstractEntity} from "../abstract-entity/abstract-entity";

export class User extends AbstractEntity {

  username:string;
  password:string;
  email:string;


  constructor() {
    super();
    this.username = '';
    this.password = '';
    this.email = '';

  }
}
