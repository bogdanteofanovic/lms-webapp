import { AbstractEntity } from '../abstract-entity/abstract-entity';

export class StaffType extends AbstractEntity{

    name: string;

    constructor(){
        super();
        this.name = '';
    }
}
