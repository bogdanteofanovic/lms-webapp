import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { SubjectExaminationType } from '../subject-examination-type/subject-examination-type';
import { SubjectRealisation } from '../subject-realisation/subject-realisation';

export class SubjectExaminationDetails extends AbstractEntity{

    active: boolean;
    dateAndTime: Date;
    subjectExaminationType: SubjectExaminationType;
    subjectRealisation: SubjectRealisation;
    subjectExaminationMaterialsNum: number;

    constructor(){
        super();
        this.active = true;
        this.dateAndTime = null;
        this.subjectExaminationType = null;
        this.subjectRealisation = null;
    }

}
