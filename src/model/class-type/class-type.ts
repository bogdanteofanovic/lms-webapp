import { AbstractEntity } from '../abstract-entity/abstract-entity';

export class ClassType extends AbstractEntity{

    name: string;

    constructor(){
        super();
        this.name = '';
    }
}
