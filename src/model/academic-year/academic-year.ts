import { AbstractEntity } from '../abstract-entity/abstract-entity';

export class AcademicYear extends AbstractEntity{
    
    name: string;

    constructor(){
        super();
        this.name = '';
    }
}
