import {AbstractEntity} from '../abstract-entity/abstract-entity';
import {Country} from '../country/country';

export class City extends AbstractEntity{

  name:string;
  country:Country;
  zipCode:string;

  constructor() {
    super();
    this.name = '';
    this.country = null;
    this.zipCode = '';

  }

}
