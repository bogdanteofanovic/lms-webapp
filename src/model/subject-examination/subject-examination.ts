import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { Student } from '../student/student';
import { SubjectExaminationDetails } from '../subject-examination-details/subject-examination-details';

export class SubjectExamination extends AbstractEntity{

    score: number;
    student: Student;
    subjectExaminationDetails: SubjectExaminationDetails;

    constructor(){
        super();
        this.score = 0;
        this.student = null;
        this.subjectExaminationDetails = null;

    }
}
