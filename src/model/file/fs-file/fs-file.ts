import {AbstractFile} from '../abstract-file/abstract-file';

export class FsFile extends AbstractFile {
  filePath: string;
}
