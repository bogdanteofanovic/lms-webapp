import {AbstractEntity} from '../../abstract-entity/abstract-entity';
import {Employee} from '../../employee/employee';


export abstract class AbstractFile extends AbstractEntity {
  fileName: string;
  fileType: string;
  employee: Employee;
  createdDate: Date;
  lastModifiedDate: Date;
  data: string;
}
