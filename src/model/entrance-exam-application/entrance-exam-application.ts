import {AbstractEntity} from '../abstract-entity/abstract-entity';
import {EntranceExam} from '../entrance-exam/entrance-exam';
import {User} from '../user/user';


export class EntranceExamApplication extends AbstractEntity{

    user:User;
    dateOfApplication:string;
    entranceExam:EntranceExam;

    constructor() {
        super();
        this.user = new User();
        this.dateOfApplication = '';
        this.entranceExam = new EntranceExam();
  }
}
