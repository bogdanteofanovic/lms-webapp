import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { AcademicYear } from '../academic-year/academic-year';
import { Subject } from '../subject/subject';
import { SubjectRealisationType } from '../subject-realisation-type/subject-realisation-type';

export class SubjectRealisation extends AbstractEntity{

    academicYear: AcademicYear;
    subject: Subject;
    subjectRealisationType: SubjectRealisationType;
    subjectRealisationMaterialsNum: number;

    constructor(){
        super();
        this.academicYear = null;
        this.subject = null;
        this.subjectRealisationType = null;
    }
}
