import { AbstractEntity } from '../abstract-entity/abstract-entity';

export class EmployeeType extends AbstractEntity{

    name: string;

    constructor(){
        super();
        this.name = '';
    }
}
