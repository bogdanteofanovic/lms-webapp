import {AbstractEntity} from "../abstract-entity/abstract-entity";
import { StudyProgram } from '../study-program/study-program';

export class YearOfStudy extends AbstractEntity{

    name: string;
    studyProgram: StudyProgram;

    constructor() {
        super();
    
        this.name = '';
        this.studyProgram = new StudyProgram();
    
      }
}
