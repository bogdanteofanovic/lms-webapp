import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { Address } from '../adress/address';
import { University } from '../university/university';

export class Faculty extends AbstractEntity{

    name:string;
    description: string;
    email:string;
    phoneNumber:string;
    address: Address;
    university: University;

    constructor() {
      super();
      this.name = '';
      this.description = '';
      this.phoneNumber = '';
      this.email = '';
      this.university = null;
      this.address = null;

    }
}
