import { AbstractEntity } from '../abstract-entity/abstract-entity';

export class Role extends AbstractEntity{

    name: string;

    constructor(){
        super();
        this.name = '';
    }
}
