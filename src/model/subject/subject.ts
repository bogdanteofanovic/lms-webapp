import {AbstractEntity} from "../abstract-entity/abstract-entity";
import { YearOfStudy } from '../year-of-study/year-of-study';


export class Subject extends AbstractEntity{

  name:string;
  syllabus: string;
  score:number;
  yearOfStudy:YearOfStudy;

  constructor() {
    super();

    this.name = '';
    this.syllabus = '';
    this.score = 0;
    this.yearOfStudy = null;
  }

}