import { AbstractEntity } from '../abstract-entity/abstract-entity';
import {SubjectRealisation} from "../subject-realisation/subject-realisation";
import {FsFile} from "../file/fs-file/fs-file";

export class SubjectRealisationMaterial extends AbstractEntity{
  subjectRealisation: SubjectRealisation;
  fsFile: FsFile;

  constructor(){
    super();
    this.subjectRealisation = null;
    this.fsFile = null;
  }
}
