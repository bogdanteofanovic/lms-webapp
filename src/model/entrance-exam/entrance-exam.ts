import {AbstractEntity} from '../abstract-entity/abstract-entity';
import {AcademicYear} from '../academic-year/academic-year';
import {StudyProgram} from '../study-program/study-program';


export class EntranceExam extends AbstractEntity{

    academicYear:AcademicYear;
    startDate:string;
    endDate:string;
    studyProgram:StudyProgram;

    constructor() {
        super();
        this.academicYear = null;
        this.startDate = '';
        this.endDate = '';
        this.studyProgram = null;
  }
}
