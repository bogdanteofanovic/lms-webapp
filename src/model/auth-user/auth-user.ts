import {AbstractEntity} from "../abstract-entity/abstract-entity";

export class AuthUser{

  username:string;
  password:string;

  constructor() {
    this.username = '';
    this.password = '';
  }

}
