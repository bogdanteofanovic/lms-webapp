import { Address } from '../adress/address';
import { AbstractEntity } from '../abstract-entity/abstract-entity';

export class University extends AbstractEntity{

    name:string;
    address:Address;
    description:string;
    phoneNumber:string;

    constructor() {
      super();
      this.name = '';
      this.description = '';
      this.phoneNumber = '';
      this.address = null; //CHANGE THIS TO EXISTING ADDRESS
    }
}
