import { ClassType } from '../class-type/class-type';
import { Employee } from '../employee/employee';
import { SubjectRealisation } from '../subject-realisation/subject-realisation';
import { AbstractEntity } from '../abstract-entity/abstract-entity';

export class ProfessorOnRealisation extends AbstractEntity{

    classType: ClassType;
    employee:Employee;
    subjectRealisation: SubjectRealisation;

    constructor(){
        super();
        this.classType = null;
        this.employee = null;
        this.subjectRealisation = null;
    }
}
