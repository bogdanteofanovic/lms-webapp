import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { EnrollmentType } from '../enrollment-type/enrollment-type';
import { Student } from '../student/student';
import { SubjectRealisation } from '../subject-realisation/subject-realisation';

export class EnrolledSubject extends AbstractEntity{

    enrollmentType: EnrollmentType;
    student: Student;
    subjectRealisation: SubjectRealisation;

    constructor(){
        super();
        this.enrollmentType = null;
        this.student = null;
        this.subjectRealisation = null;
    }
}
