import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { Employee } from '../employee/employee';
import { SubjectRealisation } from '../subject-realisation/subject-realisation';

export class SubjectRealisationNotification extends AbstractEntity{

    notification: string;
    publishDate: Date;
    publisher: Employee;
    subjectRealisation: SubjectRealisation;

    constructor(){
        super();
        this.notification = '';
        this.publishDate = null;
        this.publisher = null;
        this.subjectRealisation = null;
    }
}
