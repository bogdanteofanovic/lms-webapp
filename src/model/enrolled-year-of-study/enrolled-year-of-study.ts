import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { AcademicYear } from '../academic-year/academic-year';
import { EnrollmentYearType } from '../enrollment-year-type/enrollment-year-type';
import { Student } from '../student/student';
import { YearOfStudy } from '../year-of-study/year-of-study';

export class EnrolledYearOfStudy extends AbstractEntity{

    dateOfEnrollment: Date;
    academicYear: AcademicYear;
    enrollmentYearType: EnrollmentYearType;
    student: Student;
    yearOfStudy: YearOfStudy;

    constructor() {
        super();
        this.dateOfEnrollment = null;
        this.academicYear = null;
        this.enrollmentYearType = null;
        this.student = null;
        this.yearOfStudy = null;
    
      }
}
