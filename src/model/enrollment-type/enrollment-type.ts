import { AbstractEntity } from '../abstract-entity/abstract-entity';

export class EnrollmentType extends AbstractEntity{

    name: string;

    constructor() {
        super();
        this.name = '';
      }
}
