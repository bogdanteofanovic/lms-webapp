import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { ScheduleExamination } from '../schedule-examination/schedule-examination';
import { Student } from '../student/student';

export class ExaminationRegistration extends AbstractEntity{

    dateOfRegistration: Date;
    scheduleExamination: ScheduleExamination;
    student: Student;

    constructor() {
        super();
        this.dateOfRegistration = null;
        this.scheduleExamination = null;
        this.student = null;
      }
}
