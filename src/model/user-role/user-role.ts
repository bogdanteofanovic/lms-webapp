import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { Role } from '../role/role';
import { User } from '../user/user';

export class UserRole extends AbstractEntity {
    role: Role;
    user: User;

    constructor(){
        super();
        this.role = null;
        this.user = null;
    }
}
