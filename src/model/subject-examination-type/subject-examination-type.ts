import { AbstractEntity } from '../abstract-entity/abstract-entity';

export class SubjectExaminationType extends AbstractEntity{

    name: string;

    constructor(){
        super();
        this.name = '';
    }
}
