import {AbstractEntity} from "../abstract-entity/abstract-entity";
import {Address} from "../adress/address";
import {User} from "../user/user";

export class Student extends AbstractEntity{

  name:string;
  lastName:string;
  indexNumber:number;
  address:Address;
  user:User;

  constructor() {
    super();

    this.name = '';
    this.lastName = '';
    this.indexNumber = 0;
    this.address = new Address();
    this.user = new User();

  }

}
