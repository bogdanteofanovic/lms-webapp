import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { ClassroomType } from '../classroom-type/classroom-type';
import { Faculty } from '../faculty/faculty';

export class Classroom extends AbstractEntity{

    name: string;
    seats: number;
    classroomType: ClassroomType;
    faculty: Faculty;

    constructor() {
        super();
        this.name = '';
        this.seats = 0;
        this.classroomType = null;
        this.faculty = null;
    
      }
}
