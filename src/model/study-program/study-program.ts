import {AbstractEntity} from "../abstract-entity/abstract-entity";
import { Faculty } from '../faculty/faculty';
import { Employee } from '../employee/employee';


export class StudyProgram extends AbstractEntity{

  name:string;
  faculty: Faculty;
  description: string;
  studyProgramDirector: Employee;

  constructor() {
    super();

    this.name = '';
    this.faculty = null;
    this.description = '';
    this.studyProgramDirector = null;

  }

}
