import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { Employee } from '../employee/employee';
import { StaffType } from '../staff-type/staff-type';

export class Staff extends AbstractEntity{

    active: boolean;
    employmentEnd: Date;
    employmentStart: Date;
    employee: Employee;
    staffType: StaffType;

    constructor(){
        super();
        this.active = true;
        this.employmentEnd = null;
        this.employmentStart = null;
        this.employee = null;
        this.staffType = null;
    }
}
