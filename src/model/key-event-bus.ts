export interface KeyEventBus {

  onKeyEvent(event:KeyboardEvent);

}
