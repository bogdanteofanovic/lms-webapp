import { AbstractEntity } from '../abstract-entity/abstract-entity';
import { Time } from '@angular/common';
import { SubjectExaminationDetails } from '../subject-examination-details/subject-examination-details';

export class ScheduleExamination extends AbstractEntity{

    beginTime: Time;
    endTime: Time;
    subjectExaminationDetails: SubjectExaminationDetails;


    constructor() {
        super();
        this.beginTime = null;
        this.endTime = null;
        this.subjectExaminationDetails = null;
      }
}
