import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {AbstractEntity} from '../../../model/abstract-entity/abstract-entity';
import {ColumnDef} from '../../../model/column-def';
import {MatDialog} from "@angular/material/dialog";
import {MultiExportComponent} from "../multi-export/multi-export.component";
import {Template} from "@angular/compiler/src/render3/r3_ast";
import {AuthenticationService} from '../../../service/security/authentication.service';

@Component({
  selector: 'app-generic-table',
  templateUrl: './generic-table.component.html',
  styleUrls: ['./generic-table.component.css']
})
export class GenericTableComponent implements OnInit {


  selected : AbstractEntity;

  @Input()
  columnDefinitions: ColumnDef[];

  displayedColumns: string [] = [];

  @Input()
  data : MatTableDataSource<AbstractEntity>;

  @Output()
  emitSelect:EventEmitter<AbstractEntity> = new EventEmitter<AbstractEntity>();

  @Output()
  emitEdit:EventEmitter<AbstractEntity> = new EventEmitter<AbstractEntity>();

  @Output()
  emitDelete:EventEmitter<AbstractEntity> = new EventEmitter<AbstractEntity>();

  @Output()
  emitRestore:EventEmitter<AbstractEntity> = new EventEmitter<AbstractEntity>();

  @Output()
  emitNew:EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Input()
  enableNew:boolean = false;

  @Input()
  enableEdit:boolean = false;

  @Input()
  enableDelete:boolean = false;

  @Input()
  enableExport:boolean = false;

  @Input()
  enableRestore:boolean;

  activateRestore:boolean = true;

  constructor(private matDialog:MatDialog, private authenticationService:AuthenticationService) {

  }

  ngOnInit(): void {
    if(this.authenticationService.isAdmin()){
      this.enableRestore = true;
    }
    this.data.sortingDataAccessor = (obj, property) => this.getProperty(obj, property);

    this.data.sort = this.sort;
    this.data.filterPredicate = (data: any, filter) => {
      const dataStr =JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) !== -1;
    }

    this.columnDefinitions.forEach(e=>this.displayedColumns.push(e.name));
  }

  getProperty = (obj, path) => (
    path.split('.').reduce((o, p) => o && o[p], obj)
  )

  searchFilter(event:Event){

    const filterValue = (event.target as HTMLInputElement).value;
    this.data.filter = filterValue.trim().toLowerCase();

  }

  selectedRowIndex: number = -1;

  highlight(row){
    this.selectedRowIndex = row.id;
  }


  selectElem(elem){
    this.selected = elem;
    if(this.selected.deleted){
      this.activateRestore = false;
    }else{
      this.activateRestore = true;
    }
    // this.emitSelect.emit(elem);
  }

  editSelected(){
    this.emitEdit.emit(this.selected);
    this.selected = null;
  }

  deleteSelected(elem){
    this.emitDelete.emit(elem);
    this.selected = null;
  }

  restoreSelected(elem){
    this.emitRestore.emit(elem);
    this.selected = null;
  }

  export(){
    this.matDialog.open(MultiExportComponent,{data:{entities:this.data.filteredData}})
  }



}
