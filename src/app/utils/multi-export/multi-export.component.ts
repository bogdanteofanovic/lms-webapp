import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ConfirmationMessage} from "../confirmation-message";
import {AbstractEntity} from "../../../model/abstract-entity/abstract-entity";
import * as JsonToXML from "js2xmlparser";
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import * as _ from 'lodash';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import {MatSnackBar} from "@angular/material/snack-bar";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

const XML_TYPE = 'text/xml';
const XML_EXTENSION = '.xml';

@Component({
  selector: 'app-multi-export',
  templateUrl: './multi-export.component.html',
  styleUrls: ['./multi-export.component.css']
})
export class MultiExportComponent implements OnInit {




  dialogMessage:string;
  dialogType:string;
  type:string;
  entities : AbstractEntity [];
  data;


  constructor(private matDialogRef: MatDialogRef<MultiExportComponent>, @Optional() @Inject(MAT_DIALOG_DATA) data: any, private snackBar:MatSnackBar) {
    this.data = data;
    this.dialogType = data.type;
    this.dialogMessage = data.message;

  }

  ngOnInit(): void {

    if(this.data){
      if(this.data.entities){
        this.entities = this.data.entities;
      }
    }

  }

  exportToXML(){
    this.saveFile(JsonToXML.parse("root",{entity:this.entities}), 'LMS-XML', XML_TYPE, XML_EXTENSION);
  }

  exportToXLS(){
    let ent = this.flatNestedJSON(this.entities);
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(ent);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveFile(excelBuffer, 'excelFileName', EXCEL_TYPE, EXCEL_EXTENSION);
  }

  saveFile(buffer: any, fileName: string, type:any, extension:any): void {
    const data: Blob = new Blob([buffer], {type: type});
    try{
      FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + extension);
    }catch (e) {
      this.snackBar.open(e);
    }

    this.matDialogRef.close();

  }

  flatNestedJSON(json){
    let cloned = _.cloneDeep(json);
    for(let obj of cloned){
      for(let attr of Object.keys(obj)){
        if(obj[attr] instanceof Object){
          if(obj[attr]['name']){
            obj[attr] = obj[attr]['name']
          }else{
            obj[attr] = obj[attr]['id'];
          }
        }
      }
    }
    return cloned;
  }

  exportPDF(){


    let docDefinition = {
      header: [
        {text:'LMS PDF Report',
        }
      ]

      ,
      content: [
        {

          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [],
            body: [

            ]
          }

        }
      ],
      footer: [
        {text: 'LMS 2020.'}
      ],
      styles: {
        head: {
          fontSize: 12,
          bold: true,
          margin: [0, 0, 0, 0]
        },
        table:{
          fontSize:9,
          bold:false,
          margin: [-10, 0, 0, 0],
        }
      }
    };

    let flatedData = this.flatNestedJSON(this.entities);
    let headers = Object.keys(flatedData[0]);

    headers.forEach(()=>docDefinition.content[0].table.widths.push('*'));

    docDefinition.content[0].table.body.push(headers);

    for(let data of flatedData){
      let dataValues = [];
      for(let value of Object.keys(data)){
        dataValues.push(data[value]);
      }
      docDefinition.content[0].table.body.push(dataValues);
    }
    try{
      pdfMake.createPdf(docDefinition).download();
    }catch (e) {
      this.snackBar.open(e);
    }

    this.matDialogRef.close();

  }


}
