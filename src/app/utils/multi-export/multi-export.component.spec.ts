import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiExportComponent } from './multi-export.component';

describe('MultiExportComponent', () => {
  let component: MultiExportComponent;
  let fixture: ComponentFixture<MultiExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiExportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
