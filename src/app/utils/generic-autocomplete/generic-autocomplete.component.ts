import { Component, OnInit, Input } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith, filter} from 'rxjs/operators';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import {ComponentType} from "@angular/cdk/overlay";
import {GenericWindow} from "../../../view/generic-window";
import {AbstractEntity} from "../../../model/abstract-entity/abstract-entity";
import {GenericService} from "../../../service/generic-service/generic-service";
import {MatDialog} from "@angular/material/dialog";
import {StudyProgramWindowComponent} from '../../user/admin_portal/component/window/study-program-window/study-program-window.component';

@Component({
  selector: 'app-generic-autocomplete',
  templateUrl: './generic-autocomplete.component.html',
  styleUrls: ['./generic-autocomplete.component.css']
})
export class GenericAutocompleteComponent implements OnInit {

  private entityList: any[];
  @Input() specificServiceMethod:string;
  @Input() genericService: GenericService<any>;
  @Input() object : AbstractEntity;
  @Input() objectName: string;
  @Input() filterByAttribute: string;
  @Input() formGroup : FormGroup;

  @Input() enableAdd : boolean = true;
  @Input() disableEdit : boolean = false;

  @Input() newObjectWindow : ComponentType<GenericWindow<any, any>>

  filteredOptions: Observable<any[]>;
  formName:string;
  constructor(private matDialog:MatDialog) {
  }

  ngOnInit() {



    this.formName = this.objectName.charAt(0).toLowerCase() + this.objectName.slice(1);


    this.getObjects(this.object);
    if(this.disableEdit){
      this.formGroup.get(this.formName).disable();
    }

  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.entityList.filter(option => option[this.filterByAttribute].toLowerCase().indexOf(filterValue) === 0);
  }

  public getDisplayFn() {
    return (val) => this.display(val);
  }

  private display(object): string {
    return object && object[this.filterByAttribute] ? object[this.filterByAttribute] : '';
  }

  setObject(object){
    this.object = object;
  }

  addObject(){
    this.matDialog.open(this.newObjectWindow).afterClosed().subscribe(e=>e!=null?this.getObjects(e.data):'');
  }

  editObject(){
    this.matDialog.open(this.newObjectWindow,{data:this.object}).afterClosed().subscribe(e=>e!=null?this.getObjects(e.data):'');
  }

  getObjects(object:AbstractEntity){

    if(object && object.id != 0){
      if(this.specificServiceMethod){
        this.executeFunctionByName(this.specificServiceMethod, this.genericService).subscribe(entities => this.setObjects(entities));
      }else{
        this.genericService.getAll().subscribe(entities => this.setObjects(entities));
      }
      this.object = object;
      this.formGroup.get(this.formName).setValue(this.object);
    }else{
      if(this.specificServiceMethod){
        this.executeFunctionByName(this.specificServiceMethod, this.genericService).subscribe(entities => this.setObjects(entities));
      }else{
        this.genericService.getAll().subscribe(entities => this.setObjects(entities));
      }
    }
  }

  setObjects(objects){
    this.entityList = objects;

    this.filteredOptions = this.formGroup.get(this.formName).valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter(name) : this.entityList.slice())
      );
  }


  executeFunctionByName(functionName, context /*, args */) {
    let args = Array.prototype.slice.call(arguments, 2);
    let namespaces = functionName.split(".");
    let func = namespaces.pop();
    for (let i = 0; i < namespaces.length; i++) {
      context = context[namespaces[i]];
    }
    return context[func].apply(context, args);
  }

}
