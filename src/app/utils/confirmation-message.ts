export interface ConfirmationMessage {
  type:string;
  message:string;
}
