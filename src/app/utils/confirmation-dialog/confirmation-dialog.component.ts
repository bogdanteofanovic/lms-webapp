import {Component, Inject, Input, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../view/generic-window';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ConfirmationMessage} from '../confirmation-message';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent implements OnInit {


  dialogMessage:string;
  dialogType:string;
  type:string;


  constructor(private matDialogRef: MatDialogRef<ConfirmationDialogComponent>, @Optional() @Inject(MAT_DIALOG_DATA) data: ConfirmationMessage) {
    this.dialogType = data.type;
    this.dialogMessage = data.message;

  }

  ngOnInit(): void {

  }

  okButton(){
    this.matDialogRef.close({event:"close",data:"ok"});
  }

  cancelButton(){
    this.matDialogRef.close({event:"close",data:"nok"});
  }

}
