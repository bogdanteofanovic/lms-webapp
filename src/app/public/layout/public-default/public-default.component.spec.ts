import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicDefaultComponent } from './public-default.component';

describe('PublicDefaultComponent', () => {
  let component: PublicDefaultComponent;
  let fixture: ComponentFixture<PublicDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
