import {Component, OnInit, Optional} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {AuthenticationService} from '../../../../service/security/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  entity;
  formGroup: FormGroup;
  titleAlert = 'Field is required';
  noMatch = 'Repeated password must match password!';
  dialogRef: MatDialogRef<RegisterComponent>;

  constructor(private authenticationService: AuthenticationService, @Optional() matDialogRef: MatDialogRef<RegisterComponent>, private formBuilder: FormBuilder ) {

    this.dialogRef = matDialogRef;

  }

  ngOnInit(): void {
    this.entity = {username: '', password: '', email: ''};
    this.createForm();

  }

  register() {
    this.bindFormFields();
    if (this.entity.password === this.formGroup.get('repeatedPassword').value) {
      this.authenticationService.userRegistration(this.entity.username, this.entity.password, this.entity.email);
    } else {
      this.formGroup.controls.repeatedPassword.reset();
      this.formGroup.controls.repeatedPassword.markAsTouched();
    }

  }

  bindFormFields(): void {

    this.entity.username = this.formGroup.get('username').value;
    this.entity.password = this.formGroup.get('password').value;
    this.entity.email = this.formGroup.get('email').value;

  }

  createForm(): void {

    this.formGroup = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
      repeatedPassword: [null, Validators.required],
      email: [null, Validators.required]
    });

  }

}
