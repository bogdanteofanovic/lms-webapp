import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MenuItem} from '../../../utils/menu/MenuItem';

@Component({
  selector: 'app-faculty',
  templateUrl: './faculty.component.html',
  styleUrls: ['./faculty.component.css']
})
export class FacultyComponent implements OnInit {
  menuItems: MenuItem[] = [{name: 'Home', route: '/home'}];


  constructor(private router: Router) { }

  ngOnInit(): void {
    console.log(this.router.url);
    if (this.router.url === '/faculty/noviSad') {
      this.menuItems.push({name: 'Programs', route: '', subMenu: [{name: 'Business Economics', route: ''}, {name: 'Software and date engineering', route: ''}, {name: 'IT', route: ''}]}, {name: 'About Faculty NS', route: ''});
    }
    else if (this.router.url === '/faculty/beograd') {
      this.menuItems.push({name: 'Programs', route: '', subMenu: [{name: 'Anglistics', route: ''}, {name: 'Tourism', route: ''}]}, {name: 'About Faculty Beograd', route: ''});
    }
  }

}
