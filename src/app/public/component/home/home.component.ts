import { Component, OnInit } from '@angular/core';
import {MenuItem} from '../../../utils/menu/MenuItem';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  menuItems: MenuItem[] = [{name: 'Home', route: '/home'}, {name: 'Faculties', route: '', subMenu: [{name: 'Novi Sad', route: '/faculty/noviSad'}, {name: 'Beograd', route: '/faculty/beograd'}]}, {name: 'Gallery', route: '/subject'}, {name: 'About', route: '/about'}];

  constructor() { }

  ngOnInit(): void {
  }

}
