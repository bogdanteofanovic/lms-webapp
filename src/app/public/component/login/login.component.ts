import {Component, OnInit, Optional} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthUser} from '../../../../model/auth-user/auth-user';
import {AuthenticationService} from '../../../../service/security/authentication.service';

export interface UserLogin {
  username: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  entity: UserLogin;
  formGroup: FormGroup;
  titleAlert = 'Field is required';
  dialogRef: MatDialogRef<LoginComponent>;
  router: ActivatedRoute;


  constructor(private authenticationService: AuthenticationService, @Optional() matDialogRef: MatDialogRef<LoginComponent>,
              private formBuilder: FormBuilder ) {

    this.dialogRef = matDialogRef;

  }

  ngOnInit(): void {

    this.entity = new AuthUser();
    this.authenticationService.setDialogRef(this.dialogRef);
    this.createForm();

  }


  login() {

    this.bindFormFields();
    this.authenticationService.userAuthentication(this.entity.username, this.entity.password);

  }

  bindFormFields(): void {

    this.entity.username = this.formGroup.get('username').value;
    this.entity.password = this.formGroup.get('password').value;

  }

  createForm(): void {

    this.formGroup = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });

  }

}
