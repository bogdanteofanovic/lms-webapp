import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DbFileComponent } from './db-file.component';

describe('DbFileComponent', () => {
  let component: DbFileComponent;
  let fixture: ComponentFixture<DbFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DbFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DbFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
