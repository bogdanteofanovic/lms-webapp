import { Component } from '@angular/core';
import {File} from '../File';
import {DbFile} from '../../../../../model/file/db-file/db-file';
import {DbFileService} from '../../../../../service/file/db-file/db-file.service';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-db-file',
  templateUrl: './db-file.component.html',
  styleUrls: ['./db-file.component.css']
})
export class DbFileComponent extends File<DbFile, DbFileService> {

  constructor(dbFileService: DbFileService, fb: FormBuilder) {
    super(dbFileService, fb);
  }

}
