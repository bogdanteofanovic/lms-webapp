import * as fileSaver from 'file-saver';
import {OnInit} from '@angular/core';
import {AbstractFile} from '../../../../model/file/abstract-file/abstract-file';
import {Employee} from '../../../../model/employee/employee';
import {FormBuilder, FormGroup} from '@angular/forms';
import {GenericService} from '../../../../service/generic-service/generic-service';

export abstract class File<T extends AbstractFile, V extends GenericService<T>> implements OnInit {
  formGroup: FormGroup;
  fb: FormBuilder;
  file: T;
  service: V;
  employee: Employee;

  multipartFile;

  constructor(service: V, fb: FormBuilder) {
    this.service = service;
    this.fb = fb;
  }

  ngOnInit(): void {
    this.service.getOne(1).subscribe(e => this.file = e);
  }


  saveFileToDisk() {
    const binaryString = atob(this.file.data);
    const binaryLen = binaryString.length;
    const bytes = new Uint8Array(binaryLen);

    for (let i = 0; i < binaryLen; i++) {
      const ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }

    const blob = new Blob([bytes], {type: this.file.fileType});
    fileSaver(blob, this.file.fileName);
  }

  fileChange(event: any) {
    if (event.target.files && event.target.files.length > 0) {
      this.multipartFile = event.target.files[0];
      console.log(this.multipartFile);
    }
  }

  upload() {
    const body = new FormData();
    body.append('file', this.multipartFile);
    this.service.addOne(body).subscribe(e => console.log(e));
  }


}
