import { Component } from '@angular/core';
import {File} from '../File';
import {FsFile} from '../../../../../model/file/fs-file/fs-file';
import {FsFileService} from '../../../../../service/file/fs-file/fs-file.service';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-fs-file',
  templateUrl: './fs-file.component.html',
  styleUrls: ['./fs-file.component.css']
})
export class FsFileComponent extends File<FsFile, FsFileService> {

  constructor(fsFileService: FsFileService, fb: FormBuilder) {
    super(fsFileService, fb);
  }

}
