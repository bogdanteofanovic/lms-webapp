import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FsFileComponent } from './fs-file.component';

describe('FsFileComponent', () => {
  let component: FsFileComponent;
  let fixture: ComponentFixture<FsFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FsFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FsFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
