import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import VectorLayer from 'ol/layer/Vector';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import OSM from 'ol/source/OSM';
import * as olProj from 'ol/proj';
import TileLayer from 'ol/layer/Tile';
import {MenuItem} from '../../../utils/menu/MenuItem';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  menuItems: MenuItem[] = [{name: 'Home', route: '/home'}, {name: 'Faculties', route: '', subMenu: [{name: 'Novi Sad', route: '/faculty/noviSad'}, {name: 'Beograd', route: '/faculty/beograd'}]}, {name: 'Gallery', route: '/subject'}, {name: 'About', route: '/about'}];

  @ViewChild('map') mapElement: ElementRef;
  map: Map;

  constructor() { }

  ngOnInit(): void {
    this.map = new Map({
      layers: [
        new TileLayer({
          source: new OSM()
        })
      ],
      view: new View({
        center: olProj.fromLonLat([19.833549, 45.267136]),
        zoom: 10
      })
    });
  }

  ngAfterViewInit() {
    this.map.setTarget(this.mapElement.nativeElement);
  }

}
