import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MenuComponent } from './utils/menu/menu.component';
import { HomeComponent } from './public/component/home/home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { PageNotFoundComponent } from './utils/page-not-found/page-not-found.component';
import { LoginComponent } from './public/component/login/login.component';
import {CityViewComponent} from './user/admin_portal/component/view/city-view/city-view.component';
import {CityWindowComponent} from './user/admin_portal/component/window/city-window/city-window.component';
import {CountryViewComponent} from './user/admin_portal/component/view/country-view/country-view.component';
import {CountryWindowComponent} from './user/admin_portal/component/window/country-window/country-window.component';
import {GenericTableComponent} from './utils/generic-table/generic-table.component';
import {ConfirmationDialogComponent} from './utils/confirmation-dialog/confirmation-dialog.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MaterialDesignModule} from '../module/material-design.module';
import { HeaderComponent } from './user/admin_portal/layout/header/header.component';
import { FooterComponent } from './user/admin_portal/layout/footer/footer.component';
import { SidebarComponent } from './user/admin_portal/layout/sidebar/sidebar.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import { PublicDefaultComponent } from './public/layout/public-default/public-default.component';
import { PublicHeaderComponent } from './public/layout/public-header/public-header.component';
import { PublicFooterComponent } from './public/layout/public-footer/public-footer.component';
import { PublicSidebarComponent } from './public/layout/public-sidebar/public-sidebar.component';
import { AdminDefaultComponent } from './user/admin_portal/layout/admin-default/admin-default.component';
import { AddressViewComponent } from './user/admin_portal/component/view/address-view/address-view.component';
import { AddressWindowComponent } from './user/admin_portal/component/window/address-window/address-window.component';
import { MultiExportComponent } from './utils/multi-export/multi-export.component';
import { ProfessorDefaultComponent } from './user/professor_portal/layout/professor-default/professor-default.component';
import { ProfessorFooterComponent } from './user/professor_portal/layout/professor-footer/professor-footer.component';
import { ProfessorHeaderComponent } from './user/professor_portal/layout/professor-header/professor-header.component';
import { ProfessorSidebarComponent } from './user/professor_portal/layout/professor-sidebar/professor-sidebar.component';
import { SudentDefaultComponent } from './user/student_portal/layout/sudent-default/sudent-default.component';
import { SudentFooterComponent } from './user/student_portal/layout/sudent-footer/sudent-footer.component';
import { SudentHeaderComponent } from './user/student_portal/layout/sudent-header/sudent-header.component';
import { SudentSidebarComponent } from './user/student_portal/layout/sudent-sidebar/sudent-sidebar.component';
import {TokenInterceptor} from '../service/security/token-interceptor';
import { RegisterComponent } from './public/component/register/register.component';
import { StudentViewComponent } from './user/admin_portal/component/view/student-view/student-view.component';
import { StudentWindowComponent } from './user/admin_portal/component/window/student-window/student-window.component';
import {AboutComponent} from './public/component/about/about.component';
import { FacultyComponent } from './public/component/faculty/faculty.component';
import { UniversityViewComponent } from './user/admin_portal/component/view/university-view/university-view.component';
import { FacultyWindowComponent } from './user/admin_portal/component/window/faculty-window/faculty-window.component';
import { UniversityWindowComponent } from './user/admin_portal/component/window/university-window/university-window.component';
import { FacutlyViewComponent } from './user/admin_portal/component/view/faculty-view/facutly-view.component';
import { EmployeeViewComponent } from './user/admin_portal/component/view/employee-view/employee-view.component';
import { EmployeeWindowComponent } from './user/admin_portal/component/window/employee-window/employee-window.component';
import { EmployeeTypeViewComponent } from './user/admin_portal/component/view/employee-type-view/employee-type-view.component';
import { UserViewComponent } from './user/admin_portal/component/view/user-view/user-view.component';
import { EmployeeTypeWindowComponent } from './user/admin_portal/component/window/employee-type-window/employee-type-window.component';
import { UserWindowComponent } from './user/admin_portal/component/window/user-window/user-window.component';
import { GenericAutocompleteComponent } from './utils/generic-autocomplete/generic-autocomplete.component';
import { StudyProgramViewComponent } from './user/admin_portal/component/view/study-program-view/study-program-view.component';
import { StudyProgramWindowComponent } from './user/admin_portal/component/window/study-program-window/study-program-window.component';
import { YearOfStudyViewComponent } from './user/admin_portal/component/view/year-of-study-view/year-of-study-view.component';
import { YearOfStudyWindowComponent } from './user/admin_portal/component/window/year-of-study-window/year-of-study-window.component';
import { SubjectWindowComponent } from './user/admin_portal/component/window/subject-window/subject-window.component';
import { SubjectViewComponent } from './user/admin_portal/component/view/subject-view/subject-view.component';
import { UserRoleViewComponent } from './user/admin_portal/component/view/user-role-view/user-role-view.component';
import { SubjectRealisationViewComponent } from './user/admin_portal/component/view/subject-realisation-view/subject-realisation-view.component';
import { SubjectExaminationViewComponent } from './user/admin_portal/component/view/subject-examination-view/subject-examination-view.component';
import { RoleViewComponent } from './user/admin_portal/component/view/role-view/role-view.component';
import { ProfessorOnRealisationViewComponent } from './user/admin_portal/component/view/professor-on-realisation-view/professor-on-realisation-view.component';
import { EnrolledSubjectViewComponent } from './user/admin_portal/component/view/enrolled-subject-view/enrolled-subject-view.component';
import { ClassTypeViewComponent } from './user/admin_portal/component/view/class-type-view/class-type-view.component';
import { UserRoleWindowComponent } from './user/admin_portal/component/window/user-role-window/user-role-window.component';
import { SubjectRealisationWindowComponent } from './user/admin_portal/component/window/subject-realisation-window/subject-realisation-window.component';
import { SubjectExaminationWindowComponent } from './user/admin_portal/component/window/subject-examination-window/subject-examination-window.component';
import { RoleWindowComponent } from './user/admin_portal/component/window/role-window/role-window.component';
import { ProfessorOnRealisationWindowComponent } from './user/admin_portal/component/window/professor-on-realisation-window/professor-on-realisation-window.component';
import { EnrolledSubjectWindowComponent } from './user/admin_portal/component/window/enrolled-subject-window/enrolled-subject-window.component';
import { ClassTypeWindowComponent } from './user/admin_portal/component/window/class-type-window/class-type-window.component';
import { AcademicYearWindowComponent } from './user/admin_portal/component/window/academic-year-window/academic-year-window.component';
import { AcademicYearViewComponent } from './user/admin_portal/component/view/academic-year-view/academic-year-view.component';
import { EnrollmentTypeViewComponent } from './user/admin_portal/component/view/enrollment-type-view/enrollment-type-view.component';
import { EnrollmentWindowComponent } from './user/admin_portal/component/window/enrollment-window/enrollment-window.component';
import { EnrollmentTypeWindowComponent } from './user/admin_portal/component/window/enrollment-type-window/enrollment-type-window.component';
import {DbFileComponent} from "./public/component/file/db-file/db-file.component";
import {FsFileComponent} from "./public/component/file/fs-file/fs-file.component";
import { SubjectRealisationMaterialViewComponent } from './user/admin_portal/component/view/subject-realisation-material-view/subject-realisation-material-view.component';
import { AdminDashboardComponent } from './user/admin_portal/component/utils/admin-dashboard/admin-dashboard.component';
import { ChartLineComponent } from './user/admin_portal/component/utils/chart-line/chart-line.component';
import { ClassroomWindowComponent } from './user/admin_portal/component/window/classroom-window/classroom-window.component';
import { ClassroomTypeWindowComponent } from './user/admin_portal/component/window/classroom-type-window/classroom-type-window.component';
import { EnrolledYearOfStudyWindowComponent } from './user/admin_portal/component/window/enrolled-year-of-study-window/enrolled-year-of-study-window.component';
import { EnrollmentYearTypeWindowComponent } from './user/admin_portal/component/window/enrollment-year-type-window/enrollment-year-type-window.component';
import { ExaminationRegistrationWindowComponent } from './user/admin_portal/component/window/examination-registration-window/examination-registration-window.component';
import { ScheduleExaminationWindowComponent } from './user/admin_portal/component/window/schedule-examination-window/schedule-examination-window.component';
import { SubjectExaminationDetailsWindowComponent } from './user/admin_portal/component/window/subject-examination-details-window/subject-examination-details-window.component';
import { SubjectExaminationMaterialWindowComponent } from './user/admin_portal/component/window/subject-examination-material-window/subject-examination-material-window.component';
import { SubjectExaminationTypeWindowComponent } from './user/admin_portal/component/window/subject-examination-type-window/subject-examination-type-window.component';
import { ScheduleRealisationWindowComponent } from './user/admin_portal/component/window/schedule-realisation-window/schedule-realisation-window.component';
import { SubjectRealisationMaterialWindowComponent } from './user/admin_portal/component/window/subject-realisation-material-window/subject-realisation-material-window.component';
import { SubjectRealisationTypeWindowComponent } from './user/admin_portal/component/window/subject-realisation-type-window/subject-realisation-type-window.component';
import { SubjectRealisationNotificationWindowComponent } from './user/admin_portal/component/window/subject-realisation-notification-window/subject-realisation-notification-window.component';
import { StaffWindowComponent } from './user/admin_portal/component/window/staff-window/staff-window.component';
import { StaffTypeWindowComponent } from './user/admin_portal/component/window/staff-type-window/staff-type-window.component';
import { ClassroomViewComponent } from './user/admin_portal/component/view/classroom-view/classroom-view.component';
import { ClassroomTypeViewComponent } from './user/admin_portal/component/view/classroom-type-view/classroom-type-view.component';
import { EnrolledYearOfStudyViewComponent } from './user/admin_portal/component/view/enrolled-year-of-study-view/enrolled-year-of-study-view.component';
import { EnrollmentYearTypeViewComponent } from './user/admin_portal/component/view/enrollment-year-type-view/enrollment-year-type-view.component';
import { ExaminationRegistrationViewComponent } from './user/admin_portal/component/view/examination-registration-view/examination-registration-view.component';
import { ScheduleExaminationViewComponent } from './user/admin_portal/component/view/schedule-examination-view/schedule-examination-view.component';
import { SubjectExaminationDetailsViewComponent } from './user/admin_portal/component/view/subject-examination-details-view/subject-examination-details-view.component';
import { SubjectExaminationMaterialViewComponent } from './user/admin_portal/component/view/subject-examination-material-view/subject-examination-material-view.component';
import { SubjectExanimationTypeViewComponent } from './user/admin_portal/component/view/subject-exanimation-type-view/subject-exanimation-type-view.component';
import { ScheduleRealisationViewComponent } from './user/admin_portal/component/view/schedule-realisation-view/schedule-realisation-view.component';
import { SubjectRealisationTypeViewComponent } from './user/admin_portal/component/view/subject-realisation-type-view/subject-realisation-type-view.component';
import { SubjectRealisationNotificationViewComponent } from './user/admin_portal/component/view/subject-realisation-notification-view/subject-realisation-notification-view.component';
import { StaffViewComponent } from './user/admin_portal/component/view/staff-view/staff-view.component';
import { StaffTypeViewComponent } from './user/admin_portal/component/view/staff-type-view/staff-type-view.component';
import { FsFileViewComponent } from './user/admin_portal/component/view/fs-file-view/fs-file-view.component';
import { FsFileWindowComponent } from './user/admin_portal/component/window/fs-file-window/fs-file-window.component';
import { AdministrationDefaultComponent } from './user/administration_portal/layout/administration-default/administration-default.component';
import { AdministrationFooterComponent } from './user/administration_portal/layout/administration-footer/administration-footer.component';
import { AdministrationHeaderComponent } from './user/administration_portal/layout/administration-header/administration-header.component';
import { AdministrationSidebarComponent } from './user/administration_portal/layout/administration-sidebar/administration-sidebar.component';
import { UserDefaultComponent } from './user/user_portal/layout/user-default/user-default.component';
import { UserHeaderComponent } from './user/user_portal/layout/user-header/user-header.component';
import { UserFooterComponent } from './user/user_portal/layout/user-footer/user-footer.component';
import { UserSidebarComponent } from './user/user_portal/layout/user-sidebar/user-sidebar.component';
import { UserProfileComponent } from './user/user_portal/component/user-profile/user-profile.component';
import {EntranceExam} from '../model/entrance-exam/entrance-exam';
import {EntranceExamApplication} from '../model/entrance-exam-application/entrance-exam-application';
import {EntranceExamViewComponent} from './user/admin_portal/component/view/entrance-exam/entrance-exam-view';
import {EntranceExamApplicationViewComponent} from './user/admin_portal/component/view/entrance-exam-application/entrance-exam-application-view';
import {EntranceExamWindowComponent} from './user/admin_portal/component/window/entrance-exam/entrance-exam-window';
import {EntranceExamApplicationWindowComponent} from './user/admin_portal/component/window/entrance-exam-application/entrance-exam-application-window';
import { ChangeUserPasswordComponent } from './user/user_portal/component/change-user-password/change-user-password.component';
import { ChangeUserEmailComponent } from './user/user_portal/component/change-user-email/change-user-email.component';




@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    PageNotFoundComponent,
    LoginComponent,
    CityViewComponent,
    CityWindowComponent,
    CountryViewComponent,
    CountryWindowComponent,
    GenericTableComponent,
    ConfirmationDialogComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    PublicDefaultComponent,
    PublicHeaderComponent,
    PublicFooterComponent,
    PublicSidebarComponent,
    AdminDefaultComponent,
    AddressViewComponent,
    AddressWindowComponent,
    MultiExportComponent,
    ProfessorDefaultComponent,
    ProfessorFooterComponent,
    ProfessorHeaderComponent,
    ProfessorSidebarComponent,
    SudentDefaultComponent,
    SudentFooterComponent,
    SudentHeaderComponent,
    SudentSidebarComponent,
    RegisterComponent,
    StudentViewComponent,
    StudentWindowComponent,
    AboutComponent,
    FacultyComponent,
    UniversityViewComponent,
    UniversityWindowComponent,
    FacutlyViewComponent,
    FacultyWindowComponent,
    EmployeeViewComponent,
    EmployeeWindowComponent,
    EmployeeTypeViewComponent,
    UserViewComponent,
    EmployeeTypeWindowComponent,
    UserWindowComponent,
    GenericAutocompleteComponent,
    StudyProgramViewComponent,
    StudyProgramWindowComponent,
    YearOfStudyViewComponent,
    YearOfStudyWindowComponent,
    SubjectWindowComponent,
    SubjectViewComponent,
    UserRoleViewComponent,
    SubjectRealisationViewComponent,
    SubjectExaminationViewComponent,
    RoleViewComponent,
    ProfessorOnRealisationViewComponent,
    EnrolledSubjectViewComponent,
    ClassTypeViewComponent,
    UserRoleWindowComponent,
    SubjectRealisationWindowComponent,
    SubjectExaminationWindowComponent,
    RoleWindowComponent,
    ProfessorOnRealisationWindowComponent,
    EnrolledSubjectWindowComponent,
    ClassTypeWindowComponent,
    AcademicYearWindowComponent,
    AcademicYearViewComponent,
    EnrollmentTypeViewComponent,
    EnrollmentWindowComponent,
    EnrollmentTypeWindowComponent,
    DbFileComponent,
    FsFileComponent,
    AdminDashboardComponent,
    ChartLineComponent,
    ClassroomWindowComponent,
    ClassroomTypeWindowComponent,
    EnrolledYearOfStudyWindowComponent,
    EnrollmentYearTypeWindowComponent,
    ExaminationRegistrationWindowComponent,
    ScheduleExaminationWindowComponent,
    SubjectExaminationDetailsWindowComponent,
    SubjectExaminationMaterialWindowComponent,
    SubjectExaminationTypeWindowComponent,
    ScheduleRealisationWindowComponent,
    SubjectRealisationMaterialWindowComponent,
    SubjectRealisationTypeWindowComponent,
    SubjectRealisationMaterialViewComponent,
    SubjectRealisationNotificationWindowComponent,
    StaffWindowComponent,
    StaffTypeWindowComponent,
    ClassroomViewComponent,
    ClassroomTypeViewComponent,
    EnrolledYearOfStudyViewComponent,
    EnrollmentYearTypeViewComponent,
    ExaminationRegistrationViewComponent,
    ScheduleExaminationViewComponent,
    SubjectExaminationDetailsViewComponent,
    SubjectExaminationMaterialViewComponent,
    SubjectExanimationTypeViewComponent,
    ScheduleRealisationViewComponent,
    SubjectRealisationTypeViewComponent,
    SubjectRealisationNotificationViewComponent,
    StaffViewComponent,
    StaffTypeViewComponent,
    FsFileViewComponent,
    FsFileWindowComponent,
    SubjectRealisationMaterialViewComponent,
    AdministrationDefaultComponent,
    AdministrationFooterComponent,
    AdministrationHeaderComponent,
    AdministrationSidebarComponent,
    UserDefaultComponent,
    UserHeaderComponent,
    UserFooterComponent,
    UserSidebarComponent,
    UserProfileComponent,
    EntranceExamViewComponent,
    EntranceExamWindowComponent,
    EntranceExamApplicationViewComponent,
    EntranceExamApplicationWindowComponent,
    ChangeUserPasswordComponent,
    ChangeUserEmailComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialDesignModule,
    FlexLayoutModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
