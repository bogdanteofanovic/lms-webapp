import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './public/component/home/home.component';
import {PageNotFoundComponent} from './utils/page-not-found/page-not-found.component';
import {CityViewComponent} from './user/admin_portal/component/view/city-view/city-view.component';
import {CountryViewComponent} from './user/admin_portal/component/view/country-view/country-view.component';
import {PublicDefaultComponent} from './public/layout/public-default/public-default.component';
import {AdminDefaultComponent} from './user/admin_portal/layout/admin-default/admin-default.component';
import {AddressViewComponent} from './user/admin_portal/component/view/address-view/address-view.component';
import {AuthGuardService} from '../service/security/auth-guard.service';
import {SudentDefaultComponent} from './user/student_portal/layout/sudent-default/sudent-default.component';
import {StudentViewComponent} from './user/admin_portal/component/view/student-view/student-view.component';
import {AboutComponent} from './public/component/about/about.component';
import {FacultyComponent} from './public/component/faculty/faculty.component';
import {ProfessorDefaultComponent} from './user/professor_portal/layout/professor-default/professor-default.component';
import { UniversityViewComponent } from './user/admin_portal/component/view/university-view/university-view.component';
import { FacutlyViewComponent } from './user/admin_portal/component/view/faculty-view/facutly-view.component';
import { EmployeeViewComponent } from './user/admin_portal/component/view/employee-view/employee-view.component';
import { UserViewComponent } from './user/admin_portal/component/view/user-view/user-view.component';
import {EmployeeTypeViewComponent} from './user/admin_portal/component/view/employee-type-view/employee-type-view.component';
import { StudyProgramViewComponent } from './user/admin_portal/component/view/study-program-view/study-program-view.component';
import { YearOfStudyViewComponent } from './user/admin_portal/component/view/year-of-study-view/year-of-study-view.component';
import {RegisterComponent} from './public/component/register/register.component';
import {LoginComponent} from './public/component/login/login.component';
import { SubjectViewComponent } from './user/admin_portal/component/view/subject-view/subject-view.component';
import { AcademicYearViewComponent } from './user/admin_portal/component/view/academic-year-view/academic-year-view.component';
import { UserRoleViewComponent } from './user/admin_portal/component/view/user-role-view/user-role-view.component';
import { SubjectRealisationViewComponent } from './user/admin_portal/component/view/subject-realisation-view/subject-realisation-view.component';
import { SubjectExaminationViewComponent } from './user/admin_portal/component/view/subject-examination-view/subject-examination-view.component';
import { RoleViewComponent } from './user/admin_portal/component/view/role-view/role-view.component';
import { ProfessorOnRealisationViewComponent } from './user/admin_portal/component/view/professor-on-realisation-view/professor-on-realisation-view.component';
import { EnrolledSubjectViewComponent } from './user/admin_portal/component/view/enrolled-subject-view/enrolled-subject-view.component';
import { EnrollmentTypeViewComponent } from './user/admin_portal/component/view/enrollment-type-view/enrollment-type-view.component';
import { ClassTypeViewComponent } from './user/admin_portal/component/view/class-type-view/class-type-view.component';
import {DbFileComponent} from './public/component/file/db-file/db-file.component';
import {FsFileComponent} from './public/component/file/fs-file/fs-file.component';
import {AdminDashboardComponent} from './user/admin_portal/component/utils/admin-dashboard/admin-dashboard.component';
import { ClassroomViewComponent } from './user/admin_portal/component/view/classroom-view/classroom-view.component';
import { ClassroomTypeViewComponent } from './user/admin_portal/component/view/classroom-type-view/classroom-type-view.component';
import { EnrolledYearOfStudyViewComponent } from './user/admin_portal/component/view/enrolled-year-of-study-view/enrolled-year-of-study-view.component';
import { EnrollmentYearTypeViewComponent } from './user/admin_portal/component/view/enrollment-year-type-view/enrollment-year-type-view.component';
import { ExaminationRegistrationViewComponent } from './user/admin_portal/component/view/examination-registration-view/examination-registration-view.component';
import { ScheduleExaminationViewComponent } from './user/admin_portal/component/view/schedule-examination-view/schedule-examination-view.component';
import { SubjectExaminationDetailsViewComponent } from './user/admin_portal/component/view/subject-examination-details-view/subject-examination-details-view.component';
import { SubjectExanimationTypeViewComponent } from './user/admin_portal/component/view/subject-exanimation-type-view/subject-exanimation-type-view.component';
import { ScheduleRealisationViewComponent } from './user/admin_portal/component/view/schedule-realisation-view/schedule-realisation-view.component';
import { SubjectRealisationTypeViewComponent } from './user/admin_portal/component/view/subject-realisation-type-view/subject-realisation-type-view.component';
import { SubjectRealisationNotification } from 'src/model/subject-realisation-notification/subject-realisation-notification';
import {SubjectRealisationMaterialViewComponent} from "./user/admin_portal/component/view/subject-realisation-material-view/subject-realisation-material-view.component";
import { StaffViewComponent } from './user/admin_portal/component/view/staff-view/staff-view.component';
import { StaffTypeViewComponent } from './user/admin_portal/component/view/staff-type-view/staff-type-view.component';
import { SubjectRealisationNotificationViewComponent } from './user/admin_portal/component/view/subject-realisation-notification-view/subject-realisation-notification-view.component';
import {FsFileViewComponent} from "./user/admin_portal/component/view/fs-file-view/fs-file-view.component";
import {SubjectExaminationMaterialViewComponent} from './user/admin_portal/component/view/subject-examination-material-view/subject-examination-material-view.component';
import {UserProfileComponent} from './user/user_portal/component/user-profile/user-profile.component';
import {UserDefaultComponent} from './user/user_portal/layout/user-default/user-default.component';
import {EntranceExamApplicationViewComponent} from './user/admin_portal/component/view/entrance-exam-application/entrance-exam-application-view';
import {AdministrationDefaultComponent} from './user/administration_portal/layout/administration-default/administration-default.component';
import {EntranceExamViewComponent} from './user/admin_portal/component/view/entrance-exam/entrance-exam-view';



const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: '', component: PublicDefaultComponent, children: [
      {path: '', component: HomeComponent},
      {path: 'home', component: HomeComponent},
      {path: 'about', component: AboutComponent},
      {path: 'faculty', component: FacultyComponent, children: [
          {path: 'noviSad', component: FacultyComponent},
          {path: 'beograd', component: FacultyComponent},
        ]},
      {path: 'DBfile', component: DbFileComponent},
      {path: 'FSfile', component: FsFileComponent},
    ]},


  {path: 'admin', component: AdminDefaultComponent, canActivate: [AuthGuardService], canActivateChild: [AuthGuardService], data: { allowedRoles: ['ROLE_USER']},
    children: [
      {path: '', component: AdminDashboardComponent},
      {path: 'student', component: StudentViewComponent, data: { allowedRoles: ['ROLE_USER']}},
      {path: 'country', component: CountryViewComponent, data: { allowedRoles: ['ROLE_USER']}},
      {path: 'city', component: CityViewComponent, data: { allowedRoles: ['ROLE_USER']}},
      {path: 'address', component: AddressViewComponent},
      {path: 'university', component: UniversityViewComponent},
      {path: 'faculty', component: FacutlyViewComponent},
      {path: 'employee', component: EmployeeViewComponent},
      {path: 'employeeType', component: EmployeeTypeViewComponent},
      {path: 'user', component: UserViewComponent},
      {path: 'study-program', component: StudyProgramViewComponent},
      {path: 'year-of-study', component: YearOfStudyViewComponent},
      {path: 'subject', component: SubjectViewComponent},
      {path: 'academic-year', component: AcademicYearViewComponent},
      {path: 'user-role', component: UserRoleViewComponent},
      {path: 'subject-realisation', component: SubjectRealisationViewComponent},
      {path: 'subject-examination', component: SubjectExaminationViewComponent},
      {path: 'role', component: RoleViewComponent},
      {path: 'professor-on-realisation', component: ProfessorOnRealisationViewComponent},
      {path: 'enrolled-subject', component: EnrolledSubjectViewComponent},
      {path: 'enrollment-type', component: EnrollmentTypeViewComponent},
      {path: 'class-type', component: ClassTypeViewComponent},
      {path: 'classroom', component: ClassroomViewComponent},
      {path: 'classroom-type', component: ClassroomTypeViewComponent},
      {path: 'enrolled-year-of-study', component: EnrolledYearOfStudyViewComponent},
      {path: 'enrollment-type', component: EnrollmentTypeViewComponent},
      {path: 'enrollment-year-type', component: EnrollmentYearTypeViewComponent},
      {path: 'examination-registration', component: ExaminationRegistrationViewComponent},
      {path: 'schedule-examination', component: ScheduleExaminationViewComponent},
      {path: 'subject-examination-details', component: SubjectExaminationDetailsViewComponent},
      {path: 'subject-examination-type', component: SubjectExanimationTypeViewComponent},
      {path: 'schedule-realisation', component: ScheduleRealisationViewComponent},
      {path: 'subject-realisation-type', component: SubjectRealisationTypeViewComponent},
      {path: 'subject-realisation-notification', component: SubjectRealisationNotificationViewComponent},
      {path: 'staff', component: StaffViewComponent},
      {path: 'staff-type', component: StaffTypeViewComponent},
      {path: 'fs-file', component: FsFileViewComponent},
      {path: 'examination/:id/material', component: SubjectExaminationMaterialViewComponent},
      {path: 'realisation/:id/material', component: SubjectRealisationMaterialViewComponent},
      {path: 'entrance-exam', component: EntranceExamViewComponent},
      {path: 'entrance-exam-apply', component: EntranceExamApplicationViewComponent},
    ]},

  {path: 'professor', component: ProfessorDefaultComponent, canActivate: [AuthGuardService], canActivateChild: [AuthGuardService], data: { allowedRoles: ['ROLE_USER']},
    children: [
      {path: '', component: AdminDashboardComponent},
    ]},

  {path: 'student', component: SudentDefaultComponent, canActivate: [AuthGuardService],  canActivateChild: [AuthGuardService], data: { allowedRoles: ['ROLE_USER']},
    children: [
      {path: '', component: CountryViewComponent },
      {path: 'country', component: CountryViewComponent },
      {path: 'city', component: CityViewComponent },
      {path: 'address', component: AddressViewComponent }
    ]},

  {path: 'administration', component: AdministrationDefaultComponent, canActivate: [AuthGuardService],  canActivateChild: [AuthGuardService], data: { allowedRoles: ['ROLE_USER']},
    children: [
      {path: '', component: AdminDashboardComponent },
      {path: 'schedule-examination', component: ScheduleExaminationViewComponent},
      {path: 'schedule-realisation', component: ScheduleRealisationViewComponent},
      {path: 'student', component: StudentViewComponent}
    ]},

  {path: 'user', component: UserDefaultComponent, canActivate: [AuthGuardService], canActivateChild: [AuthGuardService], data: { allowedRoles: ['ROLE_USER']},
    children: [
      {path: '', component: UserProfileComponent},
      {path: 'entranceExamApply', component: EntranceExamApplicationViewComponent}
    ]},

  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
