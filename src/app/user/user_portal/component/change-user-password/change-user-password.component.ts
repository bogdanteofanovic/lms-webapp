import {Component, Inject, OnInit, Optional} from '@angular/core';
import {UserService} from '../../../../../service/user/user.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {User} from '../../../../../model/user/user';

@Component({
  selector: 'app-change-user-password',
  templateUrl: './change-user-password.component.html',
  styleUrls: ['./change-user-password.component.css']
})
export class ChangeUserPasswordComponent implements OnInit {

  password:string;
  private user:User;
  constructor(private userService:UserService, @Optional() @Inject(MAT_DIALOG_DATA) data: User, private matDialogRef:MatDialogRef<ChangeUserPasswordComponent>) {
    this.user = data;
  }

  ngOnInit(): void {
  }

  changeUserPassword(){
    console.log(this.user);
    this.user.password = this.password;
    this.userService.changeUserPassword(this.user).subscribe(e=>{
      this.matDialogRef.close();
    });
  }

}
