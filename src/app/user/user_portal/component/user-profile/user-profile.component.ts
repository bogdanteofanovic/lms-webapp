import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {UserService} from '../../../../../service/user/user.service';
import {User} from '../../../../../model/user/user';
import {MatDialog} from '@angular/material/dialog';
import {ChangeUserPasswordComponent} from '../change-user-password/change-user-password.component';
import {ChangeUserEmailComponent} from '../change-user-email/change-user-email.component';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  formGroup: FormGroup;
  titleAlert:string = 'Wrong';
  user:User = new User();

  constructor(private userService:UserService, private matDialog:MatDialog) { }

  ngOnInit(): void {
    this.userService.getByLoggedInUser().subscribe(e=>this.user = e);
  }

  changePassword(){
    this.matDialog.open(ChangeUserPasswordComponent, {data:this.user});
  }

  changeEmail(){
    this.matDialog.open(ChangeUserEmailComponent, {data:this.user});
  }

}
