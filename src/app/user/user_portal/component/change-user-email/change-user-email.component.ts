import {Component, Inject, OnInit, Optional} from '@angular/core';
import {User} from '../../../../../model/user/user';
import {UserService} from '../../../../../service/user/user.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-change-user-email',
  templateUrl: './change-user-email.component.html',
  styleUrls: ['./change-user-email.component.css']
})
export class ChangeUserEmailComponent implements OnInit {

  email:string;
  private user:User;
  constructor(private userService:UserService, @Optional() @Inject(MAT_DIALOG_DATA) data: User, private matDialogRef:MatDialogRef<ChangeUserEmailComponent>) {
    this.user = data;
  }

  ngOnInit(): void {
  }

  changeUserEmail(){
    console.log(this.user);
    this.user.email = this.email;
    this.userService.changeUserEmail(this.user).subscribe(e=>{
      this.matDialogRef.close();
    });
  }
}
