import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeUserEmailComponent } from './change-user-email.component';

describe('ChangeUserEmailComponent', () => {
  let component: ChangeUserEmailComponent;
  let fixture: ComponentFixture<ChangeUserEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeUserEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeUserEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
