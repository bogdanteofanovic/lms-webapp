import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AuthenticationService} from '../../../../../service/security/authentication.service';
import {ConfirmationDialogComponent} from '../../../../utils/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-user-header',
  templateUrl: './user-header.component.html',
  styleUrls: ['./user-header.component.css']
})
export class UserHeaderComponent implements OnInit {

  @Output()
  emmitCloseSidebar: EventEmitter<any> = new EventEmitter<any>();

  constructor(protected matDialog: MatDialog, protected authenticationService: AuthenticationService) { }

  ngOnInit(): void {

  }

  logout() {
    this.authenticationService.logout();
  }

  showInformationAboutApp() {
    this.matDialog.open(ConfirmationDialogComponent, {data: {type: 'info', message: 'LMS Application v1.0 beta'}});
  }

}
