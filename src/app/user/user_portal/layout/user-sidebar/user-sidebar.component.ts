import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../../../service/security/authentication.service';
import {MatDialog} from '@angular/material/dialog';
import {EntranceExamApplicationWindowComponent} from '../../../admin_portal/component/window/entrance-exam-application/entrance-exam-application-window';

@Component({
  selector: 'app-user-sidebar',
  templateUrl: './user-sidebar.component.html',
  styleUrls: ['./user-sidebar.component.css']
})
export class UserSidebarComponent implements OnInit {

  authenticationService:AuthenticationService;

  constructor(protected authService:AuthenticationService, private matDialog:MatDialog) {
    this.authenticationService = authService;
  }

  ngOnInit(): void {
  }

  entranceExamApply(){
    this.matDialog.open(EntranceExamApplicationWindowComponent);
  }

}
