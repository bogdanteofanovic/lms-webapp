import { Component, OnInit } from '@angular/core';
import {UserLayoutDefault} from '../../../user_shared/user-layout-default';
import {KeyEventBusService} from '../../../../../event/key-event-bus.service';

@Component({
  selector: 'app-user-default',
  templateUrl: './user-default.component.html',
  styleUrls: ['./user-default.component.css']
})
export class UserDefaultComponent extends UserLayoutDefault {

  constructor(keyEventBus: KeyEventBusService) {
    super(keyEventBus);
  }

}
