import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";
import {City} from '../../../../../../model/city/city';
import {CityService} from '../../../../../../service/city/city.service';
import {CountryService} from '../../../../../../service/country/country.service';
import {CountryWindowComponent} from '../country-window/country-window.component';
@Component({
  selector: 'app-city-window',
  templateUrl: './city-window.component.html',
  styleUrls: ['./city-window.component.css']
})
export class CityWindowComponent extends GenericWindow<City,CityService>{

  countryWindowComponent = CountryWindowComponent;

  constructor(cityService: CityService, @Optional() @Inject(MAT_DIALOG_DATA) data: City,
              matDialogRef: MatDialogRef<CityWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public countryService:CountryService, matSnackBar:MatSnackBar) {

    super(cityService, data, matDialogRef, gridEventBusService, matSnackBar, new City());
  }

  init() {

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required],
      'zipCode': [null, [Validators.required, Validators.minLength(4)]],
      'country': [null, Validators.required]
    });

  }


  // get name() {
  //   return this.formGroup.get('name') as FormControl
  // }

  // let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  //
  // // this.formGroup = this.formBuilder.group({
  // //   'email': [null, [Validators.required, Validators.pattern(emailregex)], this.checkInUseEmail],
  // //   'name': [null, Validators.required],
  // //   'password': [null, [Validators.required, this.checkPassword]],
  // //   'description': [null, [Validators.required, Validators.minLength(5), Validators.maxLength(10)]],
  // //   'validate': ''
  // // });

  //
  // setChangeValidate() {
  //   this.formGroup.get('validate').valueChanges.subscribe(
  //     (validate) => {
  //       if (validate == '1') {
  //         this.formGroup.get('name').setValidators([Validators.required, Validators.minLength(3)]);
  //         this.titleAlert = "You need to specify at least 3 characters";
  //       } else {
  //         this.formGroup.get('name').setValidators(Validators.required);
  //       }
  //       this.formGroup.get('name').updateValueAndValidity();
  //     }
  //   )
  // }
  //
  //
  // checkPassword(control) {
  //   let enteredPassword = control.value
  //   let passwordCheck = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/;
  //   return (!passwordCheck.test(enteredPassword) && enteredPassword) ? { 'requirements': true } : null;
  // }
  //
  // checkInUseEmail(control) {
  //   // mimic http database access
  //   let db = ['tony@gmail.com'];
  //   return new Observable(observer => {
  //     setTimeout(() => {
  //       let result = (db.indexOf(control.value) !== -1) ? { 'alreadyInUse': true } : null;
  //       observer.next(result);
  //       observer.complete();
  //     }, 4000)
  //   })
  // }
  //
  // getErrorEmail() {
  //   return this.formGroup.get('email').hasError('required') ? 'Field is required' :
  //     this.formGroup.get('email').hasError('pattern') ? 'Not a valid emailaddress' :
  //       this.formGroup.get('email').hasError('alreadyInUse') ? 'This emailaddress is already in use' : '';
  // }
  //
  // getErrorPassword() {
  //   return this.formGroup.get('password').hasError('required') ? 'Field is required (at least eight characters, one uppercase letter and one number)' :
  //     this.formGroup.get('password').hasError('requirements') ? 'Password needs to be at least eight characters, one uppercase letter and one number' : '';
  // }

}
