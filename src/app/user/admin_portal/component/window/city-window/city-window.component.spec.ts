import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityWindowComponent } from './city-window.component';

describe('CityWindowComponent', () => {
  let component: CityWindowComponent;
  let fixture: ComponentFixture<CityWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
