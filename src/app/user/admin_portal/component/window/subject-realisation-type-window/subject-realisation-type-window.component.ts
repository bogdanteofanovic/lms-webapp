import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { SubjectRealisationType } from 'src/model/subject-realisation-type/subject-realisation-type';
import { SubjectRealisationTypeService } from 'src/service/subject-realisation-type/subject-realisation-type.service';

@Component({
  selector: 'app-subject-realisation-type-window',
  templateUrl: './subject-realisation-type-window.component.html',
  styleUrls: ['./subject-realisation-type-window.component.css']
})
export class SubjectRealisationTypeWindowComponent extends GenericWindow<SubjectRealisationType, SubjectRealisationTypeService> {

  constructor(subjectRealisationTypeService: SubjectRealisationTypeService, @Optional() @Inject(MAT_DIALOG_DATA) data: SubjectRealisationType,
              matDialogRef: MatDialogRef<SubjectRealisationTypeWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog,  matSnackBar:MatSnackBar) {
    super(subjectRealisationTypeService, data, matDialogRef, gridEventBusService, matSnackBar, new SubjectRealisationType());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required]
    });
  }

}
