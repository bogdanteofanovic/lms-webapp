import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectRealisationTypeWindowComponent } from './subject-realisation-type-window.component';

describe('SubjectRealisationTypeWindowComponent', () => {
  let component: SubjectRealisationTypeWindowComponent;
  let fixture: ComponentFixture<SubjectRealisationTypeWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectRealisationTypeWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectRealisationTypeWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
