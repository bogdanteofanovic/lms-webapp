
import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {EntranceExamService} from '../../../../../../service/entrance-exam/entrance-exam.service';
import {AcademicYearWindowComponent} from '../academic-year-window/academic-year-window.component';
import {EntranceExam} from '../../../../../../model/entrance-exam/entrance-exam';
import {StudyProgramWindowComponent} from '../study-program-window/study-program-window.component';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {AcademicYearService} from '../../../../../../service/academic-year/academic-year.service';
import {StudyProgramService} from '../../../../../../service/study-program/study-program.service';

@Component({
  selector: 'app-entrance-exam-window',
  templateUrl: './entrance-exam-window.html',
  styleUrls: ['./entrance-exam-window.css']
})
export class EntranceExamWindowComponent extends GenericWindow<EntranceExam,EntranceExamService>{

  academicYearWindowComponent = AcademicYearWindowComponent;
  studyProgramWindowComponent = StudyProgramWindowComponent;


  constructor(entranceExamService: EntranceExamService, @Optional() @Inject(MAT_DIALOG_DATA) data: EntranceExam,
              matDialogRef: MatDialogRef<EntranceExamWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public academicYearService:AcademicYearService, public studyProgramService:StudyProgramService, matSnackBar:MatSnackBar) {
    super(entranceExamService, data, matDialogRef, gridEventBusService, matSnackBar, new EntranceExam());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'academicYear':[null, Validators.required],
      'startDate':[null, Validators.required],
      'endDate':[null, Validators.required],
      'studyProgram':[null, Validators.required],
    });
  }

}
