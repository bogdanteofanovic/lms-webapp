import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { ExaminationRegistration } from 'src/model/examination-registration/examination-registration';
import { ExaminationRegistrationService } from 'src/service/examination-registration/examination-registration.service';
import { ScheduleExaminationService } from 'src/service/schedule-examination/schedule-examination.service';
import { StudentService } from 'src/service/student/student.service';
import { ScheduleExaminationWindowComponent } from '../schedule-examination-window/schedule-examination-window.component';
import { StudentWindowComponent } from '../student-window/student-window.component';

@Component({
  selector: 'app-examination-registration-window',
  templateUrl: './examination-registration-window.component.html',
  styleUrls: ['./examination-registration-window.component.css']
})
export class ExaminationRegistrationWindowComponent extends  GenericWindow<ExaminationRegistration, ExaminationRegistrationService> {

  scheduleExaminationWindowComponent = ScheduleExaminationWindowComponent;
  studentWindowComponent = StudentWindowComponent;

  constructor(examinationRegistrationService: ExaminationRegistrationService, @Optional() @Inject(MAT_DIALOG_DATA) data: ExaminationRegistration,
              matDialogRef: MatDialogRef<ExaminationRegistrationWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public scheduleExaminationService:ScheduleExaminationService, public studentService: StudentService, matSnackBar:MatSnackBar) {
    super(examinationRegistrationService, data, matDialogRef, gridEventBusService, matSnackBar, new ExaminationRegistration());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'dateOfRegistration': [null, Validators.required],
      'scheduleExamination': [null, Validators.required],
      'student':[null, Validators.required]
    });
  }
}
