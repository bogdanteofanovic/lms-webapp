import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExaminationRegistrationWindowComponent } from './examination-registration-window.component';

describe('ExaminationRegistrationWindowComponent', () => {
  let component: ExaminationRegistrationWindowComponent;
  let fixture: ComponentFixture<ExaminationRegistrationWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExaminationRegistrationWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExaminationRegistrationWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
