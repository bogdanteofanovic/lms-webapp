import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassTypeWindowComponent } from './class-type-window.component';

describe('ClassTypeWindowComponent', () => {
  let component: ClassTypeWindowComponent;
  let fixture: ComponentFixture<ClassTypeWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassTypeWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassTypeWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
