import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";
import { ClassType } from 'src/model/class-type/class-type';
import { ClassTypeService } from 'src/service/class-type/class-type.service';

@Component({
  selector: 'app-class-type-window',
  templateUrl: './class-type-window.component.html',
  styleUrls: ['./class-type-window.component.css']
})
export class ClassTypeWindowComponent extends GenericWindow<ClassType, ClassTypeService> {



  constructor(classTypeService: ClassTypeService, @Optional() @Inject(MAT_DIALOG_DATA) data: ClassType,
              matDialogRef: MatDialogRef<ClassTypeWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, matSnackBar:MatSnackBar) {

    super(classTypeService, data, matDialogRef, gridEventBusService, matSnackBar, new ClassType());
  }

  init() {

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required]
    });

  }

}
