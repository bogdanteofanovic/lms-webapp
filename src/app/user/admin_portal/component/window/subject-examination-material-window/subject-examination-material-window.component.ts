import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import {SubjectExaminationMaterial} from "../../../../../../model/subject-examination-material/subject-examination-material";
import {SubjectExaminationMaterialService} from "../../../../../../service/subject-examination-material/subject-examination-material.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {FsFile} from "../../../../../../model/file/fs-file/fs-file";
import * as fileSaver from 'file-saver';
import {SubjectExaminationDetailsService} from "../../../../../../service/subject-examination-details/subject-examination-details.service";
import {SubjectExaminationDetailsWindowComponent} from "../subject-examination-details-window/subject-examination-details-window.component";
import {FsFileService} from "../../../../../../service/file/fs-file/fs-file.service";
import {SubjectExaminationTypeWindowComponent} from "../subject-examination-type-window/subject-examination-type-window.component";
import {FsFileWindowComponent} from "../fs-file-window/fs-file-window.component";

@Component({
  selector: 'app-subject-examination-material-window',
  templateUrl: './subject-examination-material-window.component.html',
  styleUrls: ['./subject-examination-material-window.component.css']
})
export class SubjectExaminationMaterialWindowComponent extends GenericWindow<SubjectExaminationMaterial, SubjectExaminationMaterialService> {

  subjectExaminationDetailsWindowComponent = SubjectExaminationDetailsWindowComponent;
  fsFileWindowComponent = FsFileWindowComponent;

  constructor(service: SubjectExaminationMaterialService, @Optional() @Inject(MAT_DIALOG_DATA) data: SubjectExaminationMaterial,
              matDialogRef: MatDialogRef<SubjectExaminationMaterialWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog: MatDialog, public subjectExaminationDetailsService: SubjectExaminationDetailsService, public fsFileService: FsFileService, matSnackBar: MatSnackBar) {
    super(service, data, matDialogRef, gridEventBusService, matSnackBar, new SubjectExaminationMaterial());
  }

  init() {

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'fsFile': [null, Validators.required],
      'subjectExaminationDetails': [null, Validators.required],
    });

  }

}
