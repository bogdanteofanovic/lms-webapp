import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectExaminationMaterialWindowComponent } from './subject-examination-material-window.component';

describe('SubjectExaminationMaterialWindowComponent', () => {
  let component: SubjectExaminationMaterialWindowComponent;
  let fixture: ComponentFixture<SubjectExaminationMaterialWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectExaminationMaterialWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectExaminationMaterialWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
