import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollmentTypeWindowComponent } from './enrollment-type-window.component';

describe('EnrollmentTypeWindowComponent', () => {
  let component: EnrollmentTypeWindowComponent;
  let fixture: ComponentFixture<EnrollmentTypeWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollmentTypeWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollmentTypeWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
