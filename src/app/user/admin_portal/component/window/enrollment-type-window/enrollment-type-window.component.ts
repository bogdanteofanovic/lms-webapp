import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";
import { EnrollmentType } from 'src/model/enrollment-type/enrollment-type';
import { EnrollmentTypeService } from 'src/service/enrollment-type/enrollment-type.service';
@Component({
  selector: 'app-enrollment-type-window',
  templateUrl: './enrollment-type-window.component.html',
  styleUrls: ['./enrollment-type-window.component.css']
})
export class EnrollmentTypeWindowComponent extends GenericWindow<EnrollmentType, EnrollmentTypeService> {


  constructor(enrollmentTypeService: EnrollmentTypeService, @Optional() @Inject(MAT_DIALOG_DATA) data: EnrollmentType,
              matDialogRef: MatDialogRef<EnrollmentTypeWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, matSnackBar:MatSnackBar) {

    super(enrollmentTypeService, data, matDialogRef, gridEventBusService, matSnackBar, new EnrollmentType());
  }

  init() {

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required]
    });

  }

}
