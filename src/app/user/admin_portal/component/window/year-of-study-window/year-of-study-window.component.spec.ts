import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YearOfStudyWindowComponent } from './year-of-study-window.component';

describe('YearOfStudyWindowComponent', () => {
  let component: YearOfStudyWindowComponent;
  let fixture: ComponentFixture<YearOfStudyWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YearOfStudyWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearOfStudyWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
