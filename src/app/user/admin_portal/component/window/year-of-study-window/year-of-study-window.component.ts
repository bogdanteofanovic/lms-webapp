
import { YearOfStudy } from 'src/model/year-of-study/year-of-study';
import { Component, OnInit, TypeProvider, Optional, Inject } from '@angular/core';
import { GenericWindow } from 'src/view/generic-window';
import { Observable } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { FormBuilder,Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {map, startWith} from "rxjs/operators";
import { StudyProgram } from 'src/model/study-program/study-program';
import { StudyProgramService } from 'src/service/study-program/study-program.service';
import { YearOfStudyService } from 'src/service/year-of-study/year-of-study.service';
import { StudyProgramWindowComponent } from '../study-program-window/study-program-window.component';


@Component({
  selector: 'app-year-of-study-window',
  templateUrl: './year-of-study-window.component.html',
  styleUrls: ['./year-of-study-window.component.css']
})
export class YearOfStudyWindowComponent extends GenericWindow<YearOfStudy, YearOfStudyService> {

  studyPrograms: StudyProgram[] = [];

  filteredOptionsStudyProgram: Observable<StudyProgram[]>

    constructor(yearOfStudyService: YearOfStudyService, @Optional() @Inject(MAT_DIALOG_DATA) data: YearOfStudy,
  matDialogRef: MatDialogRef<YearOfStudyWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
  private matDialog: MatDialog, private studyProgramService: StudyProgramService,
  matSnackBar: MatSnackBar) {
    super(yearOfStudyService, data, matDialogRef, gridEventBusService, matSnackBar, new YearOfStudy());
   }

  init(): void {
    this.getStudyPrograms(this.entity.studyProgram);
  }

  changeStudyProgram(){
    this.formGroup.get('studyProgram').enable();
  }

  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required],
      'studyProgram': [null, Validators.required]
    });
    //autocomplete filter options
    this.filteredOptionsStudyProgram = this.formGroup.get('studyProgram').valueChanges
    .pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.name),
      map(name => name ? this._filterStudyProgram(name) : this.studyPrograms.slice())
    );
  }

    //fn for displaying StudyProgram ~ toString
    displayFnStudyProgram(c: StudyProgram): string {
      return c && c.name ? c.name : '';
    }
  
    //filter
    private _filterStudyProgram(name: string): StudyProgram[] {
      const filterValue = name.toLowerCase();
      return this.studyPrograms.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
    }

      //set studyProgram from autocomplete
  setStudyProgram(studyProgram){
    this.entity.studyProgram = studyProgram;
  }

  //open dialog for insert new studyProgram
  addStudyProgram(){
    this.matDialog.open(StudyProgramWindowComponent).afterClosed().subscribe(e=>e!=null?this.getStudyPrograms(e.data):'');
  }

  getStudyProgram(studyProgram:StudyProgram){
    //after dialog close set new studyProgram
    if(studyProgram.id != 0){
      this.entity.studyProgram = studyProgram;
      this.formGroup.get('studyProgram').setValue(this.entity.studyProgram);
    }
  }

  editStudyProgram(){
    this.matDialog.open(StudyProgramWindowComponent,{data:this.entity.studyProgram}).afterClosed().subscribe(e=>e!=null?this.getStudyProgram(e.data):'');
  }

  getStudyPrograms(studyProgram:StudyProgram){

    //after dialog close set new address
    if(studyProgram.id != 0){

      this.studyProgramService.getAll().subscribe(studyPrograms => this.studyPrograms = studyPrograms);
      this.entity.studyProgram = studyProgram;
      this.formGroup.get('studyProgram').setValue(this.entity.studyProgram);

    }else{

      this.studyProgramService.getAll().subscribe(studyPrograms => this.studyPrograms = studyPrograms);
    }
  }

}
