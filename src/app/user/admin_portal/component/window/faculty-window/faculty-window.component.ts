import { Component, OnInit, Optional, Inject } from '@angular/core';
import { Faculty } from 'src/model/faculty/faculty';
import { FacultyService } from 'src/service/faculty/faculty.service';
import { Address } from 'src/model/adress/address';
import { University } from 'src/model/university/university';
import { Observable } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AddressService } from 'src/service/address/address.service';
import { UniversityService } from 'src/service/university/university.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AddressWindowComponent } from '../address-window/address-window.component';
import { UniversityWindowComponent } from '../university-window/university-window.component';
import {map, startWith} from "rxjs/operators";
import { GenericWindow } from 'src/view/generic-window';


@Component({
  selector: 'app-faculty-window',
  templateUrl: './faculty-window.component.html',
  styleUrls: ['./faculty-window.component.css']
})
export class FacultyWindowComponent extends GenericWindow<Faculty, FacultyService>{

  addressWindowComponent = AddressWindowComponent;
  universityWindowComponent = UniversityWindowComponent;


  constructor(facultyService: FacultyService, @Optional() @Inject(MAT_DIALOG_DATA) data: Faculty,
              matDialogRef: MatDialogRef<FacultyWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog: MatDialog, public addressService: AddressService, public universityService: UniversityService, matSnackBar: MatSnackBar) {
    super(facultyService, data, matDialogRef, gridEventBusService, matSnackBar, new Faculty());
  }

  init(){
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required],
      'description': [null, Validators.required],
      'email': [null, [Validators.required,Validators.email]],
      'phoneNumber': [null, Validators.required],
      'address': [null, Validators.required],
      'university': [null, Validators.required]
    });

}

}
