import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacultyWindowComponent } from './faculty-window.component';

describe('FacultyWindowComponent', () => {
  let component: FacultyWindowComponent;
  let fixture: ComponentFixture<FacultyWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacultyWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
