import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { ScheduleRealisation } from 'src/model/schedule-realisation/schedule-realisation';
import { ScheduleRealisationService } from 'src/service/schedule-realisation/schedule-realisation.service';
import { ClassroomWindowComponent } from '../classroom-window/classroom-window.component';
import { SubjectRealisationWindowComponent } from '../subject-realisation-window/subject-realisation-window.component';
import { ClassroomService } from 'src/service/classroom/classroom.service';
import { SubjectRealisationService } from 'src/service/subject-realisation/subject-realisation.service';
import { AxisDateTimeLabelFormatsOptions} from 'highcharts';
import { Time } from '@angular/common';
import { Classroom } from 'src/model/classroom/classroom';

@Component({
  selector: 'app-schedule-realisation-window',
  templateUrl: './schedule-realisation-window.component.html',
  styleUrls: ['./schedule-realisation-window.component.css']
})
export class ScheduleRealisationWindowComponent extends GenericWindow<ScheduleRealisation, ScheduleRealisationService> {

  classroomWindowComponent = ClassroomWindowComponent;
  subjectRealisationWindowComponent = SubjectRealisationWindowComponent;

  schedules: ScheduleRealisation[] = [];

  constructor(scheduleRealisationService: ScheduleRealisationService, @Optional() @Inject(MAT_DIALOG_DATA) data: ScheduleRealisation,
              matDialogRef: MatDialogRef<ScheduleRealisationWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public classroomService:ClassroomService, public subjectRealisationService: SubjectRealisationService, matSnackBar:MatSnackBar) {
    super(scheduleRealisationService, data, matDialogRef, gridEventBusService, matSnackBar, new ScheduleRealisation());
  }



  ngOnInit(): void {


    this.createForm();

    if(!this.entity){
      this.add = true;
      this.entity = this.emptyEntity;
    }else {
      this.add = false;
    }

    this.init();

    this.initFormFields();

  }

  init() {
    this.service.getAll().subscribe(schedules => {this.schedules = schedules
    console.log(schedules)});
  }

  compareDates(){
    //Returns true if start is bigger than end
    console.log(this.entity)
    return this.entity.startTime > this.entity.endTime;
  }

  isFreeSchedule(){
    for(var i = 0; i < this.schedules.length; i++){
      if(this.schedules[i].classroom.id != this.entity.classroom.id){
        continue;
      }
      let scheduledStart = this.schedules[i].startTime;
      let scheduledEnd = this.schedules[i].endTime;

      if( ((scheduledStart < this.entity.startTime) && (scheduledEnd > this.entity.startTime)) || ((scheduledStart < this.entity.endTime) && (scheduledEnd > this.entity.endTime)) ){
        return false;
      }
    }
    return true;
  }

  submitForm():void{
    this.bindBeforeSending();
    console.log(this.entity.classroom);
    if(this.compareDates()){
      this.errorSubmit({message: "Start date can't be bigger than end date!"})
      return;
    }
    if(!this.isFreeSchedule()){
      this.errorSubmit({message: "Date has already been taken!"})
      return;
    }
    if(this.add){
      this.service.addOne(this.entity).subscribe(e=> this.successSubmit(e), error => this.errorSubmit(error));

    }
    else {
      this.service.updateOne(this.entity.id, this.entity).subscribe(e=>this.successSubmit(e), error => this.errorSubmit(error));

    }

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'startTime': [null, Validators.required],
      'endTime': [null, Validators.required],
      'classroom':[null, Validators.required],
      'subjectRealisation':[null, Validators.required]
    });
  }

}
