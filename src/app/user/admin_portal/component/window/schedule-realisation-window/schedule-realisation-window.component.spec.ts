import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleRealisationWindowComponent } from './schedule-realisation-window.component';

describe('ScheduleRealisationWindowComponent', () => {
  let component: ScheduleRealisationWindowComponent;
  let fixture: ComponentFixture<ScheduleRealisationWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleRealisationWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleRealisationWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
