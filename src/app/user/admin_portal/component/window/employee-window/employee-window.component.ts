import { Component, OnInit, TypeProvider, Optional, Inject } from '@angular/core';
import { GenericWindow } from 'src/view/generic-window';
import { Employee } from 'src/model/employee/employee';
import { EmployeeService } from 'src/service/employee/employee.service';
import { Address } from 'src/model/adress/address';
import { User } from 'src/model/user/user';
import { EmployeeType } from 'src/model/employee-type/employee-type';
import { Observable } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { FormBuilder,Validators } from '@angular/forms';
import { AddressService } from 'src/service/address/address.service';
import { UserService } from 'src/service/user/user.service';
import { EmployeeTypeService } from 'src/service/employee-type/employee-type.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AddressWindowComponent } from '../address-window/address-window.component';
import {map, startWith} from "rxjs/operators";
import { UserWindowComponent } from '../user-window/user-window.component';
import { EmployeeTypeWindowComponent } from '../employee-type-window/employee-type-window.component';
import { AbstractEntity } from 'src/model/abstract-entity/abstract-entity';

@Component({
  selector: 'app-employee-window',
  templateUrl: './employee-window.component.html',
  styleUrls: ['./employee-window.component.css']
})
export class EmployeeWindowComponent extends GenericWindow<Employee, EmployeeService> {

  addressWindowComponent = AddressWindowComponent;
  userWindowComponent = UserWindowComponent;
  employeeTypeWindowComponent = EmployeeTypeWindowComponent;

  constructor(employeeService: EmployeeService, @Optional() @Inject(MAT_DIALOG_DATA) data: Employee,
              matDialogRef: MatDialogRef<EmployeeWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog: MatDialog, public addressService: AddressService, public userService: UserService, public employeeTypeService: EmployeeTypeService,
              matSnackBar: MatSnackBar) {
    super(employeeService, data, matDialogRef, gridEventBusService, matSnackBar, new Employee());
               }

init(){
}

//create form and add validators
createForm() {

  this.formGroup = this.formBuilder.group({
    'name': [null, Validators.required],
    'lastName': [null, Validators.required],
    'user': [null, Validators.required],
    'address': [null, Validators.required],
    'employeeType': [null, Validators.required]
  });
}

}
