import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { Classroom } from 'src/model/classroom/classroom';
import { ClassroomService } from 'src/service/classroom/classroom.service';
import { ClassroomTypeWindowComponent } from '../classroom-type-window/classroom-type-window.component';
import { FacultyWindowComponent } from '../faculty-window/faculty-window.component';
import { ClassroomTypeService } from 'src/service/classroom-type/classroom-type.service';
import { FacultyService } from 'src/service/faculty/faculty.service';

@Component({
  selector: 'app-classroom-window',
  templateUrl: './classroom-window.component.html',
  styleUrls: ['./classroom-window.component.css']
})
export class ClassroomWindowComponent extends GenericWindow<Classroom, ClassroomService> {

  classroomTypeWindowComponent = ClassroomTypeWindowComponent;
  facultyWindowComponent = FacultyWindowComponent;

  constructor(classroomService: ClassroomService, @Optional() @Inject(MAT_DIALOG_DATA) data: Classroom,
              matDialogRef: MatDialogRef<ClassroomWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public classroomTypeService: ClassroomTypeService, public facultyService: FacultyService, matSnackBar:MatSnackBar) {
    super(classroomService, data, matDialogRef, gridEventBusService, matSnackBar, new Classroom());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required],
      'seats': [null, Validators.required],
      'classroomType': [null, Validators.required],
      'faculty':[null, Validators.required]
    });
  }
}
