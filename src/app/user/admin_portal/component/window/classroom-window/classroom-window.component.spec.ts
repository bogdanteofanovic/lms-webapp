import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassroomWindowComponent } from './classroom-window.component';

describe('ClassroomWindowComponent', () => {
  let component: ClassroomWindowComponent;
  let fixture: ComponentFixture<ClassroomWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassroomWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
