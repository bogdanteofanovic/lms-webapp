import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolledYearOfStudyWindowComponent } from './enrolled-year-of-study-window.component';

describe('EnrolledYearOfStudyWindowComponent', () => {
  let component: EnrolledYearOfStudyWindowComponent;
  let fixture: ComponentFixture<EnrolledYearOfStudyWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrolledYearOfStudyWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolledYearOfStudyWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
