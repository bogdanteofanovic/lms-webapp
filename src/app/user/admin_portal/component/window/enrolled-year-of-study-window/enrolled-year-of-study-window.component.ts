import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { EnrolledYearOfStudy } from 'src/model/enrolled-year-of-study/enrolled-year-of-study';
import { EnrolledYearOfStudyService } from 'src/service/enrolled-year-of-study/enrolled-year-of-study.service';
import { AcademicYearWindowComponent } from '../academic-year-window/academic-year-window.component';
import { EnrollmentYearTypeWindowComponent } from '../enrollment-year-type-window/enrollment-year-type-window.component';
import { StudentWindowComponent } from '../student-window/student-window.component';
import { AcademicYearService } from 'src/service/academic-year/academic-year.service';
import { EnrollmentYearTypeService } from 'src/service/enrollment-year-type/enrollment-year-type.service';
import { StudentService } from 'src/service/student/student.service';
import { YearOfStudyWindowComponent } from '../year-of-study-window/year-of-study-window.component';
import { YearOfStudyService } from 'src/service/year-of-study/year-of-study.service';

@Component({
  selector: 'app-enrolled-year-of-study-window',
  templateUrl: './enrolled-year-of-study-window.component.html',
  styleUrls: ['./enrolled-year-of-study-window.component.css']
})
export class EnrolledYearOfStudyWindowComponent extends GenericWindow<EnrolledYearOfStudy, EnrolledYearOfStudyService> {

  academicYearWindowComponent = AcademicYearWindowComponent;
  enrollmentYearTypeWindowComponent = EnrollmentYearTypeWindowComponent;
  studentWindowComponent = StudentWindowComponent;
  yearOfStudyWindowComponent = YearOfStudyWindowComponent;

  constructor(enrolledYearOfStudyService: EnrolledYearOfStudyService, @Optional() @Inject(MAT_DIALOG_DATA) data: EnrolledYearOfStudy,
              matDialogRef: MatDialogRef<EnrolledYearOfStudyWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public academicYearService:AcademicYearService, public enrollmentYearTypeService: EnrollmentYearTypeService,
              public yearOfStudyService: YearOfStudyService, public studentService: StudentService, matSnackBar:MatSnackBar) {
    super(enrolledYearOfStudyService, data, matDialogRef, gridEventBusService, matSnackBar, new EnrolledYearOfStudy());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'dateOfEnrollment': [null, Validators.required],
      'academicYear': [null, Validators.required],
      'enrollmentYearType':[null, Validators.required],
      'student':[null, Validators.required],
      'yearOfStudy':[null, Validators.required]
    });
  }


}
