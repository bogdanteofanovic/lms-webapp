import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { Staff } from 'src/model/staff/staff';
import { StaffService } from 'src/service/staff/staff.service';
import { EmployeeWindowComponent } from '../employee-window/employee-window.component';
import { StaffTypeWindowComponent } from '../staff-type-window/staff-type-window.component';
import { EmployeeService } from 'src/service/employee/employee.service';
import { StaffTypeService } from 'src/service/staff-type/staff-type.service';

@Component({
  selector: 'app-staff-window',
  templateUrl: './staff-window.component.html',
  styleUrls: ['./staff-window.component.css']
})
export class StaffWindowComponent extends GenericWindow<Staff, StaffService> {

  employeeWindowComponent = EmployeeWindowComponent;
  staffTypeWindowComponent = StaffTypeWindowComponent;

  constructor(staffService: StaffService, @Optional() @Inject(MAT_DIALOG_DATA) data: Staff,
              matDialogRef: MatDialogRef<StaffWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public employeeService:EmployeeService, public staffTypeService: StaffTypeService, matSnackBar:MatSnackBar) {
    super(staffService, data, matDialogRef, gridEventBusService, matSnackBar, new Staff());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'employmentStart': [null, Validators.required],
      'employmentEnd': [null, Validators.required],
      'employee':[null, Validators.required],
      'staffType':[null, Validators.required],
      'active': [null, Validators.required]
    });
  }

}
