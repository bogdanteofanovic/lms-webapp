import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffWindowComponent } from './staff-window.component';

describe('StaffWindowComponent', () => {
  let component: StaffWindowComponent;
  let fixture: ComponentFixture<StaffWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
