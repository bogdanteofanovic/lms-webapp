import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { ScheduleExamination } from 'src/model/schedule-examination/schedule-examination';
import { ScheduleExaminationService } from 'src/service/schedule-examination/schedule-examination.service';
import { SubjectExaminationDetailsWindowComponent } from '../subject-examination-details-window/subject-examination-details-window.component';
import { SubjectExaminationDetailsService } from 'src/service/subject-examination-details/subject-examination-details.service';

@Component({
  selector: 'app-schedule-examination-window',
  templateUrl: './schedule-examination-window.component.html',
  styleUrls: ['./schedule-examination-window.component.css']
})
export class ScheduleExaminationWindowComponent extends GenericWindow<ScheduleExamination, ScheduleExaminationService> {

  subjectExaminationDetailsWindowComponent = SubjectExaminationDetailsWindowComponent;

  constructor(scheduleExaminationService: ScheduleExaminationService, @Optional() @Inject(MAT_DIALOG_DATA) data: ScheduleExamination,
              matDialogRef: MatDialogRef<ScheduleExaminationWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public subjectExaminationDetailsService:SubjectExaminationDetailsService, matSnackBar:MatSnackBar) {
    super(scheduleExaminationService, data, matDialogRef, gridEventBusService, matSnackBar, new ScheduleExamination());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'beginTime': [null, Validators.required],
      'endTime': [null, Validators.required],
      'subjectExaminationDetails':[null, Validators.required]
    });
  }

}
