import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleExaminationWindowComponent } from './schedule-examination-window.component';

describe('ScheduleExaminationWindowComponent', () => {
  let component: ScheduleExaminationWindowComponent;
  let fixture: ComponentFixture<ScheduleExaminationWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleExaminationWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleExaminationWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
