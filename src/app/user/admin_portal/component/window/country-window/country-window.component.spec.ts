import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryWindowComponent } from './country-window.component';

describe('CountryWindowComponent', () => {
  let component: CountryWindowComponent;
  let fixture: ComponentFixture<CountryWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
