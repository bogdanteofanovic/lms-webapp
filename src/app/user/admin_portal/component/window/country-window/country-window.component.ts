import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {Country} from '../../../../../../model/country/country';
import {CountryService} from '../../../../../../service/country/country.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-country-window',
  templateUrl: './country-window.component.html',
  styleUrls: ['./country-window.component.css']
})
export class CountryWindowComponent extends GenericWindow<Country, CountryService>{


  constructor(countryService: CountryService, @Optional() @Inject(MAT_DIALOG_DATA) data: Country,
              matDialogRef: MatDialogRef<CountryWindowComponent>, gridEventBusService:GridEventBusService,
              private formBuilder:FormBuilder, matSnackBar:MatSnackBar ) {
    super(countryService, data, matDialogRef, gridEventBusService, matSnackBar, new Country());
  }


  init() {

  }


  createForm(): void {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required]
    });

  }


}
