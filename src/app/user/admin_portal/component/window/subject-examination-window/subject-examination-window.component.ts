import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";
import { SubjectExamination } from 'src/model/subject-examination/subject-examination';
import { SubjectExaminationService } from 'src/service/subject-examination/subject-examination.service';
import { StudentService } from 'src/service/student/student.service';
import { SubjectRealisationService } from 'src/service/subject-realisation/subject-realisation.service';
import {StudentWindowComponent} from '../student-window/student-window.component';
import {SubjectRealisationWindowComponent} from '../subject-realisation-window/subject-realisation-window.component';
import { SubjectExaminationDetailsWindowComponent } from '../subject-examination-details-window/subject-examination-details-window.component';
import { SubjectExaminationDetailsService } from 'src/service/subject-examination-details/subject-examination-details.service';

@Component({
  selector: 'app-subject-examination-window',
  templateUrl: './subject-examination-window.component.html',
  styleUrls: ['./subject-examination-window.component.css']
})
export class SubjectExaminationWindowComponent extends GenericWindow<SubjectExamination, SubjectExaminationService> {

  studentWindowComponent = StudentWindowComponent;
  subjectRealisationWindowComponent = SubjectRealisationWindowComponent;
  subjectExaminationDetailsWindowComponent = SubjectExaminationDetailsWindowComponent;

  constructor(subjectExaminationService: SubjectExaminationService, @Optional() @Inject(MAT_DIALOG_DATA) data: SubjectExamination,
              matDialogRef: MatDialogRef<SubjectExaminationWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public subjectExaminationDetailsService: SubjectExaminationDetailsService, public studentService:StudentService, public subjectRealisationService: SubjectRealisationService, matSnackBar:MatSnackBar) {

    super(subjectExaminationService, data, matDialogRef, gridEventBusService, matSnackBar, new SubjectExamination());
  }

  init() {

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'score': [null, Validators.required],
      'student': [null, Validators.required],
      'subjectExaminationDetails': [null, Validators.required]
    });

  }

}
