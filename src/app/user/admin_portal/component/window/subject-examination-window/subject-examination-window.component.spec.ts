import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectExaminationWindowComponent } from './subject-examination-window.component';

describe('SubjectExaminationWindowComponent', () => {
  let component: SubjectExaminationWindowComponent;
  let fixture: ComponentFixture<SubjectExaminationWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectExaminationWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectExaminationWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
