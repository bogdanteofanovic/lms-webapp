import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressWindowComponent } from './address-window.component';

describe('AddressWindowComponent', () => {
  let component: AddressWindowComponent;
  let fixture: ComponentFixture<AddressWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
