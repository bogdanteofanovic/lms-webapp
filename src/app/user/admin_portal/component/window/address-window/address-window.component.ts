
import {Address} from "../../../../../../model/adress/address";
import {AddressService} from "../../../../../../service/address/address.service";
import {CityService} from "../../../../../../service/city/city.service";
import {CityWindowComponent} from "../city-window/city-window.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";

@Component({
  selector: 'app-address-window',
  templateUrl: './address-window.component.html',
  styleUrls: ['./address-window.component.css']
})
export class AddressWindowComponent extends GenericWindow<Address,AddressService>{

  cityWindowComponent = CityWindowComponent;

  constructor(addressService: AddressService, @Optional() @Inject(MAT_DIALOG_DATA) data: Address,
              matDialogRef: MatDialogRef<AddressWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public cityService:CityService, matSnackBar:MatSnackBar) {
    super(addressService, data, matDialogRef, gridEventBusService, matSnackBar, new Address());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required],
      'city': [null, Validators.required],
      'number':[null, Validators.required]
    });
  }




}
