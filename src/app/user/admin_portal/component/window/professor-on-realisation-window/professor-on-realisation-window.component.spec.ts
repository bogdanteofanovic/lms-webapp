import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessorOnRealisationWindowComponent } from './professor-on-realisation-window.component';

describe('ProfessorOnRealisationWindowComponent', () => {
  let component: ProfessorOnRealisationWindowComponent;
  let fixture: ComponentFixture<ProfessorOnRealisationWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessorOnRealisationWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessorOnRealisationWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
