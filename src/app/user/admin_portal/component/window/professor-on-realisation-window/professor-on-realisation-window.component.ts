import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";
import { ProfessorOnRealisation } from 'src/model/professor-on-realisation/professor-on-realisation';
import { ProfessorOnRealisationService } from 'src/service/professor-on-realisation/professor-on-realisation.service';
import { ClassTypeService } from 'src/service/class-type/class-type.service';
import { EmployeeService } from 'src/service/employee/employee.service';
import { SubjectRealisationService } from 'src/service/subject-realisation/subject-realisation.service';
import {ClassTypeWindowComponent} from '../class-type-window/class-type-window.component';
import {EmployeeWindowComponent} from '../employee-window/employee-window.component';
import {SubjectRealisationWindowComponent} from '../subject-realisation-window/subject-realisation-window.component';

@Component({
  selector: 'app-professor-on-realisation-window',
  templateUrl: './professor-on-realisation-window.component.html',
  styleUrls: ['./professor-on-realisation-window.component.css']
})
export class ProfessorOnRealisationWindowComponent extends GenericWindow<ProfessorOnRealisation, ProfessorOnRealisationService> {

  classTypeWindowComponent = ClassTypeWindowComponent;
  employeeWindowComponent = EmployeeWindowComponent;
  subjectRealisationWindowComponent = SubjectRealisationWindowComponent;

  constructor(professorOnRealisationService: ProfessorOnRealisationService, @Optional() @Inject(MAT_DIALOG_DATA) data: ProfessorOnRealisation,
              matDialogRef: MatDialogRef<ProfessorOnRealisationWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public classTypeService:ClassTypeService, public employeeService: EmployeeService, public subjectOnRealisationService: SubjectRealisationService, matSnackBar:MatSnackBar) {

    super(professorOnRealisationService, data, matDialogRef, gridEventBusService, matSnackBar, new ProfessorOnRealisation());
  }

  init() {

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'classType': [null, Validators.required],
      'employee': [null, Validators.required],
      'subjectRealisation': [null, Validators.required]
    });

  }

}
