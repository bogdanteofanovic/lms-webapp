import {Component, Inject, OnInit, Optional} from '@angular/core';
import { SubjectService } from 'src/service/subject/subject.service';
import { Subject } from 'src/model/subject/subject';
import { YearOfStudyService } from 'src/service/year-of-study/year-of-study.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {MatSnackBar} from "@angular/material/snack-bar";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { GenericWindow } from 'src/view/generic-window';
import {YearOfStudyWindowComponent} from '../year-of-study-window/year-of-study-window.component';
@Component({
  selector: 'app-subject-window',
  templateUrl: './subject-window.component.html',
  styleUrls: ['./subject-window.component.css']
})
export class SubjectWindowComponent extends GenericWindow<Subject,SubjectService> {

  yearOfStudyWindowComponent = YearOfStudyWindowComponent;

  constructor(subjectService: SubjectService, @Optional() @Inject(MAT_DIALOG_DATA) data: Subject,
  matDialogRef: MatDialogRef<SubjectWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
  private matDialog:MatDialog, public yearOfStudyService:YearOfStudyService, matSnackBar:MatSnackBar) {

  super(subjectService, data, matDialogRef, gridEventBusService, matSnackBar, new Subject());
}

init() {
  console.log(this.entity)
}

createForm() {

  this.formGroup = this.formBuilder.group({
    'name': [null, Validators.required],
    'score': [null, Validators.required],
    'yearOfStudy': [null, Validators.required],
    'syllabus': [null, Validators.required]
  });

}

}
