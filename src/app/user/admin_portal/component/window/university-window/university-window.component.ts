import { Component, OnInit, Optional, Inject } from '@angular/core';
import { GenericWindow } from 'src/view/generic-window';
import { University } from 'src/model/university/university';
import { UniversityService } from 'src/service/university/university.service';
import {Observable} from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { AddressService } from 'src/service/address/address.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Address } from 'src/model/adress/address';
import {map, startWith} from "rxjs/operators";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { AddressWindowComponent } from '../address-window/address-window.component';
@Component({
  selector: 'app-university-window',
  templateUrl: './university-window.component.html',
  styleUrls: ['./university-window.component.css']
})
export class UniversityWindowComponent extends GenericWindow<University, UniversityService> {


  addressWindowComponent = AddressWindowComponent;

  constructor(universityService: UniversityService, @Optional() @Inject(MAT_DIALOG_DATA) data: University,
              matDialogRef: MatDialogRef<UniversityWindowComponent>, gridEventBusService: GridEventBusService,
              private matDialog: MatDialog, private formBuilder: FormBuilder, public addressService: AddressService, matSnackBar: MatSnackBar) {
    super(universityService, data, matDialogRef, gridEventBusService, matSnackBar, new University());
  }

  init() {

  }

  createForm(){
    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required],
      'address': [null, Validators.required],
      'description': [null, Validators.required],
      'phoneNumber': [null, Validators.required]

    });


  }


}
