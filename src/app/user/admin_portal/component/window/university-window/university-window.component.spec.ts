import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniversityWindowComponent } from './university-window.component';

describe('UniversityWindowComponent', () => {
  let component: UniversityWindowComponent;
  let fixture: ComponentFixture<UniversityWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniversityWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniversityWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
