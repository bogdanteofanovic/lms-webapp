
import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {EntranceExamApplicationService} from '../../../../../../service/entrance-exam-application/entrance-exam-application.service';
import {UserWindowComponent} from '../user-window/user-window.component';
import {EntranceExamApplication} from '../../../../../../model/entrance-exam-application/entrance-exam-application';
import {EntranceExamWindowComponent} from '../entrance-exam/entrance-exam-window';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {UserService} from '../../../../../../service/user/user.service';
import {EntranceExamService} from '../../../../../../service/entrance-exam/entrance-exam.service';
import {AuthenticationService} from '../../../../../../service/security/authentication.service';

@Component({
  selector: 'app-entrance-exam-application-window',
  templateUrl: './entrance-exam-application-window.html',
  styleUrls: ['./entrance-exam-application-window.css']
})
export class EntranceExamApplicationWindowComponent extends GenericWindow<EntranceExamApplication,EntranceExamApplicationService>{

  entranceExamWindowComponent = EntranceExamWindowComponent;


  constructor(entranceExamApplicationService: EntranceExamApplicationService, @Optional() @Inject(MAT_DIALOG_DATA) data: EntranceExamApplication,
              matDialogRef: MatDialogRef<EntranceExamApplicationWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public userService:UserService, public entranceExamService:EntranceExamService, matSnackBar:MatSnackBar, private authenticationService:AuthenticationService) {
    super(entranceExamApplicationService, data, matDialogRef, gridEventBusService, matSnackBar, new EntranceExamApplication());
  }

  init() {

    if(this.authenticationService.isAdmin()){
      this.enableCbAdd = true;
      this.enableCbEdit = true;
      this.disabledCb = false;
    }

    this.userService.getByLoggedInUser().subscribe(user => {
      this.entity.user = user;
      this.formGroup.get('user').setValue(this.entity.user);
      this.formGroup.get('dateOfApplication').setValue(new Date().toISOString().slice(0, 19))
    });

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'entranceExam':[null, Validators.required],
      'user':[null, Validators.required],
      'dateOfApplication':[null, Validators.required],
    });
  }

}
