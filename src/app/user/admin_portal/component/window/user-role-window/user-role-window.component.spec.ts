import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRoleWindowComponent } from './user-role-window.component';

describe('UserRoleWindowComponent', () => {
  let component: UserRoleWindowComponent;
  let fixture: ComponentFixture<UserRoleWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRoleWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRoleWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
