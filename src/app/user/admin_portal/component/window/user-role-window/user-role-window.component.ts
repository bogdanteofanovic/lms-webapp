import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";
import { UserRole } from 'src/model/user-role/user-role';
import { UserRoleService } from 'src/service/user-role/user-role.service';
import { RoleService } from 'src/service/role/role.service';
import { UserService } from 'src/service/user/user.service';
import {RoleWindowComponent} from '../role-window/role-window.component';
import {UserWindowComponent} from '../user-window/user-window.component';

@Component({
  selector: 'app-user-role-window',
  templateUrl: './user-role-window.component.html',
  styleUrls: ['./user-role-window.component.css']
})
export class UserRoleWindowComponent extends GenericWindow<UserRole, UserRoleService> {

  roleWindowComponent = RoleWindowComponent;
  userWindowComponent = UserWindowComponent;

  constructor(userRoleService: UserRoleService, @Optional() @Inject(MAT_DIALOG_DATA) data: UserRole,
              matDialogRef: MatDialogRef<UserRoleWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public roleService:RoleService, public userService: UserService, matSnackBar:MatSnackBar) {

    super(userRoleService, data, matDialogRef, gridEventBusService, matSnackBar, new UserRole());
  }

  init() {

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'role': [null, Validators.required],
      'user': [null, Validators.required]
    });

  }

}
