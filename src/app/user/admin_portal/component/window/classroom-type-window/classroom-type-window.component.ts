import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { ClassroomType } from 'src/model/classroom-type/classroom-type';
import { ClassroomTypeService } from 'src/service/classroom-type/classroom-type.service';

@Component({
  selector: 'app-classroom-type-window',
  templateUrl: './classroom-type-window.component.html',
  styleUrls: ['./classroom-type-window.component.css']
})
export class ClassroomTypeWindowComponent extends GenericWindow<ClassroomType, ClassroomTypeService> {

  constructor(classroomTypeService: ClassroomTypeService, @Optional() @Inject(MAT_DIALOG_DATA) data: ClassroomType,
              matDialogRef: MatDialogRef<ClassroomTypeWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, matSnackBar:MatSnackBar) {
    super(classroomTypeService, data, matDialogRef, gridEventBusService, matSnackBar, new ClassroomType());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required]
    });
  }

}
