import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassroomTypeWindowComponent } from './classroom-type-window.component';

describe('ClassroomTypeWindowComponent', () => {
  let component: ClassroomTypeWindowComponent;
  let fixture: ComponentFixture<ClassroomTypeWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassroomTypeWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomTypeWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
