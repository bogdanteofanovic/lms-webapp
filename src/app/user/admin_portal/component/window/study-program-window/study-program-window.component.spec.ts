import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramWindowComponent } from './study-program-window.component';

describe('StudyProgramWindowComponent', () => {
  let component: StudyProgramWindowComponent;
  let fixture: ComponentFixture<StudyProgramWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
