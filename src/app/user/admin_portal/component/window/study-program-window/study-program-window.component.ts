import { Component, OnInit, TypeProvider, Optional, Inject } from '@angular/core';
import { GenericWindow } from 'src/view/generic-window';
import { Observable } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { FormBuilder,Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {map, startWith} from "rxjs/operators";
import { StudyProgram } from 'src/model/study-program/study-program';
import { StudyProgramService } from 'src/service/study-program/study-program.service';
import { Faculty } from 'src/model/faculty/faculty';
import { FacultyService } from 'src/service/faculty/faculty.service';
import { FacultyWindowComponent } from '../faculty-window/faculty-window.component';
import { EmployeeWindowComponent } from '../employee-window/employee-window.component'
import { EmployeeService } from 'src/service/employee/employee.service';


@Component({
  selector: 'app-study-program-window',
  templateUrl: './study-program-window.component.html',
  styleUrls: ['./study-program-window.component.css']
})
export class StudyProgramWindowComponent extends GenericWindow<StudyProgram, StudyProgramService> {

  facultyWindowComponent = FacultyWindowComponent;
  employeeWindowComponent = EmployeeWindowComponent;

  constructor(studyProgramService: StudyProgramService, @Optional() @Inject(MAT_DIALOG_DATA) data: StudyProgram,
  matDialogRef: MatDialogRef<StudyProgramWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
  private matDialog: MatDialog, public facultyService: FacultyService, public employeeService: EmployeeService,
  matSnackBar: MatSnackBar) {
    super(studyProgramService, data, matDialogRef, gridEventBusService, matSnackBar, new StudyProgram());
   }

  init(){

  }

  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required],
      'faculty': [null, Validators.required],
      'description' : [null, Validators.required],
      'studyProgramDirector' : [null, Validators.required]
    });
  }

}
