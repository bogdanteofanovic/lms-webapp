import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-enrollment-window',
  templateUrl: './enrollment-window.component.html',
  styleUrls: ['./enrollment-window.component.css']
})
export class EnrollmentWindowComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
