import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollmentWindowComponent } from './enrollment-window.component';

describe('EnrollmentWindowComponent', () => {
  let component: EnrollmentWindowComponent;
  let fixture: ComponentFixture<EnrollmentWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollmentWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollmentWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
