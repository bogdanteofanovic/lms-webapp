import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectRealisationWindowComponent } from './subject-realisation-window.component';

describe('SubjectRealisationWindowComponent', () => {
  let component: SubjectRealisationWindowComponent;
  let fixture: ComponentFixture<SubjectRealisationWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectRealisationWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectRealisationWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
