import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";
import { SubjectRealisation } from 'src/model/subject-realisation/subject-realisation';
import { SubjectRealisationService } from 'src/service/subject-realisation/subject-realisation.service';
import { AcademicYearService } from 'src/service/academic-year/academic-year.service';
import { SubjectService } from 'src/service/subject/subject.service';
import {AcademicYearWindowComponent} from '../academic-year-window/academic-year-window.component';
import {SubjectWindowComponent} from '../subject-window/subject-window.component';
import { SubjectRealisationTypeWindowComponent } from '../subject-realisation-type-window/subject-realisation-type-window.component';
import { SubjectRealisationTypeService } from 'src/service/subject-realisation-type/subject-realisation-type.service';

@Component({
  selector: 'app-subject-realisation-window',
  templateUrl: './subject-realisation-window.component.html',
  styleUrls: ['./subject-realisation-window.component.css']
})
export class SubjectRealisationWindowComponent extends GenericWindow<SubjectRealisation, SubjectRealisationService> {

  academicYearWindowComponent = AcademicYearWindowComponent;
  subjectWindowComponent = SubjectWindowComponent;
  subjectRealisationTypeWindowComponent = SubjectRealisationTypeWindowComponent;

  constructor(subjectRealisationService: SubjectRealisationService, @Optional() @Inject(MAT_DIALOG_DATA) data: SubjectRealisation,
              matDialogRef: MatDialogRef<SubjectRealisationWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public subjectRealisationTypeService: SubjectRealisationTypeService, public academicYearService:AcademicYearService, public subjectService: SubjectService, matSnackBar:MatSnackBar) {

    super(subjectRealisationService, data, matDialogRef, gridEventBusService, matSnackBar, new SubjectRealisation());
  }

  init() {

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'academicYear': [null, Validators.required],
      'subject': [null, Validators.required],
      'subjectRealisationType': [null, Validators.required]
    });

  }

}
