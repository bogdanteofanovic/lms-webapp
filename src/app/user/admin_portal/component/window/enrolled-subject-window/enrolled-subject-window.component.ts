import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";
import { EnrolledSubject } from 'src/model/enrolled-subject/enrolled-subject';
import { EnrolledSubjectService } from 'src/service/enrolled-subject/enrolled-subject.service';
import { EnrollmentTypeService } from 'src/service/enrollment-type/enrollment-type.service';
import { StudentService } from 'src/service/student/student.service';
import { SubjectRealisationService } from 'src/service/subject-realisation/subject-realisation.service';
import {EnrollmentTypeWindowComponent} from '../enrollment-type-window/enrollment-type-window.component';
import {StudentWindowComponent} from '../student-window/student-window.component';
import {SubjectRealisationWindowComponent} from '../subject-realisation-window/subject-realisation-window.component';


@Component({
  selector: 'app-enrolled-subject-window',
  templateUrl: './enrolled-subject-window.component.html',
  styleUrls: ['./enrolled-subject-window.component.css']
})
export class EnrolledSubjectWindowComponent extends GenericWindow<EnrolledSubject, EnrolledSubjectService> {

  enrollmentTypeWindowComponent = EnrollmentTypeWindowComponent;
  studentWindowComponent = StudentWindowComponent;
  subjectRealisationWindowComponent = SubjectRealisationWindowComponent;

  constructor(enrolledSubjectService: EnrolledSubjectService, @Optional() @Inject(MAT_DIALOG_DATA) data: EnrolledSubject,
              matDialogRef: MatDialogRef<EnrolledSubjectWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public enrollmentTypeService:EnrollmentTypeService,
              public studentService: StudentService, public subjectRealisationService: SubjectRealisationService, matSnackBar:MatSnackBar) {

    super(enrolledSubjectService, data, matDialogRef, gridEventBusService, matSnackBar, new EnrolledSubject());
  }

  init() {

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'enrollmentType': [null, Validators.required],
      'student': [null, [Validators.required, Validators.minLength(4)]],
      'subjectRealisation': [null, Validators.required]
    });

  }

}
