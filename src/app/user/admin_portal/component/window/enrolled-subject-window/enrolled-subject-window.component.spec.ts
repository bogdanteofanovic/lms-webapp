import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolledSubjectWindowComponent } from './enrolled-subject-window.component';

describe('EnrolledSubjectWindowComponent', () => {
  let component: EnrolledSubjectWindowComponent;
  let fixture: ComponentFixture<EnrolledSubjectWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrolledSubjectWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolledSubjectWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
