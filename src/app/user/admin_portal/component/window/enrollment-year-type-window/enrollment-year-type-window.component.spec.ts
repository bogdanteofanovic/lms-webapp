import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollmentYearTypeWindowComponent } from './enrollment-year-type-window.component';

describe('EnrollmentYearTypeWindowComponent', () => {
  let component: EnrollmentYearTypeWindowComponent;
  let fixture: ComponentFixture<EnrollmentYearTypeWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollmentYearTypeWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollmentYearTypeWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
