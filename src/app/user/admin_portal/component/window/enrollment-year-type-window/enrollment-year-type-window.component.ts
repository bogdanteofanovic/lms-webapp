import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { EnrollmentYearType } from 'src/model/enrollment-year-type/enrollment-year-type';
import { EnrollmentYearTypeService } from 'src/service/enrollment-year-type/enrollment-year-type.service';

@Component({
  selector: 'app-enrollment-year-type-window',
  templateUrl: './enrollment-year-type-window.component.html',
  styleUrls: ['./enrollment-year-type-window.component.css']
})
export class EnrollmentYearTypeWindowComponent extends GenericWindow<EnrollmentYearType, EnrollmentYearTypeService> {


  constructor(enrollmentYearTypeService: EnrollmentYearTypeService, @Optional() @Inject(MAT_DIALOG_DATA) data: EnrollmentYearType,
              matDialogRef: MatDialogRef<EnrollmentYearTypeWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, matSnackBar:MatSnackBar) {
    super(enrollmentYearTypeService, data, matDialogRef, gridEventBusService, matSnackBar, new EnrollmentYearType());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required]
    });
  }

}
