import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcademicYearWindowComponent } from './academic-year-window.component';

describe('AcademicYearWindowComponent', () => {
  let component: AcademicYearWindowComponent;
  let fixture: ComponentFixture<AcademicYearWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcademicYearWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcademicYearWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
