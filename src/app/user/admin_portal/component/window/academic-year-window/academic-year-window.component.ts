import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";
import { AcademicYear } from 'src/model/academic-year/academic-year';
import { AcademicYearService } from 'src/service/academic-year/academic-year.service';

@Component({
  selector: 'app-academic-year-window',
  templateUrl: './academic-year-window.component.html',
  styleUrls: ['./academic-year-window.component.css']
})
export class AcademicYearWindowComponent extends GenericWindow<AcademicYear, AcademicYearService> {


  constructor(academicYearService: AcademicYearService, @Optional() @Inject(MAT_DIALOG_DATA) data: AcademicYear,
              matDialogRef: MatDialogRef<AcademicYearWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, matSnackBar:MatSnackBar) {

    super(academicYearService, data, matDialogRef, gridEventBusService, matSnackBar, new AcademicYear());
  }

  init() {

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required]
    });
  }
}
