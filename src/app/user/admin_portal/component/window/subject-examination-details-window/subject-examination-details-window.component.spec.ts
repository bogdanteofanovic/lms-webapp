import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectExaminationDetailsWindowComponent } from './subject-examination-details-window.component';

describe('SubjectExaminationDetailsWindowComponent', () => {
  let component: SubjectExaminationDetailsWindowComponent;
  let fixture: ComponentFixture<SubjectExaminationDetailsWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectExaminationDetailsWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectExaminationDetailsWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
