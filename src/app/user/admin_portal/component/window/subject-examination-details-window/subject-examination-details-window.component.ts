import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { SubjectExaminationDetails } from 'src/model/subject-examination-details/subject-examination-details';
import { SubjectExaminationDetailsService } from 'src/service/subject-examination-details/subject-examination-details.service';
import { SubjectExaminationTypeWindowComponent } from '../subject-examination-type-window/subject-examination-type-window.component';
import { SubjectRealisationWindowComponent } from '../subject-realisation-window/subject-realisation-window.component';
import { SubjectExaminationTypeService } from 'src/service/subject-examination-type/subject-examination-type.service';
import { SubjectRealisationService } from 'src/service/subject-realisation/subject-realisation.service';

@Component({
  selector: 'app-subject-examination-details-window',
  templateUrl: './subject-examination-details-window.component.html',
  styleUrls: ['./subject-examination-details-window.component.css']
})
export class SubjectExaminationDetailsWindowComponent extends GenericWindow<SubjectExaminationDetails, SubjectExaminationDetailsService> {

  subjectExaminationTypeWindowComponent = SubjectExaminationTypeWindowComponent;
  subjectRealisationWindowComponent = SubjectRealisationWindowComponent;

  constructor(subjectExaminationDetailsService: SubjectExaminationDetailsService, @Optional() @Inject(MAT_DIALOG_DATA) data: SubjectExaminationDetails,
              matDialogRef: MatDialogRef<SubjectExaminationDetailsWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public subjectExaminationTypeService:SubjectExaminationTypeService, public subjectRealisationService: SubjectRealisationService, matSnackBar:MatSnackBar) {
    super(subjectExaminationDetailsService, data, matDialogRef, gridEventBusService, matSnackBar, new SubjectExaminationDetails());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'dateAndTime': [null, Validators.required],
      'subjectExaminationType': [null, Validators.required],
      'subjectRealisation':[null, Validators.required],
      'active': [null,Validators.required]
    });
  }
}
