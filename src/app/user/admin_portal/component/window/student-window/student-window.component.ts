import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import {Student} from "../../../../../../model/student/student";
import {StudentService} from "../../../../../../service/student/student.service";
import {CountryService} from "../../../../../../service/country/country.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Country} from "../../../../../../model/country/country";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CountryWindowComponent} from "../country-window/country-window.component";
import {AddressWindowComponent} from "../address-window/address-window.component";
import {Address} from "../../../../../../model/adress/address";
import {User} from "../../../../../../model/user/user";
import {UserService} from "../../../../../../service/user/user.service";
import {map, startWith} from "rxjs/operators";
import {Observable} from "rxjs";
import { UserWindowComponent } from '../user-window/user-window.component';
import { AddressService } from 'src/service/address/address.service';

@Component({
  selector: 'app-student-window',
  templateUrl: './student-window.component.html',
  styleUrls: ['./student-window.component.css']
})
export class StudentWindowComponent extends GenericWindow<Student, StudentService>{

  userWindowComponent = UserWindowComponent;
  addressWindowComponent = AddressWindowComponent;

  constructor(studentService: StudentService, @Optional() @Inject(MAT_DIALOG_DATA) data: Student,
              matDialogRef: MatDialogRef<StudentWindowComponent>, gridEventBusService:GridEventBusService,
              private formBuilder:FormBuilder, matSnackBar:MatSnackBar,private matDialog:MatDialog, public userService:UserService, public addressService: AddressService) {
    super(studentService, data, matDialogRef, gridEventBusService, matSnackBar, new Student());
  }



  init() {

  }

  createForm(): void {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required],
      'lastName': [null, Validators.required],
      'indexNumber': [null, Validators.required],
      'address': [null, Validators.required],
      'user': [null, Validators.required]
    });
  }

}
