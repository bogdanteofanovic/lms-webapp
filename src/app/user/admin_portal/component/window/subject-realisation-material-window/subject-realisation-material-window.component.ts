import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import {SubjectRealisationMaterial} from "../../../../../../model/subject-realisation-material/subject-realisation-material";
import {SubjectRealisationMaterialService} from "../../../../../../service/subject-realisation-material/subject-realisation-material.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {FsFileWindowComponent} from "../fs-file-window/fs-file-window.component";
import {FsFileService} from "../../../../../../service/file/fs-file/fs-file.service";
import {SubjectRealisationWindowComponent} from "../subject-realisation-window/subject-realisation-window.component";
import {SubjectRealisationService} from "../../../../../../service/subject-realisation/subject-realisation.service";

@Component({
  selector: 'app-subject-realisation-material-window',
  templateUrl: './subject-realisation-material-window.component.html',
  styleUrls: ['./subject-realisation-material-window.component.css']
})
export class SubjectRealisationMaterialWindowComponent extends GenericWindow<SubjectRealisationMaterial, SubjectRealisationMaterialService> {
  subjectRealisationWindowComponent = SubjectRealisationWindowComponent;
  fsFileWindowComponent = FsFileWindowComponent;

  constructor(service: SubjectRealisationMaterialService, @Optional() @Inject(MAT_DIALOG_DATA) data: SubjectRealisationMaterial,
              matDialogRef: MatDialogRef<SubjectRealisationMaterialWindowComponent>, gridEventBusService: GridEventBusService,
              public subjectRealisationService: SubjectRealisationService, public fsFileService: FsFileService, private formBuilder: FormBuilder,
              private matDialog: MatDialog, matSnackBar: MatSnackBar) {
    super(service, data, matDialogRef, gridEventBusService, matSnackBar, new SubjectRealisationMaterial());
  }

  init() {

  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'fsFile': [null, Validators.required],
      'subjectRealisation': [null, Validators.required],
    });

  }


}
