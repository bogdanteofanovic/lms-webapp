import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectRealisationMaterialWindowComponent } from './subject-realisation-material-window.component';

describe('SubjectRealisationMaterialWindowComponent', () => {
  let component: SubjectRealisationMaterialWindowComponent;
  let fixture: ComponentFixture<SubjectRealisationMaterialWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectRealisationMaterialWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectRealisationMaterialWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
