import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { SubjectRealisationNotification } from 'src/model/subject-realisation-notification/subject-realisation-notification';
import { SubjectRealisationNotificationService } from 'src/service/subject-realisation-notification/subject-realisation-notification.service';
import { EmployeeWindowComponent } from '../employee-window/employee-window.component';
import { SubjectRealisationWindowComponent } from '../subject-realisation-window/subject-realisation-window.component';
import { EmployeeService } from 'src/service/employee/employee.service';
import { SubjectRealisationService } from 'src/service/subject-realisation/subject-realisation.service';

@Component({
  selector: 'app-subject-realisation-notification-window',
  templateUrl: './subject-realisation-notification-window.component.html',
  styleUrls: ['./subject-realisation-notification-window.component.css']
})
export class SubjectRealisationNotificationWindowComponent extends GenericWindow<SubjectRealisationNotification, SubjectRealisationNotificationService> {

  employeeWindowComponent = EmployeeWindowComponent;
  subjectRealisationWindowComponent = SubjectRealisationWindowComponent;

  constructor(subjectRealisationNotificationService: SubjectRealisationNotificationService, @Optional() @Inject(MAT_DIALOG_DATA) data: SubjectRealisationNotification,
              matDialogRef: MatDialogRef<SubjectRealisationNotificationWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public employeeService:EmployeeService, public subjectRealisationService: SubjectRealisationService, matSnackBar:MatSnackBar) {
    super(subjectRealisationNotificationService, data, matDialogRef, gridEventBusService, matSnackBar, new SubjectRealisationNotification());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'notification': [null, Validators.required],
      'publishDate': [null, Validators.required],
      'publisher':[null, Validators.required],
      'subjectRealisation':[null, Validators.required]
    });
  }
}
