import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectRealisationNotificationWindowComponent } from './subject-realisation-notification-window.component';

describe('SubjectRealisationNotificationWindowComponent', () => {
  let component: SubjectRealisationNotificationWindowComponent;
  let fixture: ComponentFixture<SubjectRealisationNotificationWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectRealisationNotificationWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectRealisationNotificationWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
