import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";
import { Role } from 'src/model/role/role';
import { RoleService } from 'src/service/role/role.service';

@Component({
  selector: 'app-role-window',
  templateUrl: './role-window.component.html',
  styleUrls: ['./role-window.component.css']
})
export class RoleWindowComponent extends GenericWindow<Role, RoleService>{

  constructor(roleService: RoleService, @Optional() @Inject(MAT_DIALOG_DATA) data: Role,
              matDialogRef: MatDialogRef<RoleWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, public countryService:RoleService, matSnackBar:MatSnackBar) {

    super(roleService, data, matDialogRef, gridEventBusService, matSnackBar, new Role());
  }

  init() {

  }

  //create form and add validators
  createForm() {
    let rolePattern: RegExp = /ROLE_([A-Z])/
    this.formGroup = this.formBuilder.group({
      'name': [null, [Validators.required, Validators.pattern(rolePattern)]]
    });

  }

}
