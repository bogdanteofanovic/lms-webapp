import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleWindowComponent } from './role-window.component';

describe('RoleWindowComponent', () => {
  let component: RoleWindowComponent;
  let fixture: ComponentFixture<RoleWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
