import { Component, OnInit, Optional, Inject } from '@angular/core';
import { GenericWindow } from 'src/view/generic-window';
import { EmployeeType } from 'src/model/employee-type/employee-type';
import { EmployeeTypeService } from 'src/service/employee-type/employee-type.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Employee } from 'src/model/employee/employee';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { FormBuilder ,Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-employee-type-window',
  templateUrl: './employee-type-window.component.html',
  styleUrls: ['./employee-type-window.component.css']
})
export class EmployeeTypeWindowComponent extends GenericWindow<EmployeeType, EmployeeTypeService> {
  
  constructor(employeeTypeService: EmployeeTypeService, @Optional() @Inject(MAT_DIALOG_DATA) data: Employee,
              matDialogRef: MatDialogRef<EmployeeTypeWindowComponent>, gridEventBusService: GridEventBusService,
              private matDialog: MatDialog, private formBuilder: FormBuilder, matSnackBar: MatSnackBar) {
    super(employeeTypeService, data, matDialogRef, gridEventBusService, matSnackBar, new EmployeeType());
               }

  init(){

  }

  //create form and add validators
createForm() {

  this.formGroup = this.formBuilder.group({
    'name': [null, Validators.required]
  });
}

}
