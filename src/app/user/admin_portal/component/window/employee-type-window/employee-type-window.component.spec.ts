import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeTypeWindowComponent } from './employee-type-window.component';

describe('EmployeeTypeWindowComponent', () => {
  let component: EmployeeTypeWindowComponent;
  let fixture: ComponentFixture<EmployeeTypeWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeTypeWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeTypeWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
