import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FsFileWindowComponent } from './fs-file-window.component';

describe('FsFileWindowComponent', () => {
  let component: FsFileWindowComponent;
  let fixture: ComponentFixture<FsFileWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FsFileWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FsFileWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
