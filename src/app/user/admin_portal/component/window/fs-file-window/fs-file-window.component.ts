import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from '../../../../../../view/generic-window';
import {FsFile} from '../../../../../../model/file/fs-file/fs-file';
import {FsFileService} from '../../../../../../service/file/fs-file/fs-file.service';
import {FormBuilder} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {StudentService} from '../../../../../../service/student/student.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import * as fileSaver from 'file-saver';

@Component({
  selector: 'app-fs-file-window',
  templateUrl: './fs-file-window.component.html',
  styleUrls: ['./fs-file-window.component.css']
})
export class FsFileWindowComponent extends GenericWindow<FsFile, FsFileService> {
  multipartFile;
  valid = false;
  file: FsFile;

  constructor(fsFileService: FsFileService, @Optional() @Inject(MAT_DIALOG_DATA) data: FsFile,
              matDialogRef: MatDialogRef<FsFileWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog: MatDialog, public studentService: StudentService, matSnackBar: MatSnackBar) {
    super(fsFileService, data, matDialogRef, gridEventBusService, matSnackBar, new FsFile());
  }

  init() {
    if (!this.add) {
      this.service.getOne(this.entity.id).subscribe(e => this.file = e);
    }
  }

  // create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({

    });

  }

  fileChange(event: any) {
    this.valid = true;
    if (event.target.files && event.target.files.length > 0) {
      this.multipartFile = event.target.files[0];
      console.log(this.multipartFile);
    }
  }

  upload() {
    const body = new FormData();
    body.append('file', this.multipartFile);
    if (this.add) {
      this.service.addOne(body).subscribe(e => this.successSubmit(e), error => this.errorSubmit(error));
    } else {
      this.service.updateOne(this.entity.id, body).subscribe(e => this.successSubmit(e), error => this.errorSubmit(error));
    }
  }

  saveFileToDisk() {
    console.log(this.entity);
    const binaryString = atob(this.file.data);
    const binaryLen = binaryString.length;
    const bytes = new Uint8Array(binaryLen);

    for (let i = 0; i < binaryLen; i++) {
      const ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }

    const blob = new Blob([bytes], {type: this.file.fileType});
    fileSaver(blob, this.file.fileName);
  }

}
