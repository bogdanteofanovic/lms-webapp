import { Component, OnInit, Optional, Inject } from '@angular/core';
import { GenericWindow } from 'src/view/generic-window';
import { User } from 'src/model/user/user';
import { UserService } from 'src/service/user/user.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { FormBuilder , Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-user-window',
  templateUrl: './user-window.component.html',
  styleUrls: ['./user-window.component.css']
})
export class UserWindowComponent extends GenericWindow<User, UserService> {

  constructor(userService: UserService, @Optional() @Inject(MAT_DIALOG_DATA) data: User,
              matDialogRef: MatDialogRef<UserWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog: MatDialog, private matSnackBar: MatSnackBar) {
    super(userService, data, matDialogRef, gridEventBusService, matSnackBar, new User());
               }

  init(){
    console.log(this.entity);
  }

   //create form and add validators
   createForm() {

    this.formGroup = this.formBuilder.group({
      'username': [null, Validators.required],
      'password': [null, Validators.required],
      'email': [null, Validators.required]
    });
  }

}
