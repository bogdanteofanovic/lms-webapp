import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffTypeWindowComponent } from './staff-type-window.component';

describe('StaffTypeWindowComponent', () => {
  let component: StaffTypeWindowComponent;
  let fixture: ComponentFixture<StaffTypeWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffTypeWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffTypeWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
