import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { StaffType } from 'src/model/staff-type/staff-type';
import { StaffTypeService } from 'src/service/staff-type/staff-type.service';

@Component({
  selector: 'app-staff-type-window',
  templateUrl: './staff-type-window.component.html',
  styleUrls: ['./staff-type-window.component.css']
})
export class StaffTypeWindowComponent extends GenericWindow<StaffType, StaffTypeService> {

  constructor(staffTypeService: StaffTypeService, @Optional() @Inject(MAT_DIALOG_DATA) data: StaffType,
              matDialogRef: MatDialogRef<StaffTypeWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog,  matSnackBar:MatSnackBar) {
    super(staffTypeService, data, matDialogRef, gridEventBusService, matSnackBar, new StaffType());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required]
    });
  }

}
