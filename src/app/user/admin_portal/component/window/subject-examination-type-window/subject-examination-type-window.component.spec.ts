import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectExaminationTypeWindowComponent } from './subject-examination-type-window.component';

describe('SubjectExaminationTypeWindowComponent', () => {
  let component: SubjectExaminationTypeWindowComponent;
  let fixture: ComponentFixture<SubjectExaminationTypeWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectExaminationTypeWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectExaminationTypeWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
