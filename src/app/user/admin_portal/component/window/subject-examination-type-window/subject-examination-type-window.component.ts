import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Component, Inject, OnInit, Optional} from '@angular/core';
import {GenericWindow} from "../../../../../../view/generic-window";
import { SubjectExaminationType } from 'src/model/subject-examination-type/subject-examination-type';
import { SubjectExaminationTypeService } from 'src/service/subject-examination-type/subject-examination-type.service';


@Component({
  selector: 'app-subject-examination-type-window',
  templateUrl: './subject-examination-type-window.component.html',
  styleUrls: ['./subject-examination-type-window.component.css']
})
export class SubjectExaminationTypeWindowComponent extends GenericWindow<SubjectExaminationType, SubjectExaminationTypeService> {

  constructor(subjectExaminationTypeService: SubjectExaminationTypeService, @Optional() @Inject(MAT_DIALOG_DATA) data: SubjectExaminationType,
              matDialogRef: MatDialogRef<SubjectExaminationTypeWindowComponent>, gridEventBusService: GridEventBusService, private formBuilder: FormBuilder,
              private matDialog:MatDialog, matSnackBar:MatSnackBar) {
    super(subjectExaminationTypeService, data, matDialogRef, gridEventBusService, matSnackBar, new SubjectExaminationType());
  }

  init() {
  }

  //create form and add validators
  createForm() {

    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required]
    });
  }

}
