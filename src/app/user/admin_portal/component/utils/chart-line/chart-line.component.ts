import {Component, Input, OnInit} from '@angular/core';
import * as HighCharts from 'highcharts';
@Component({
  selector: 'app-chart-line',
  templateUrl: './chart-line.component.html',
  styleUrls: ['./chart-line.component.css']
})
export class ChartLineComponent implements OnInit {

  title = 'Angular 9 HighCharts';
  ngOnInit(): void {
    this.barChartPopulation();
    this.pieChartBrowser();
    // this.lineChart();
    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    }, 300);
  }

  barChartPopulation() {
    HighCharts.chart('barChart', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Upis studenata po godinama'
      },
      xAxis: {
        categories: ['SII', 'IIR', 'TIH', 'PEK'],
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Upisani studenti',
          align: 'high'
        },
      },
      tooltip: {
        valueSuffix: ' upisanih'
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      series: [{
        type: undefined,
        name: 'Godina 2017',
        data: [985, 755, 635, 528]
      }, {
        type: undefined,
        name: 'Godina 2018',
        data: [1125, 892, 615, 511]
      }, {
        type: undefined,
        name: 'Godina 2019',
        data: [1280, 811, 724, 618]
      }]
    });
  }

  lineChart(){
    HighCharts.chart('lineChart', {
      chart: {
        type: 'area'
      },
      title: {
        text: 'Upis studenata po godinama'
      },
      xAxis: {
        categories: ['Godina 2017', 'Godina 2018', 'Godina 2019'],
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Upisani studenti',
          align: 'high'
        },
      },
      tooltip: {
        valueSuffix: ' upisanih'
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      series: [{
        type: undefined,
        name: 'Godina 2017',
        data: [985, 755, 635, 528]
      }, {
        type: undefined,
        name: 'Godina 2018',
        data: [1125, 892, 615, 511]
      }, {
        type: undefined,
        name: 'Godina 2019',
        data: [1280, 811, 724, 618]
      }]
    });
  }


  pieChartBrowser() {
    HighCharts.chart('pieChart', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: 'Uspešno položeni ispiti Jun 2020.'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
          }
        }
      },
      series: [{
        name: 'Uspešno položili',
        colorByPoint: true,
        type: undefined,
        data: [{
          name: 'Softversko informaciono inzenjerstvo',
          y: 61.41
        }, {
          name: 'Informacione tehnologije',
          y: 15.84
        }, {
          name: 'Turizam i hotelijerstvo',
          y: 10.85
        }, {
          name: 'Poslovna ekonomija',
          y: 4.67
        }, {
          name: 'Informatika i računarstvo',
          y: 4.18
        }]
      }]
    });

  }

}
