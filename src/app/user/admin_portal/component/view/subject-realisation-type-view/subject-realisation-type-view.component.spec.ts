import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectRealisationTypeViewComponent } from './subject-realisation-type-view.component';

describe('SubjectRealisationTypeViewComponent', () => {
  let component: SubjectRealisationTypeViewComponent;
  let fixture: ComponentFixture<SubjectRealisationTypeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectRealisationTypeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectRealisationTypeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
