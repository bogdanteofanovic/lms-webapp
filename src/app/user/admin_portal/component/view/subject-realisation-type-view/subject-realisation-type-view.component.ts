import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { SubjectRealisationType } from 'src/model/subject-realisation-type/subject-realisation-type';
import { SubjectRealisationTypeService } from 'src/service/subject-realisation-type/subject-realisation-type.service';
import { SubjectRealisationTypeWindowComponent } from '../../window/subject-realisation-type-window/subject-realisation-type-window.component';

@Component({
  selector: 'app-subject-realisation-type-view',
  templateUrl: './subject-realisation-type-view.component.html',
  styleUrls: ['./subject-realisation-type-view.component.css']
})
export class SubjectRealisationTypeViewComponent extends GenericView<SubjectRealisationType, SubjectRealisationTypeService> {

  constructor(subjectRealisationTypeService:SubjectRealisationTypeService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(subjectRealisationTypeService, SubjectRealisationTypeWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'name', title: 'Name'}

];
}

}
