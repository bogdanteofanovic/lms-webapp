import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { SubjectExaminationDetails } from 'src/model/subject-examination-details/subject-examination-details';
import { SubjectExaminationDetailsService } from 'src/service/subject-examination-details/subject-examination-details.service';
import { SubjectExaminationDetailsWindowComponent } from '../../window/subject-examination-details-window/subject-examination-details-window.component';

@Component({
  selector: 'app-subject-examination-details-view',
  templateUrl: './subject-examination-details-view.component.html',
  styleUrls: ['./subject-examination-details-view.component.css']
})
export class SubjectExaminationDetailsViewComponent extends GenericView<SubjectExaminationDetails, SubjectExaminationDetailsService> {

  constructor(subjectExaminationDetialsService:SubjectExaminationDetailsService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(subjectExaminationDetialsService, SubjectExaminationDetailsWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'dateAndTime', title: 'Date and time'},
{name: 'subjectExaminationType.name', title: 'Type'},
{name: 'active', title: 'Active'}

];
}

}
