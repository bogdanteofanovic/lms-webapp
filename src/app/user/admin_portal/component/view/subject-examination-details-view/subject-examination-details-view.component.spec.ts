import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectExaminationDetailsViewComponent } from './subject-examination-details-view.component';

describe('SubjectExaminationDetailsViewComponent', () => {
  let component: SubjectExaminationDetailsViewComponent;
  let fixture: ComponentFixture<SubjectExaminationDetailsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectExaminationDetailsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectExaminationDetailsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
