import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { UserRole } from 'src/model/user-role/user-role';
import { UserRoleService } from 'src/service/user-role/user-role.service';
import { UserRoleWindowComponent } from '../../window/user-role-window/user-role-window.component';

@Component({
  selector: 'app-user-role-view',
  templateUrl: './user-role-view.component.html',
  styleUrls: ['./user-role-view.component.css']
})
export class UserRoleViewComponent extends GenericView<UserRole, UserRoleService> {

  constructor(userRoleService:UserRoleService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
    super(userRoleService, UserRoleWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
  this.enableNew();
  this.enableEdit();
  this.enableDelete();
  this.enableExport();
}

doLast(): void {


}

initTableColumns(): ColumnDef[] {
  return [

    {name: 'id', title: 'ID'},
    {name: 'role.name', title: 'Role'},
    {name: 'user.username', title: 'User'}


    ];
}

}
