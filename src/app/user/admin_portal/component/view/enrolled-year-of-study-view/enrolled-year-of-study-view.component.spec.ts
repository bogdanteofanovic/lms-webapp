import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolledYearOfStudyViewComponent } from './enrolled-year-of-study-view.component';

describe('EnrolledYearOfStudyViewComponent', () => {
  let component: EnrolledYearOfStudyViewComponent;
  let fixture: ComponentFixture<EnrolledYearOfStudyViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrolledYearOfStudyViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolledYearOfStudyViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
