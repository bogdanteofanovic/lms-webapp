import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { EnrolledYearOfStudy } from 'src/model/enrolled-year-of-study/enrolled-year-of-study';
import { EnrolledYearOfStudyService } from 'src/service/enrolled-year-of-study/enrolled-year-of-study.service';
import { EnrolledYearOfStudyWindowComponent } from '../../window/enrolled-year-of-study-window/enrolled-year-of-study-window.component';

@Component({
  selector: 'app-enrolled-year-of-study-view',
  templateUrl: './enrolled-year-of-study-view.component.html',
  styleUrls: ['./enrolled-year-of-study-view.component.css']
})
export class EnrolledYearOfStudyViewComponent extends GenericView<EnrolledYearOfStudy, EnrolledYearOfStudyService> {

  constructor(enrolledYearOfStudyService :EnrolledYearOfStudyService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(enrolledYearOfStudyService, EnrolledYearOfStudyWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'name', title: 'Name'},
{name: 'dateOfEnrollment', title: 'Date of enrollment'},
{name: 'academicYear.name', title: 'Academic Year'},
{name: 'student_portal.name', title: 'Student'},
{name: 'enrollmentYearType.name', title:'Enrollment year type'},
{name: 'yearOfStudy.name', title: 'Year of study'}

];
}

}
