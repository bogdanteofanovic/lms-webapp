import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { SubjectExamination } from 'src/model/subject-examination/subject-examination';
import { SubjectExaminationService } from 'src/service/subject-examination/subject-examination.service';
import { SubjectExaminationWindowComponent } from '../../window/subject-examination-window/subject-examination-window.component';

@Component({
  selector: 'app-subject-examination-view',
  templateUrl: './subject-examination-view.component.html',
  styleUrls: ['./subject-examination-view.component.css']
})
export class SubjectExaminationViewComponent extends GenericView<SubjectExamination, SubjectExaminationService> {

  constructor(subjectExaminationService:SubjectExaminationService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(subjectExaminationService, SubjectExaminationWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
  this.enableNew();
  this.enableEdit();
  this.enableDelete();
  this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
  return [

    {name: 'id', title: 'ID'},
    {name: 'subjectExaminationDetails.dateAndTime', title: 'Date and Time'},
    {name: 'score', title: 'Score'},
    {name: 'student.name', title: 'Student Name'},
    {name: 'student.lastName', title: 'Student Last Name'},
    {name: 'subjectExaminationDetails.subjectRealisation.id', title: 'Subject Realisation ID'},
    {name: 'subjectExaminationDetails.subjectExaminationType.name', title: 'Examination type'},
    {name: 'student_portal.name', title: 'Student'},
    {name: 'subjectRealisation.id', title: 'Subject Realisation ID'},
    {name: 'subjectExaminationDetails.id', title: 'Subject Examination Details ID'}

    ];
}
}
