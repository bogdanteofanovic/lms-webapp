import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectExaminationViewComponent } from './subject-examination-view.component';

describe('SubjectExaminationViewComponent', () => {
  let component: SubjectExaminationViewComponent;
  let fixture: ComponentFixture<SubjectExaminationViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectExaminationViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectExaminationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
