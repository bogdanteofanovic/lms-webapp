import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleExaminationViewComponent } from './schedule-examination-view.component';

describe('ScheduleExaminationViewComponent', () => {
  let component: ScheduleExaminationViewComponent;
  let fixture: ComponentFixture<ScheduleExaminationViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleExaminationViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleExaminationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
