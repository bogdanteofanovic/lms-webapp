import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { ScheduleExamination } from 'src/model/schedule-examination/schedule-examination';
import { ScheduleExaminationService } from 'src/service/schedule-examination/schedule-examination.service';
import { ScheduleExaminationWindowComponent } from '../../window/schedule-examination-window/schedule-examination-window.component';

@Component({
  selector: 'app-schedule-examination-view',
  templateUrl: './schedule-examination-view.component.html',
  styleUrls: ['./schedule-examination-view.component.css']
})
export class ScheduleExaminationViewComponent extends GenericView<ScheduleExamination, ScheduleExaminationService> {

  constructor(scheduleExaminationService:ScheduleExaminationService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(scheduleExaminationService, ScheduleExaminationWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'beginTime', title: 'Begin'},
{name: 'endTime', title: 'End'}

];
}

}
