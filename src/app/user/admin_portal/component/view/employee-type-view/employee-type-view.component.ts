import { Component, OnInit } from '@angular/core';
import { GenericView } from 'src/view/generic-view';
import { EmployeeType } from 'src/model/employee-type/employee-type';
import { EmployeeTypeService } from 'src/service/employee-type/employee-type.service';
import { MatDialog } from '@angular/material/dialog';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { KeyEventBusService } from 'src/event/key-event-bus.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EmployeeTypeWindowComponent } from '../../window/employee-type-window/employee-type-window.component';
import { ColumnDef } from 'src/model/column-def';

@Component({
  selector: 'app-employee-type-view',
  templateUrl: './employee-type-view.component.html',
  styleUrls: ['./employee-type-view.component.css']
})
export class EmployeeTypeViewComponent extends GenericView<EmployeeType, EmployeeTypeService> {

  constructor(employeeTypeService: EmployeeTypeService, dialog: MatDialog,
              gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
              matSnackBar: MatSnackBar) { 
    super(employeeTypeService, EmployeeTypeWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);
               }

  doFirst(): void {
  this.enableNew();
  this.enableEdit();
  this.enableDelete();
  this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
  return [
    {name: 'id', title: 'ID'},
    {name: 'name', title: 'Name'},
    ];
}

}
