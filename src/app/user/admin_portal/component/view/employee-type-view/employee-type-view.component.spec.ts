import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeTypeViewComponent } from './employee-type-view.component';

describe('EmployeeTypeViewComponent', () => {
  let component: EmployeeTypeViewComponent;
  let fixture: ComponentFixture<EmployeeTypeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeTypeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeTypeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
