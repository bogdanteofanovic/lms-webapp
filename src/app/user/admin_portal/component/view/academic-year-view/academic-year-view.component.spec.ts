import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcademicYearViewComponent } from './academic-year-view.component';

describe('AcademicYearViewComponent', () => {
  let component: AcademicYearViewComponent;
  let fixture: ComponentFixture<AcademicYearViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcademicYearViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcademicYearViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
