import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { AcademicYear } from 'src/model/academic-year/academic-year';
import { AcademicYearService } from 'src/service/academic-year/academic-year.service';
import { AcademicYearWindowComponent } from '../../window/academic-year-window/academic-year-window.component';

@Component({
  selector: 'app-academic-year-view',
  templateUrl: './academic-year-view.component.html',
  styleUrls: ['./academic-year-view.component.css']
})
export class AcademicYearViewComponent extends GenericView<AcademicYear, AcademicYearService> {

  constructor(academicYearService:AcademicYearService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(academicYearService, AcademicYearWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
  this.enableNew();
  this.enableEdit();
  this.enableDelete();
  this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
  return [

    {name: 'id', title: 'ID'},
    {name: 'name', title: 'Name'}

    ];
}

}
