import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectExaminationMaterialViewComponent } from './subject-examination-material-view.component';

describe('SubjectExaminationMaterialViewComponent', () => {
  let component: SubjectExaminationMaterialViewComponent;
  let fixture: ComponentFixture<SubjectExaminationMaterialViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectExaminationMaterialViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectExaminationMaterialViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
