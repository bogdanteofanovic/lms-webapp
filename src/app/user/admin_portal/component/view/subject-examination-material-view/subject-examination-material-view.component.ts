import { Component, OnInit } from '@angular/core';
import {FsFileService} from "../../../../../../service/file/fs-file/fs-file.service";
import {MatDialog} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {FsFileWindowComponent} from "../../window/fs-file-window/fs-file-window.component";
import {GenericView} from "../../../../../../view/generic-view";
import {FsFile} from "../../../../../../model/file/fs-file/fs-file";
import {ColumnDef} from "../../../../../../model/column-def";
import {ActivatedRoute} from "@angular/router";
import {SubjectExaminationMaterial} from "../../../../../../model/subject-examination-material/subject-examination-material";
import {SubjectExaminationMaterialService} from "../../../../../../service/subject-examination-material/subject-examination-material.service";
import {SubjectExaminationMaterialWindowComponent} from "../../window/subject-examination-material-window/subject-examination-material-window.component";

@Component({
  selector: 'app-subject-examination-material-view',
  templateUrl: './subject-examination-material-view.component.html',
  styleUrls: ['./subject-examination-material-view.component.css']
})
export class SubjectExaminationMaterialViewComponent extends GenericView<SubjectExaminationMaterial, SubjectExaminationMaterialService> implements OnInit {

  constructor(service: SubjectExaminationMaterialService, dialog: MatDialog,
              gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
              matSnackBar: MatSnackBar, private activatedRoute: ActivatedRoute) {
    super(service, SubjectExaminationMaterialWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar); }

  doFirst(): void {
    this.enableNew();
    this.enableEdit();
    this.enableDelete();
    this.enableExport();
  }

  doLast(): void {

  }

  initTableColumns(): ColumnDef[] {
    return [

      {name: 'id', title: 'ID'},
      {name: 'fsFile.fileName', title: 'Name'},
      {name: 'fsFile.fileType', title: 'File type'},
      {name: 'fsFile.createdDate', title: 'Created'},
      {name: 'fsFile.filePath', title: 'File Path'},
      {name: 'fsFile.lastModifiedDate', title: 'Last modification'},
      {name: 'fsFile.user.username', title: 'Added by'}

    ];
  }

  ngOnInit(): void {
    this.keyEventBusService.attach(this);

    this.gridEventBusService.attach({name:this.service.entityName, entity:this});

    this.columnDefinitions = this.initTableColumns();

    this.doFirst();

    this.getAllForExaminationDetails();

    this.doLast();

  }

  getAllForExaminationDetails() {
    this.service.getForExaminationDetails(this.activatedRoute.snapshot.params['id']).subscribe(entities => this.dataSource.data = entities);
  }

  refreshGrid(): void {

    this.getAllForExaminationDetails();

  }

}
