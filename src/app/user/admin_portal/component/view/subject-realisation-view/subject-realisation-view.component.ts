import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { SubjectRealisation } from 'src/model/subject-realisation/subject-realisation';
import { SubjectRealisationService } from 'src/service/subject-realisation/subject-realisation.service';
import { SubjectRealisationWindowComponent } from '../../window/subject-realisation-window/subject-realisation-window.component';

@Component({
  selector: 'app-subject-realisation-view',
  templateUrl: './subject-realisation-view.component.html',
  styleUrls: ['./subject-realisation-view.component.css']
})
export class SubjectRealisationViewComponent extends GenericView<SubjectRealisation, SubjectRealisationService> {

  constructor(subjectRealisationService:SubjectRealisationService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(subjectRealisationService, SubjectRealisationWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}


doFirst(): void {
  this.enableNew();
  this.enableEdit();
  this.enableDelete();
  this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
  return [

    {name: 'id', title: 'ID'},
    {name: 'academicYear.id', title: 'Academic year ID'},
    {name: 'academicYear.name', title: 'Academic year'},
    {name: 'subject.name', title: 'Subject'},
    {name: 'subjectRealisationType.name', title: 'Type'}

    ];
}

}
