import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectRealisationViewComponent } from './subject-realisation-view.component';

describe('SubjectRealisationViewComponent', () => {
  let component: SubjectRealisationViewComponent;
  let fixture: ComponentFixture<SubjectRealisationViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectRealisationViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectRealisationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
