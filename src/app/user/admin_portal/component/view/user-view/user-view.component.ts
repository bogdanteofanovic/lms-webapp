import { Component, OnInit } from '@angular/core';
import { User } from 'src/model/user/user';
import { GenericView } from 'src/view/generic-view';
import { UserService } from 'src/service/user/user.service';
import { MatDialog } from '@angular/material/dialog';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { KeyEventBusService } from 'src/event/key-event-bus.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserWindowComponent } from '../../window/user-window/user-window.component';
import { ColumnDef } from 'src/model/column-def';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent extends GenericView<User, UserService> {

constructor(userService: UserService, dialog: MatDialog,
            gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
            matSnackBar:MatSnackBar) {
  super(userService, UserWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);
              } 

doFirst(): void {
  this.enableNew();
  this.enableEdit();
  this.enableDelete();
  this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
  return [

    {name: 'id', title: 'ID'},
    {name: 'username', title: 'Username'},
    {name: 'email', title: 'E-mail'}
    // ,{name: 'password', title: 'Password'}

    ];
}

}
