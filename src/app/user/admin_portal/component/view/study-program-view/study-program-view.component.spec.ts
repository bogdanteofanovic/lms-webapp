import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramViewComponent } from './study-program-view.component';

describe('StudyProgramViewComponent', () => {
  let component: StudyProgramViewComponent;
  let fixture: ComponentFixture<StudyProgramViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
