import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/model/employee/employee';
import { StudyProgram } from 'src/model/study-program/study-program';
import { GenericView } from 'src/view/generic-view';
import { StudyProgramService } from 'src/service/study-program/study-program.service';
import { MatDialog } from '@angular/material/dialog';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { KeyEventBusService } from 'src/event/key-event-bus.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StudyProgramWindowComponent } from '../../window/study-program-window/study-program-window.component';
import { ColumnDef } from 'src/model/column-def';

@Component({
  selector: 'app-study-program-view',
  templateUrl: './study-program-view.component.html',
  styleUrls: ['./study-program-view.component.css']
})
export class StudyProgramViewComponent extends GenericView<StudyProgram, StudyProgramService> {

  constructor(studyProgramService: StudyProgramService, dialog: MatDialog,
    gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
    matSnackBar: MatSnackBar) {
    super(studyProgramService, StudyProgramWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'name', title: 'Name'},
{name: 'description', title: 'Description'},
{name: 'faculty.name', title: 'Faculty'},
{name: 'studyProgramDirector.name', title: 'Director'}

];
}

}
