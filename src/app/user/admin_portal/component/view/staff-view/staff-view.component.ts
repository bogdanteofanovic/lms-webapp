import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { Staff } from 'src/model/staff/staff';
import { StaffService } from 'src/service/staff/staff.service';
import { StaffWindowComponent } from '../../window/staff-window/staff-window.component';

@Component({
  selector: 'app-staff-view',
  templateUrl: './staff-view.component.html',
  styleUrls: ['./staff-view.component.css']
})
export class StaffViewComponent extends GenericView<Staff, StaffService> {

  constructor(staffService:StaffService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(staffService, StaffWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'employee.name', title: 'Employee'},
{name: 'employmentStart', title: 'Employment start'},
{name: 'employmentEnd', title: 'Employment end'},
{name: 'staffType.name', title: 'Staff type'},
{name: 'studyProgram.name', title: 'Study program'},

];
}

}
