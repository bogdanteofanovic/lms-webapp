import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolledSubjectViewComponent } from './enrolled-subject-view.component';

describe('EnrolledSubjectViewComponent', () => {
  let component: EnrolledSubjectViewComponent;
  let fixture: ComponentFixture<EnrolledSubjectViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrolledSubjectViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolledSubjectViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
