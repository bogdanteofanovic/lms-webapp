import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { EnrolledSubject } from 'src/model/enrolled-subject/enrolled-subject';
import { EnrolledSubjectService } from 'src/service/enrolled-subject/enrolled-subject.service';
import { EnrolledSubjectWindowComponent } from '../../window/enrolled-subject-window/enrolled-subject-window.component';

@Component({
  selector: 'app-enrolled-subject-view',
  templateUrl: './enrolled-subject-view.component.html',
  styleUrls: ['./enrolled-subject-view.component.css']
})
export class EnrolledSubjectViewComponent extends GenericView<EnrolledSubject, EnrolledSubjectService> {

  constructor(enrolledSubjectService:EnrolledSubjectService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(enrolledSubjectService, EnrolledSubjectWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'enrollmentType.name', title: 'Enrollment type'},
{name: 'student_portal.name', title: 'Student'},
{name: 'subjectOnRealisation.id', title: 'Subject realisation ID'}

];
}

}
