import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YearOfStudyViewComponent } from './year-of-study-view.component';

describe('YearOfStudyViewComponent', () => {
  let component: YearOfStudyViewComponent;
  let fixture: ComponentFixture<YearOfStudyViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YearOfStudyViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearOfStudyViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
