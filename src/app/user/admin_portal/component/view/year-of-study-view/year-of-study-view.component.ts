import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/model/employee/employee';
import { StudyProgram } from 'src/model/study-program/study-program';
import { GenericView } from 'src/view/generic-view';
import { StudyProgramService } from 'src/service/study-program/study-program.service';
import { MatDialog } from '@angular/material/dialog';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { KeyEventBusService } from 'src/event/key-event-bus.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StudyProgramWindowComponent } from '../../window/study-program-window/study-program-window.component';
import { ColumnDef } from 'src/model/column-def';
import { YearOfStudy } from 'src/model/year-of-study/year-of-study';
import { YearOfStudyService } from 'src/service/year-of-study/year-of-study.service';
import { YearOfStudyWindowComponent } from '../../window/year-of-study-window/year-of-study-window.component';

@Component({
  selector: 'app-year-of-study-view',
  templateUrl: './year-of-study-view.component.html',
  styleUrls: ['./year-of-study-view.component.css']
})
export class YearOfStudyViewComponent  extends GenericView<YearOfStudy, YearOfStudyService> {

  constructor(yearOfStudyService: YearOfStudyService, dialog: MatDialog,
    gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
    matSnackBar: MatSnackBar) {
    super(yearOfStudyService, YearOfStudyWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);}

  doFirst(): void {
    this.enableNew();
    this.enableEdit();
    this.enableDelete();
    this.enableExport();
    }
    
    doLast(): void {
    
    }

    initTableColumns(): ColumnDef[] {
      return [
      
      {name: 'id', title: 'ID'},
      {name: 'name', title: 'Name'},
      {name: 'studyProgram.name', title: 'Study Program'}
      
      ];
      }
}
