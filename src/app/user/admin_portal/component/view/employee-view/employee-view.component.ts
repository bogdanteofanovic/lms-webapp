import { Component, OnInit } from '@angular/core';
import { GenericView } from 'src/view/generic-view';
import { Employee } from 'src/model/employee/employee';
import { EmployeeService } from 'src/service/employee/employee.service';
import { MatDialog } from '@angular/material/dialog';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { KeyEventBusService } from 'src/event/key-event-bus.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ColumnDef } from 'src/model/column-def';
import { EmployeeWindowComponent } from '../../window/employee-window/employee-window.component';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.css']
})
export class EmployeeViewComponent extends GenericView<Employee, EmployeeService>{

  constructor(employeeService: EmployeeService, dialog: MatDialog,
              gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
              matSnackBar: MatSnackBar) {
    super(employeeService, EmployeeWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);}

  doFirst(): void {
    this.enableNew();
    this.enableEdit();
    this.enableDelete();
    this.enableExport();
  }

  doLast(): void {

  }

  initTableColumns(): ColumnDef[] {
    return [

      {name: 'id', title: 'ID'},
      {name: 'name', title: 'Name'},
      {name: 'lastName', title: 'Last name'},
      {name: 'address.name', title: 'Address'},
      {name: 'employeeType.name', title: 'Type'}, //type.name
      {name: 'user.username', title: 'User'}

      ];
  }

}
