import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { ClassType } from 'src/model/class-type/class-type';
import { ClassTypeService } from 'src/service/class-type/class-type.service';
import { ClassTypeWindowComponent } from '../../window/class-type-window/class-type-window.component';

@Component({
  selector: 'app-class-type-view',
  templateUrl: './class-type-view.component.html',
  styleUrls: ['./class-type-view.component.css']
})
export class ClassTypeViewComponent extends GenericView<ClassType, ClassTypeService> {

  constructor(classTypeService:ClassTypeService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(classTypeService, ClassTypeWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'name', title: 'Name'}

];
}

}
