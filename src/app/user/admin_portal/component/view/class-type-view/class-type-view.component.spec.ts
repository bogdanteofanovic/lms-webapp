import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassTypeViewComponent } from './class-type-view.component';

describe('ClassTypeViewComponent', () => {
  let component: ClassTypeViewComponent;
  let fixture: ComponentFixture<ClassTypeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassTypeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassTypeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
