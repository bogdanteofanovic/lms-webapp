import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessorOnRealisationViewComponent } from './professor-on-realisation-view.component';

describe('ProfessorOnRealisationViewComponent', () => {
  let component: ProfessorOnRealisationViewComponent;
  let fixture: ComponentFixture<ProfessorOnRealisationViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessorOnRealisationViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessorOnRealisationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
