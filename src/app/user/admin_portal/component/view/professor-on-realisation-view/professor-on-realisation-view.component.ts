import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { ProfessorOnRealisation } from 'src/model/professor-on-realisation/professor-on-realisation';
import { ProfessorOnRealisationService } from 'src/service/professor-on-realisation/professor-on-realisation.service';
import { ProfessorOnRealisationWindowComponent } from '../../window/professor-on-realisation-window/professor-on-realisation-window.component';
@Component({
  selector: 'app-professor-on-realisation-view',
  templateUrl: './professor-on-realisation-view.component.html',
  styleUrls: ['./professor-on-realisation-view.component.css']
})
export class ProfessorOnRealisationViewComponent extends GenericView<ProfessorOnRealisation, ProfessorOnRealisationService> {

  constructor(professorOnRealisationService:ProfessorOnRealisationService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(professorOnRealisationService, ProfessorOnRealisationWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'classType.name', title: 'Class type'},
{name: 'employee.name', title: 'Professor'},
{name: 'subjectRealisation.id', title: 'Subject realisation ID'}

];
}

}
