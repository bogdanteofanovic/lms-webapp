import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffTypeViewComponent } from './staff-type-view.component';

describe('StaffTypeViewComponent', () => {
  let component: StaffTypeViewComponent;
  let fixture: ComponentFixture<StaffTypeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffTypeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffTypeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
