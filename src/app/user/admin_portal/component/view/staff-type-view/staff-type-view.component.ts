import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { StaffType } from 'src/model/staff-type/staff-type';
import { StaffTypeService } from 'src/service/staff-type/staff-type.service';
import { StaffTypeWindowComponent } from '../../window/staff-type-window/staff-type-window.component';

@Component({
  selector: 'app-staff-type-view',
  templateUrl: './staff-type-view.component.html',
  styleUrls: ['./staff-type-view.component.css']
})
export class StaffTypeViewComponent extends GenericView<StaffType, StaffTypeService> {

  constructor(staffTypeService:StaffTypeService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(staffTypeService, StaffTypeWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'name', title: 'Name'}

];
}
}
