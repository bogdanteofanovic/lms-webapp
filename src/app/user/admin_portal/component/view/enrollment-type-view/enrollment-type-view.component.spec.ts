import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollmentTypeViewComponent } from './enrollment-type-view.component';

describe('EnrollmentTypeViewComponent', () => {
  let component: EnrollmentTypeViewComponent;
  let fixture: ComponentFixture<EnrollmentTypeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollmentTypeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollmentTypeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
