import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { EnrollmentType } from 'src/model/enrollment-type/enrollment-type';
import { EnrollmentTypeService } from 'src/service/enrollment-type/enrollment-type.service';
import { EnrollmentTypeWindowComponent } from '../../window/enrollment-type-window/enrollment-type-window.component';

@Component({
  selector: 'app-enrollment-type-view',
  templateUrl: './enrollment-type-view.component.html',
  styleUrls: ['./enrollment-type-view.component.css']
})
export class EnrollmentTypeViewComponent extends GenericView<EnrollmentType, EnrollmentTypeService> {

  constructor(enrollmentTypeService:EnrollmentTypeService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(enrollmentTypeService, EnrollmentTypeWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'name', title: 'Name'}

];
}

}
