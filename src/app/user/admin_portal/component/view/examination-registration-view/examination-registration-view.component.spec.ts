import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExaminationRegistrationViewComponent } from './examination-registration-view.component';

describe('ExaminationRegistrationViewComponent', () => {
  let component: ExaminationRegistrationViewComponent;
  let fixture: ComponentFixture<ExaminationRegistrationViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExaminationRegistrationViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExaminationRegistrationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
