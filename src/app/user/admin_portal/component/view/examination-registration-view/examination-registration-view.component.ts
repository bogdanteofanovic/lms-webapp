import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { ExaminationRegistration } from 'src/model/examination-registration/examination-registration';
import { ExaminationRegistrationWindowComponent } from '../../window/examination-registration-window/examination-registration-window.component';
import {ExaminationRegistrationService} from '../../../../../../service/examination-registration/examination-registration.service';

@Component({
  selector: 'app-examination-registration-view',
  templateUrl: './examination-registration-view.component.html',
  styleUrls: ['./examination-registration-view.component.css']
})
export class ExaminationRegistrationViewComponent extends GenericView<ExaminationRegistration, ExaminationRegistrationService> {

  constructor(examinationRegistrationService:ExaminationRegistrationService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(examinationRegistrationService, ExaminationRegistrationWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'dateOfRegistration', title: 'Date of registration'},
{name: 'student_portal.name', title: 'Student'}

];
}
}
