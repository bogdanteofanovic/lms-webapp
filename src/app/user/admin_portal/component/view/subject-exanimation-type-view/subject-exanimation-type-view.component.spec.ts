import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectExanimationTypeViewComponent } from './subject-exanimation-type-view.component';

describe('SubjectExanimationTypeViewComponent', () => {
  let component: SubjectExanimationTypeViewComponent;
  let fixture: ComponentFixture<SubjectExanimationTypeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectExanimationTypeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectExanimationTypeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
