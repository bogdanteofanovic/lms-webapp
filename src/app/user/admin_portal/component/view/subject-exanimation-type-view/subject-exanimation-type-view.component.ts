import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { SubjectExaminationType } from 'src/model/subject-examination-type/subject-examination-type';
import { SubjectExaminationTypeService } from 'src/service/subject-examination-type/subject-examination-type.service';
import { SubjectExaminationTypeWindowComponent } from '../../window/subject-examination-type-window/subject-examination-type-window.component';

@Component({
  selector: 'app-subject-exanimation-type-view',
  templateUrl: './subject-exanimation-type-view.component.html',
  styleUrls: ['./subject-exanimation-type-view.component.css']
})
export class SubjectExanimationTypeViewComponent extends GenericView<SubjectExaminationType, SubjectExaminationTypeService> {

  constructor(service: SubjectExaminationTypeService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
      super(service, SubjectExaminationTypeWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

    }

    doFirst(): void {
      this.enableNew();
      this.enableEdit();
      this.enableDelete();
      this.enableExport();
    }

    doLast(): void {

    }

    initTableColumns(): ColumnDef[] {
      return [

      {name: 'id', title: 'ID'},
      {name: 'name', title: 'Name'}

      ];
    }

}
