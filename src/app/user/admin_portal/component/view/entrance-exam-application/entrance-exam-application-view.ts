import { Component, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {EntranceExamApplication} from '../../../../../../model/entrance-exam-application/entrance-exam-application';
import {EntranceExamApplicationService} from '../../../../../../service/entrance-exam-application/entrance-exam-application.service';
import {EntranceExamApplicationWindowComponent} from '../../window/entrance-exam-application/entrance-exam-application-window';
import {GenericView} from '../../../../../../view/generic-view';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {KeyEventBusService} from '../../../../../../event/key-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';

@Component({
  selector: 'app-entrance-exam-application-view',
  templateUrl: './entrance-exam-application-view.html',
  styleUrls: ['./entrance-exam-application-view.css']
})
export class EntranceExamApplicationViewComponent extends GenericView<EntranceExamApplication, EntranceExamApplicationService> {

constructor(entranceExamApplicationService: EntranceExamApplicationService, dialog: MatDialog,
            gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
            matSnackBar:MatSnackBar) {
  super(entranceExamApplicationService, EntranceExamApplicationWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);
              }

doFirst(): void {
  this.enableNew();
  this.enableEdit();
  this.enableDelete();
  this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
  return [
    {name: 'user', title: 'User'},
    {name: 'dateOfApplication', title: 'DateOfApplication'},
    {name: 'entranceExam', title: 'EntranceExam'}
    ];
}

}
