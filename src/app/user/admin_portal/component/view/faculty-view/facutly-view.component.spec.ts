import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacutlyViewComponent } from './facutly-view.component';

describe('FacutlyViewComponent', () => {
  let component: FacutlyViewComponent;
  let fixture: ComponentFixture<FacutlyViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacutlyViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacutlyViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
