import { Component, OnInit } from '@angular/core';
import { Faculty } from 'src/model/faculty/faculty';
import { FacultyService } from 'src/service/faculty/faculty.service';
import { MatDialog } from '@angular/material/dialog';
import { KeyEventBusService } from 'src/event/key-event-bus.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ColumnDef } from 'src/model/column-def';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { GenericView } from 'src/view/generic-view';
import { FacultyWindowComponent } from '../../window/faculty-window/faculty-window.component';



@Component({
  selector: 'app-facutly-view',
  templateUrl: './facutly-view.component.html',
  styleUrls: ['./facutly-view.component.css']
})
export class FacutlyViewComponent extends GenericView<Faculty, FacultyService> {

  constructor(facultyService: FacultyService, dialog: MatDialog,
              gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
              matSnackBar: MatSnackBar) {
    super(facultyService, FacultyWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);}

  doFirst(): void {
    this.enableNew();
    this.enableEdit();
    this.enableDelete();
    this.enableExport();
  }

  doLast(): void {

  }

  initTableColumns(): ColumnDef[] {
    return [

      {name: 'id', title: 'ID'},
      {name: 'name', title: 'Name'},
      {name: 'description', title: 'Description'},
      {name: 'phoneNumber', title: 'Phone #'},
      {name: 'address.name', title: 'Address'},
      {name: 'university.name', title: 'University'}

      ];
  }

}
