import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollmentYearTypeViewComponent } from './enrollment-year-type-view.component';

describe('EnrollmentYearTypeViewComponent', () => {
  let component: EnrollmentYearTypeViewComponent;
  let fixture: ComponentFixture<EnrollmentYearTypeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollmentYearTypeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollmentYearTypeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
