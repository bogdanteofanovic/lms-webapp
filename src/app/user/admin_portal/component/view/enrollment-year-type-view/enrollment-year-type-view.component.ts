import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { EnrollmentYearType } from 'src/model/enrollment-year-type/enrollment-year-type';
import { EnrollmentYearTypeService } from 'src/service/enrollment-year-type/enrollment-year-type.service';
import { EnrollmentYearTypeWindowComponent } from '../../window/enrollment-year-type-window/enrollment-year-type-window.component';

@Component({
  selector: 'app-enrollment-year-type-view',
  templateUrl: './enrollment-year-type-view.component.html',
  styleUrls: ['./enrollment-year-type-view.component.css']
})
export class EnrollmentYearTypeViewComponent extends GenericView<EnrollmentYearType, EnrollmentYearTypeService>{

  constructor(enrollmentYearTypeService:EnrollmentYearTypeService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(enrollmentYearTypeService, EnrollmentYearTypeWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'name', title: 'Name'}

];
}

}
