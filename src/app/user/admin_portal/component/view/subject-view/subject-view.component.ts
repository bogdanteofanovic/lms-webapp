import { Component, OnInit } from '@angular/core';
import { GenericView } from 'src/view/generic-view';

import { SubjectService } from 'src/service/subject/subject.service';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { SubjectWindowComponent } from '../../window/subject-window/subject-window.component';
import { Subject } from 'src/model/subject/subject';
@Component({
  selector: 'app-subject-view',
  templateUrl: './subject-view.component.html',
  styleUrls: ['./subject-view.component.css']
})
export class SubjectViewComponent extends GenericView<Subject,SubjectService> {

  constructor(subjectService: SubjectService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
      super(subjectService, SubjectWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);
    }

doFirst(): void {
  this.enableNew();
  this.enableEdit();
  this.enableDelete();
  this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
  return [

    {name: 'id', title: 'ID'},
    {name: 'name', title: 'Name'},
    {name: 'syllabus', title: 'Syllabus'},
    {name: 'score', title: 'Score'},
    {name: 'yearOfStudy.name', title: 'Year of study'}

    ];
}

}
