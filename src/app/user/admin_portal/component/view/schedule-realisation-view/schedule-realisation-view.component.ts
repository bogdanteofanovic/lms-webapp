import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { ScheduleRealisation } from 'src/model/schedule-realisation/schedule-realisation';
import { ScheduleRealisationService } from 'src/service/schedule-realisation/schedule-realisation.service';
import { ScheduleRealisationWindowComponent } from '../../window/schedule-realisation-window/schedule-realisation-window.component';

@Component({
  selector: 'app-schedule-realisation-view',
  templateUrl: './schedule-realisation-view.component.html',
  styleUrls: ['./schedule-realisation-view.component.css']
})
export class ScheduleRealisationViewComponent extends GenericView<ScheduleRealisation, ScheduleRealisationService> {

  constructor(scheduleRealisationService:ScheduleRealisationService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(scheduleRealisationService, ScheduleRealisationWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'startTime', title: 'Start'},
{name: 'endTime', title: 'End'},
{name: 'classroom.name', title: 'Classroom'}

];
}

}
