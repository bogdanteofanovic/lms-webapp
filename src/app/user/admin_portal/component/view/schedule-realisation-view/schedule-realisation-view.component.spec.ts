import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleRealisationViewComponent } from './schedule-realisation-view.component';

describe('ScheduleRealisationViewComponent', () => {
  let component: ScheduleRealisationViewComponent;
  let fixture: ComponentFixture<ScheduleRealisationViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleRealisationViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleRealisationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
