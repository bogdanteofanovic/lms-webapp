import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {Country} from '../../../../../../model/country/country';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {CountryWindowComponent} from '../../window/country-window/country-window.component';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { CountryService } from 'src/service/country/country.service';

@Component({
  selector: 'app-country-view',
  templateUrl: './country-view.component.html',
  styleUrls: ['./country-view.component.css']
})

export class CountryViewComponent extends GenericView<Country, CountryService>{

  constructor(countryService:CountryService, dialog:MatDialog,
              gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
              matSnackBar:MatSnackBar) {
    super(countryService, CountryWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

  }

  doFirst(): void {

    this.enableNew();
    this.enableEdit();
    this.enableDelete();
    this.enableExport();

  }

  doLast(): void {
  }

  initTableColumns(): ColumnDef[] {
    return [
      {name: 'id', title: 'ID'},
      {name: 'name', title: 'Country Name'}

    ];
  }


}
