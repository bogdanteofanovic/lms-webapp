import { Component, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {EntranceExam} from '../../../../../../model/entrance-exam/entrance-exam';
import {EntranceExamService} from '../../../../../../service/entrance-exam/entrance-exam.service';
import {EntranceExamWindowComponent} from '../../window/entrance-exam/entrance-exam-window';
import {GenericView} from '../../../../../../view/generic-view';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {KeyEventBusService} from '../../../../../../event/key-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';

@Component({
  selector: 'app-entrance-exam-view',
  templateUrl: './entrance-exam-view.html',
  styleUrls: ['./entrance-exam-view.css']
})
export class EntranceExamViewComponent extends GenericView<EntranceExam, EntranceExamService> {

constructor(entranceExamService: EntranceExamService, dialog: MatDialog,
            gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
            matSnackBar:MatSnackBar) {
  super(entranceExamService, EntranceExamWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);
              }

doFirst(): void {
  this.enableNew();
  this.enableEdit();
  this.enableDelete();
  this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
  return [
    {name: 'academicYear.name', title: 'AcademicYear'},
    {name: 'startDate', title: 'StartDate'},
    {name: 'endDate', title: 'EndDate'},
    {name: 'studyProgram.name', title: 'StudyProgram'}
    ];
}

}
