import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { ClassroomType } from 'src/model/classroom-type/classroom-type';
import { ClassroomTypeService } from 'src/service/classroom-type/classroom-type.service';
import { ClassroomTypeWindowComponent } from '../../window/classroom-type-window/classroom-type-window.component';

@Component({
  selector: 'app-classroom-type-view',
  templateUrl: './classroom-type-view.component.html',
  styleUrls: ['./classroom-type-view.component.css']
})
export class ClassroomTypeViewComponent extends GenericView<ClassroomType, ClassroomTypeService> {

  constructor(classroomTypeService:ClassroomTypeService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(classroomTypeService, ClassroomTypeWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'name', title: 'Name'}

];
}

}
