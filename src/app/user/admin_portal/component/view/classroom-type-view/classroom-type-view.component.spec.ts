import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassroomTypeViewComponent } from './classroom-type-view.component';

describe('ClassroomTypeViewComponent', () => {
  let component: ClassroomTypeViewComponent;
  let fixture: ComponentFixture<ClassroomTypeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassroomTypeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomTypeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
