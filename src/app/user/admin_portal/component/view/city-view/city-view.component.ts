import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";

import { CityService } from 'src/service/city/city.service';
import {CityWindowComponent} from '../../window/city-window/city-window.component';
import {City} from '../../../../../../model/city/city';
@Component({
  selector: 'app-city-view',
  templateUrl: './city-view.component.html',
  styleUrls: ['./city-view.component.css']
})
export class CityViewComponent extends GenericView<City, CityService>{

  constructor(cityService:CityService, dialog:MatDialog,
              gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
              matSnackBar:MatSnackBar) {
    super(cityService, CityWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

  }

  doFirst(): void {
    this.enableNew();
    this.enableEdit();
    this.enableDelete();
    this.enableExport();
  }

  doLast(): void {

  }

  initTableColumns(): ColumnDef[] {
    return [

      {name: 'id', title: 'ID'},
      {name: 'name', title: 'Name'},
      {name: 'zipCode', title: 'Zip code'},
      {name: 'country.name', title: 'Country Name'}

      ];
  }

}
