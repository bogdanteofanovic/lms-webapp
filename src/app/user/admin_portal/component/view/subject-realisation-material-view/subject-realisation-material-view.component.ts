import { Component, OnInit } from '@angular/core';
import {GenericView} from "../../../../../../view/generic-view";
import {MatDialog} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ActivatedRoute} from "@angular/router";
import {ColumnDef} from "../../../../../../model/column-def";
import {SubjectRealisationMaterialService} from "../../../../../../service/subject-realisation-material/subject-realisation-material.service";
import {SubjectRealisationMaterial} from "../../../../../../model/subject-realisation-material/subject-realisation-material";
import {SubjectRealisationMaterialWindowComponent} from '../../window/subject-realisation-material-window/subject-realisation-material-window.component';


@Component({
  selector: 'app-subject-realisation-material-view',
  templateUrl: './subject-realisation-material-view.component.html',
  styleUrls: ['./subject-realisation-material-view.component.css']
})
export class SubjectRealisationMaterialViewComponent extends GenericView<SubjectRealisationMaterial, SubjectRealisationMaterialService> implements OnInit {

  constructor(service: SubjectRealisationMaterialService, dialog: MatDialog,
              gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
              matSnackBar: MatSnackBar, private activatedRoute: ActivatedRoute) {
    super(service, SubjectRealisationMaterialWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar); }

  doFirst(): void {
    this.enableNew();
    this.enableEdit();
    this.enableDelete();
    this.enableExport();
  }

  doLast(): void {

  }

  initTableColumns(): ColumnDef[] {
    return [

      {name: 'id', title: 'ID'},
      {name: 'fsFile.fileName', title: 'File Name'},
      {name: 'fsFile.fileType', title: 'File type'},
      {name: 'fsFile.createdDate', title: 'Created'},
      {name: 'fsFile.filePath', title: 'File Path'},
      {name: 'fsFile.lastModifiedDate', title: 'Last modification'},
      {name: 'fsFile.user.username', title: 'Added by'},
      {name: 'subjectRealisation.id', title: 'Realisation ID'},

    ];
  }

  ngOnInit(): void {
    this.keyEventBusService.attach(this);

    this.gridEventBusService.attach({name:this.service.entityName, entity:this});

    this.columnDefinitions = this.initTableColumns();

    this.doFirst();

    this.getAllForRealisation();

    this.doLast();

  }

  getAllForRealisation() {
    this.service.getForRealisation(this.activatedRoute.snapshot.params['id']).subscribe(entities => this.dataSource.data = entities);
  }

  refreshGrid(): void {

    this.getAllForRealisation();

  }


}
