import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectRealisationMaterialViewComponent } from './subject-realisation-material-view.component';

describe('SubjectRealisationMaterialViewComponent', () => {
  let component: SubjectRealisationMaterialViewComponent;
  let fixture: ComponentFixture<SubjectRealisationMaterialViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectRealisationMaterialViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectRealisationMaterialViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
