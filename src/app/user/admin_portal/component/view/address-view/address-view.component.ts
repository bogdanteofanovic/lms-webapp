import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {Address} from '../../../../../../model/adress/address';
import {AddressService} from '../../../../../../service/address/address.service';
import {CityService} from '../../../../../../service/city/city.service';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {CityWindowComponent} from '../../window/city-window/city-window.component';
import {AddressWindowComponent} from '../../window/address-window/address-window.component';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from '../../../../../../event/key-event-bus.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AuthenticationService} from '../../../../../../service/security/authentication.service';

@Component({
  selector: 'app-address-view',
  templateUrl: './address-view.component.html',
  styleUrls: ['./address-view.component.css']
})
export class AddressViewComponent extends GenericView<Address, AddressService> {

  constructor(addressService: AddressService, dialog: MatDialog,
              gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
              matSnackBar: MatSnackBar, private authenticationService: AuthenticationService) {
    super(addressService, AddressWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar );

  }

  doFirst(): void {

    this.enableNew();
    this.enableEdit();
    this.enableDelete();

    if (this.authenticationService.isAdmin()) {
      this.enableNew();
      this.enableEdit();
    }

    if (this.authenticationService.isUser()) {
      this.enableDelete();
    }
    if(this.authenticationService.isAdmin()){

    }


    this.enableExport();
  }

  doLast(): void {

  }

  initTableColumns(): ColumnDef[] {
    return [
      {name: 'id', title: 'ID'},
      {name: 'name', title: 'Name'},
      {name: 'number', title: 'Number'},
      {name: 'city.name', title: 'City name'},
      {name: 'city.country.name', title: 'Country'}];
  }

}
