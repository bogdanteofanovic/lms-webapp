import { Component, OnInit } from '@angular/core';
import {GenericView} from "../../../../../../view/generic-view";
import {Student} from "../../../../../../model/student/student";
import {StudentService} from "../../../../../../service/student/student.service";
import {MatDialog} from "@angular/material/dialog";
import {GridEventBusService} from "../../../../../../event/grid-event-bus.service";
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {StudentWindowComponent} from "../../window/student-window/student-window.component";
import {ColumnDef} from "../../../../../../model/column-def";
import {AuthenticationService} from "../../../../../../service/security/authentication.service";

@Component({
  selector: 'app-student-view',
  templateUrl: './student-view.component.html',
  styleUrls: ['./student-view.component.css']
})
export class StudentViewComponent extends GenericView<Student, StudentService>{

  constructor(studentService:StudentService, dialog:MatDialog,
              gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
              matSnackBar:MatSnackBar) {
    super(studentService, StudentWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

  }


  doFirst(): void {

    this.enableNew();
    this.enableEdit();
    this.enableDelete();
    this.enableExport();

  }

  doLast(): void {



  }

  initTableColumns(): ColumnDef[] {
    return [

      {name: 'id', title: 'ID'},
      {name: 'name', title: 'Name'},
      {name: 'lastName', title: 'Last name'},
      {name: 'user.username', title: 'Username'}

    ];
  }


}
