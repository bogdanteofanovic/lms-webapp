import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FsFileViewComponent } from './fs-file-view.component';

describe('FsFileViewComponent', () => {
  let component: FsFileViewComponent;
  let fixture: ComponentFixture<FsFileViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FsFileViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FsFileViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
