import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {FsFile} from '../../../../../../model/file/fs-file/fs-file';
import {FsFileService} from '../../../../../../service/file/fs-file/fs-file.service';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {KeyEventBusService} from '../../../../../../event/key-event-bus.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ColumnDef} from '../../../../../../model/column-def';
import {FsFileWindowComponent} from '../../window/fs-file-window/fs-file-window.component';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-fs-file-view',
  templateUrl: './fs-file-view.component.html',
  styleUrls: ['./fs-file-view.component.css']
})
export class FsFileViewComponent extends GenericView<FsFile, FsFileService> implements OnInit{

  constructor(fsFileService: FsFileService, dialog: MatDialog,
              gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
              matSnackBar: MatSnackBar) {
    super(fsFileService, FsFileWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar); }

  doFirst(): void {
    this.enableNew();
    this.enableEdit();
    this.enableDelete();
    this.enableExport();
  }

  doLast(): void {

  }

  initTableColumns(): ColumnDef[] {
    return [

      {name: 'id', title: 'ID'},
      {name: 'fileName', title: 'Name'},
      {name: 'fileType', title: 'File type'},
      {name: 'createdDate', title: 'Created'},
      {name: 'filePath', title: 'File Path'},
      {name: 'lastModifiedDate', title: 'Last modification'},
      {name: 'user.username', title: 'Added by'}

    ];
  }

  ngOnInit(): void {
    this.getAll();

    this.keyEventBusService.attach(this);

    this.gridEventBusService.attach({name:this.service.entityName, entity:this});

    this.columnDefinitions = this.initTableColumns();

    this.doFirst();

    this.doLast();
  }

}
