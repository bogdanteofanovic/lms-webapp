import { Component, OnInit } from '@angular/core';
import { GenericView } from 'src/view/generic-view';
import { University } from 'src/model/university/university';
import { UniversityService } from 'src/service/university/university.service';
import { MatDialog } from '@angular/material/dialog';
import { GridEventBusService } from 'src/event/grid-event-bus.service';
import { KeyEventBusService } from 'src/event/key-event-bus.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ColumnDef } from 'src/model/column-def';
import { UniversityWindowComponent } from '../../window/university-window/university-window.component';

@Component({
  selector: 'app-university-view',
  templateUrl: './university-view.component.html',
  styleUrls: ['./university-view.component.css']
})
export class UniversityViewComponent extends GenericView<University, UniversityService> {

  constructor(universityService: UniversityService, dialog:MatDialog,
              gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService,
              matSnackBar: MatSnackBar) {
    super( universityService, UniversityWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);
  }

  doFirst(): void {
    this.enableNew();
    this.enableEdit();
    this.enableDelete();
    this.enableExport();
  }

  doLast(): void {

  }


  initTableColumns(): ColumnDef[] {
    return [

      {name: 'id', title: 'ID'},
      {name: 'name', title: 'Name'},
      {name: 'address.name', title: 'Address'}

      ];
  }

}
