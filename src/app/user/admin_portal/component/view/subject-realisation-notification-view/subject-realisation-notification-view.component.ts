import { Component, OnInit } from '@angular/core';
import {GenericView} from '../../../../../../view/generic-view';
import {MatDialog} from '@angular/material/dialog';
import {GridEventBusService} from '../../../../../../event/grid-event-bus.service';
import {ColumnDef} from '../../../../../../model/column-def';
import {KeyEventBusService} from "../../../../../../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { SubjectRealisationNotification } from 'src/model/subject-realisation-notification/subject-realisation-notification';
import { SubjectRealisationNotificationService } from 'src/service/subject-realisation-notification/subject-realisation-notification.service';
import { SubjectRealisationNotificationWindowComponent } from '../../window/subject-realisation-notification-window/subject-realisation-notification-window.component';

@Component({
  selector: 'app-subject-realisation-notification-view',
  templateUrl: './subject-realisation-notification-view.component.html',
  styleUrls: ['./subject-realisation-notification-view.component.css']
})
export class SubjectRealisationNotificationViewComponent extends GenericView<SubjectRealisationNotification, SubjectRealisationNotificationService> {

  constructor(subjectRealisationNotificationService:SubjectRealisationNotificationService, dialog:MatDialog,
    gridEventBusService:GridEventBusService, keyEventBusService:KeyEventBusService,
    matSnackBar:MatSnackBar) {
super(subjectRealisationNotificationService, SubjectRealisationNotificationWindowComponent, dialog, gridEventBusService, keyEventBusService, matSnackBar);

}

doFirst(): void {
this.enableNew();
this.enableEdit();
this.enableDelete();
this.enableExport();
}

doLast(): void {

}

initTableColumns(): ColumnDef[] {
return [

{name: 'id', title: 'ID'},
{name: 'notification', title: 'Notification'},
{name: 'publishDate', title: 'Publish date'},
{name: 'publisher.name', title: 'Publisher'}

];
}

}
