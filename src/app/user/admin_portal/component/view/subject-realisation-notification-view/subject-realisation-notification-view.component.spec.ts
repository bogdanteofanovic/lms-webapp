import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectRealisationNotificationViewComponent } from './subject-realisation-notification-view.component';

describe('SubjectRealisationNotificationViewComponent', () => {
  let component: SubjectRealisationNotificationViewComponent;
  let fixture: ComponentFixture<SubjectRealisationNotificationViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectRealisationNotificationViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectRealisationNotificationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
