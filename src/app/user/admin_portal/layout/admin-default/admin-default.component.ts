import {Component, HostListener, Input, OnDestroy, OnInit} from '@angular/core';
import {KeyEventBusService} from '../../../../../event/key-event-bus.service';
import {UserLayoutDefault} from '../../../user_shared/user-layout-default';

@Component({
  selector: 'app-admin-default',
  templateUrl: './admin-default.component.html',
  styleUrls: ['./admin-default.component.css']
})
export class AdminDefaultComponent extends UserLayoutDefault {

  constructor(keyEventBus: KeyEventBusService) {
    super(keyEventBus);
  }
}
