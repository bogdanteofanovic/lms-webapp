import {Component, EventEmitter, HostListener, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmationDialogComponent} from '../../../../utils/confirmation-dialog/confirmation-dialog.component';

import {AuthenticationService} from '../../../../../service/security/authentication.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {



  @Output()
  emmitCloseSidebar: EventEmitter<any> = new EventEmitter<any>();

  constructor(protected matDialog: MatDialog, protected authenticationService: AuthenticationService) { }

  ngOnInit(): void {

  }

  logout() {
    this.authenticationService.logout();
  }

  showInformationAboutApp() {
    this.matDialog.open(ConfirmationDialogComponent, {data: {type: 'info', message: 'LMS Application v1.0 beta'}});
  }

}
