import {KeyEventBusService} from '../../../event/key-event-bus.service';
import {KeyEventBus} from '../../../model/key-event-bus';
import {HostListener, OnDestroy, OnInit} from '@angular/core';


export class UserLayoutDefault implements OnInit, OnDestroy, KeyEventBus {
  openSidebar = true;

  constructor(protected keyEventBus: KeyEventBusService) {

  }


  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    this.keyEventBus.notify(event);
  }

  ngOnInit(): void {
    this.keyEventBus.attach(this);
  }


  openSidebarFn() {
    this.openSidebar = !this.openSidebar;
  }

  onKeyEvent(event: KeyboardEvent) {
    if (event.altKey && event.key === 's') {
      this.openSidebarFn();
    }
  }

  ngOnDestroy(): void {
    this.keyEventBus.dettach(this);
  }
}
