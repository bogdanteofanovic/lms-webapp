import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SudentHeaderComponent } from './sudent-header.component';

describe('SudentHeaderComponent', () => {
  let component: SudentHeaderComponent;
  let fixture: ComponentFixture<SudentHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SudentHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SudentHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
