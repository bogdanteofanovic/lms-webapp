import { Component, OnInit } from '@angular/core';
import {HeaderComponent} from '../../../admin_portal/layout/header/header.component';
import {MatDialog} from '@angular/material/dialog';
import {AuthenticationService} from '../../../../../service/security/authentication.service';

@Component({
  selector: 'app-sudent-header',
  templateUrl: './sudent-header.component.html',
  styleUrls: ['./sudent-header.component.css']
})
export class SudentHeaderComponent extends HeaderComponent {

  constructor(matDialog: MatDialog, authenticationService: AuthenticationService) {
    super(matDialog, authenticationService);
  }



}
