import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SudentSidebarComponent } from './sudent-sidebar.component';

describe('SudentSidebarComponent', () => {
  let component: SudentSidebarComponent;
  let fixture: ComponentFixture<SudentSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SudentSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SudentSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
