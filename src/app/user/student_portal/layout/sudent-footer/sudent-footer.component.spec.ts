import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SudentFooterComponent } from './sudent-footer.component';

describe('SudentFooterComponent', () => {
  let component: SudentFooterComponent;
  let fixture: ComponentFixture<SudentFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SudentFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SudentFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
