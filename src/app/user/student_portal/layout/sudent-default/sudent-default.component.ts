import { Component, OnInit } from '@angular/core';
import {KeyEventBusService} from '../../../../../event/key-event-bus.service';
import {UserLayoutDefault} from '../../../user_shared/user-layout-default';

@Component({
  selector: 'app-sudent-default',
  templateUrl: './sudent-default.component.html',
  styleUrls: ['./sudent-default.component.css']
})
export class SudentDefaultComponent extends UserLayoutDefault {

  constructor(keyEventBus: KeyEventBusService) {
    super(keyEventBus);
  }

}
