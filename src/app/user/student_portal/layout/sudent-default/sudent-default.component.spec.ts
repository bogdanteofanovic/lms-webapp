import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SudentDefaultComponent } from './sudent-default.component';

describe('SudentDefaultComponent', () => {
  let component: SudentDefaultComponent;
  let fixture: ComponentFixture<SudentDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SudentDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SudentDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
