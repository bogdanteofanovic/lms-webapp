import { Component, OnInit } from '@angular/core';
import {KeyEventBusService} from '../../../../../event/key-event-bus.service';
import {UserLayoutDefault} from '../../../user_shared/user-layout-default';

@Component({
  selector: 'app-professor-default',
  templateUrl: './professor-default.component.html',
  styleUrls: ['./professor-default.component.css']
})
export class ProfessorDefaultComponent extends UserLayoutDefault {

  constructor(keyEventBus: KeyEventBusService) {
    super(keyEventBus);
  }

}
