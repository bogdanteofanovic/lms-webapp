import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessorDefaultComponent } from './professor-default.component';

describe('ProfessorDefaultComponent', () => {
  let component: ProfessorDefaultComponent;
  let fixture: ComponentFixture<ProfessorDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessorDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessorDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
