import { Component, OnInit } from '@angular/core';
import {HeaderComponent} from '../../../admin_portal/layout/header/header.component';
import {MatDialog} from '@angular/material/dialog';
import {AuthenticationService} from '../../../../../service/security/authentication.service';

@Component({
  selector: 'app-professor-header',
  templateUrl: './professor-header.component.html',
  styleUrls: ['./professor-header.component.css']
})
export class ProfessorHeaderComponent extends HeaderComponent {

  constructor(matDialog: MatDialog, authenticationService: AuthenticationService) {
    super(matDialog, authenticationService);
  }


}
