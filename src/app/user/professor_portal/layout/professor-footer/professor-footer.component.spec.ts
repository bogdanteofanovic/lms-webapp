import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessorFooterComponent } from './professor-footer.component';

describe('ProfessorFooterComponent', () => {
  let component: ProfessorFooterComponent;
  let fixture: ComponentFixture<ProfessorFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessorFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessorFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
