import { Component, OnInit } from '@angular/core';
import {UserLayoutDefault} from '../../../user_shared/user-layout-default';
import {KeyEventBusService} from '../../../../../event/key-event-bus.service';

@Component({
  selector: 'app-administration-default',
  templateUrl: './administration-default.component.html',
  styleUrls: ['./administration-default.component.css']
})
export class AdministrationDefaultComponent extends UserLayoutDefault {

  constructor(keyEventBus: KeyEventBusService) {
    super(keyEventBus);
  }
}
