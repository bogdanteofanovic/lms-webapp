import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationDefaultComponent } from './administration-default.component';

describe('AdministrationDefaultComponent', () => {
  let component: AdministrationDefaultComponent;
  let fixture: ComponentFixture<AdministrationDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
