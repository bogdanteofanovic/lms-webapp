import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AuthenticationService} from '../../../../../service/security/authentication.service';
import {ConfirmationDialogComponent} from '../../../../utils/confirmation-dialog/confirmation-dialog.component';
import {HeaderComponent} from '../../../admin_portal/layout/header/header.component';

@Component({
  selector: 'app-administration-header',
  templateUrl: './administration-header.component.html',
  styleUrls: ['./administration-header.component.css']
})
export class AdministrationHeaderComponent extends HeaderComponent{



}
