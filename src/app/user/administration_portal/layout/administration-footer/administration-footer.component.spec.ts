import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationFooterComponent } from './administration-footer.component';

describe('AdministrationFooterComponent', () => {
  let component: AdministrationFooterComponent;
  let fixture: ComponentFixture<AdministrationFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
