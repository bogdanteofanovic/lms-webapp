import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { EnrollmentType } from 'src/model/enrollment-type/enrollment-type';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EnrollmentTypeService extends GenericService<EnrollmentType>{

  constructor(httpClient: HttpClient) { super(httpClient, "enrollmentType"); }
}
