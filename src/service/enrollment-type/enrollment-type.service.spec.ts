import { TestBed } from '@angular/core/testing';

import { EnrollmentTypeService } from './enrollment-type.service';

describe('EnrollmentTypeService', () => {
  let service: EnrollmentTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnrollmentTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
