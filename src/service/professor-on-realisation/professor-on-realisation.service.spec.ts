import { TestBed } from '@angular/core/testing';

import { ProfessorOnRealisationService } from './professor-on-realisation.service';

describe('ProfessorOnRealisationService', () => {
  let service: ProfessorOnRealisationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfessorOnRealisationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
