import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { ProfessorOnRealisation } from 'src/model/professor-on-realisation/professor-on-realisation';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfessorOnRealisationService extends GenericService<ProfessorOnRealisation>{

  constructor(httpClient: HttpClient) { super(httpClient, "professorOnRealisation"); }
}
