import { Injectable } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {GenericService} from '../generic-service/generic-service';
import {EntranceExamApplication} from '../../model/entrance-exam-application/entrance-exam-application';
import {EntranceExam} from '../../model/entrance-exam/entrance-exam';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EntranceExamApplicationService extends GenericService<EntranceExamApplication>{

  constructor(httpClient: HttpClient) {
        super(httpClient, "entranceExamApplication");

  }

  applyForEntranceExam(entranceExam:EntranceExam){
    this.httpClient.post<any>(this.apiUrl + '/' + this.entityName + '/apply' , entranceExam);
  }

}
