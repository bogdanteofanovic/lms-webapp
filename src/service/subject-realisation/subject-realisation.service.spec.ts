import { TestBed } from '@angular/core/testing';

import { SubjectRealisationService } from './subject-realisation.service';

describe('SubjectRealisationService', () => {
  let service: SubjectRealisationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubjectRealisationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
