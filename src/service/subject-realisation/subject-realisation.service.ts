import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { SubjectRealisation } from 'src/model/subject-realisation/subject-realisation';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubjectRealisationService extends GenericService<SubjectRealisation>{

  constructor(httpClient: HttpClient) { super(httpClient, "subjectRealisation"); }
}
