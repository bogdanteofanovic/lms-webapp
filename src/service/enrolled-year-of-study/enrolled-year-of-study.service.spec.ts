import { TestBed } from '@angular/core/testing';

import { EnrolledYearOfStudyService } from './enrolled-year-of-study.service';

describe('EnrolledYearOfStudyService', () => {
  let service: EnrolledYearOfStudyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnrolledYearOfStudyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
