import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { EnrolledYearOfStudy } from 'src/model/enrolled-year-of-study/enrolled-year-of-study';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EnrolledYearOfStudyService extends GenericService<EnrolledYearOfStudy>{

  constructor(httpClient: HttpClient) { super(httpClient, "enrolledYearOfStudy"); }
}
