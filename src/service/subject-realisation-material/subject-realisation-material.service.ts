import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { SubjectRealisationMaterial } from 'src/model/subject-realisation-material/subject-realisation-material';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs";
import {FsFile} from "../../model/file/fs-file/fs-file";

@Injectable({
  providedIn: 'root'
})
export class SubjectRealisationMaterialService extends GenericService<SubjectRealisationMaterial>{

  constructor(httpClient: HttpClient) { super(httpClient, 'subjectRealisationMaterial'); }

  getForRealisation(id: number): Observable<SubjectRealisationMaterial[]> {
    return this.httpClient.get<SubjectRealisationMaterial[]>(this.apiUrl + '/' + this.entityName + '/subjectRealisation/' + id);
  }

}
