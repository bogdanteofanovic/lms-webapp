import { TestBed } from '@angular/core/testing';

import { SubjectRealisationMaterialService } from './subject-realisation-material.service';

describe('SubjectRealisationMaterialService', () => {
  let service: SubjectRealisationMaterialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubjectRealisationMaterialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
