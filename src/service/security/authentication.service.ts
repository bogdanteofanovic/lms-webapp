import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ConfirmationDialogComponent} from '../../app/utils/confirmation-dialog/confirmation-dialog.component';
import {Route, Router} from '@angular/router';
import {LoginComponent} from '../../app/public/component/login/login.component';
import * as jwt_decode from 'jwt-decode';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private loginApi = 'http://localhost:8080/login/authenticate';
  private registerApi = 'http://localhost:8080/login/register';

  private token;

  dialogRef: MatDialogRef<LoginComponent>;

  constructor(private http: HttpClient, private matDialog: MatDialog, private router: Router, private snackBar: MatSnackBar) {

  }

  setDialogRef(dialogRef: MatDialogRef<LoginComponent>) {
    this.dialogRef = dialogRef;
  }

  userAuthentication(username, password) {
    this.http.post(this.loginApi, {username, password}).subscribe(e => this.loginSuccessful(e),
      error => this.matDialog.open(ConfirmationDialogComponent, {data: {message: error.message, type: 'error'}}));
  }

  userRegistration(username, password, email) {
    this.http.post(this.registerApi, {username, password, email}).subscribe(() => this.registerSuccessful(),
        error => this.matDialog.open(ConfirmationDialogComponent, {data: {message: error.message, type: 'error'}}));
  }

  loginSuccessful(e) {
    localStorage.setItem('lms_token', e.token);
    if (this.dialogRef) {
      this.dialogRef.close();
    }


    // provera prve role zbog navikacije na odredjeni portal
    let userRoles = [];
    userRoles = this.getUserRoles();
    if (userRoles.length > 0) {
      const role = userRoles[0].authority;
      if (role === 'ROLE_ADMIN') {
        this.router.navigate(['/admin']);
      } else if (role === 'ROLE_STUDENT') {
        this.router.navigate(['/student']);
      } else if (role === 'ROLE_PROFESSOR') {
        this.router.navigate(['/professor']);
      } else if (role === 'ROLE_ADMINISTRATION') {
        this.router.navigate(['/administration']);
      }else if (role === 'ROLE_USER') {
        this.router.navigate(['/user']);
      }else {
        this.router.navigate(['/home']);
        }
    }



  }

  registerSuccessful() {
    this.snackBar.open('Registration successful', 'Close', {
      duration: 5000
    });
    this.router.navigate(['/login']);
  }

  getToken(): string {
    return localStorage.getItem('lms_token');
  }

  isTokenValid(): boolean {
    return this.getToken() && !this.isTokenExpired(this.getToken());
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) { return null; }

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if (!token) { token = this.getToken(); }
    if (!token) { return true; }

    const date = this.getTokenExpirationDate(token);
    if (date === undefined) { return false; }
    return !(date.valueOf() > new Date().valueOf());
  }

  logout() {
    localStorage.removeItem('lms_token');
    this.router.navigate(['/']);
  }

  getUserRoles(): [] {
    const token = jwt_decode(localStorage.getItem('lms_token'));
    if (token.roles.length > 0) {
      return token.roles;
    }
    return [];
  }

  isRole(customRole: string): boolean {

    // const token = jwt_decode(this.getToken());
    // for (const role of token.roles) {
    //   if (role.authority === customRole) {
    //     return true;
    //   }
    // }
    return false;
  }

  isAdmin(): boolean {
    const token = jwt_decode(this.getToken());
    for (const role of token.roles) {
      if (role.authority === 'ROLE_ADMIN') {
        return true;
      }
    }
    return false;
  }
  isUser(): boolean {
    const token = jwt_decode(this.getToken());
    for (const role of token.roles) {
      if (role.authority === 'ROLE_USER') {
        return true;
      }
    }
    return false;
  }

  isProfessor(): boolean {
    const token = jwt_decode(this.getToken());
    for (const role of token.roles) {
      if (role.authority === 'ROLE_PROFESSOR') {
        return true;
      }
    }
    return false;
  }

  isAdministration(): boolean {
    const token = jwt_decode(this.getToken());
    for (const role of token.roles) {
      if (role.authority === 'ROLE_ADMINISTRATION') {
        return true;
      }
    }
    return false;
  }

  isStudent(): boolean {
    const token = jwt_decode(this.getToken());
    for (const role of token.roles) {
      if (role.authority === 'ROLE_STUDENT') {
        return true;
      }
    }
    return false;
  }


}
