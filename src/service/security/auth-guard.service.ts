import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot, RoutesRecognized, UrlTree
} from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanActivateChild {

  private allowedRoles: string[];

  constructor(private router: Router,  private authenticationService: AuthenticationService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checkIsAllowed(route);
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // ukoliko ne nadje data u child ruti uzima data objekat od roditelja
    // if (childRoute.data) {
    //   childRoute.data = childRoute.parent.data;
    // }

    let parent = childRoute;
    let allowedRoles = undefined;
    while (parent) {
      allowedRoles = parent.data.allowedRoles;
      if (allowedRoles) {
        break;
      }
      parent = parent.parent;
    }

    return this.checkIsAllowed(parent);
  }



  private checkIsAllowed(route): boolean {

    this.allowedRoles = route.data.allowedRoles;

    if (this.authenticationService.isTokenValid()) {

      for (let userRole of this.authenticationService.getUserRoles()) {
        if (this.allowedRoles.includes(userRole['authority'])) {
          return true;
        }
      }
    }
    // if (this.authenticationService.isTokenValid()) {
    //
    //   for (const role of route.data.allowedRoles) {
    //     if (this.authenticationService.isRole(role)) {
    //       return true;
    //     }
    //   }
    // }

    this.router.navigate(['/login']);
    return false;
  }

}
