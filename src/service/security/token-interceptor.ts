import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {AuthenticationService} from "./authentication.service";



@Injectable()
export class TokenInterceptor implements HttpInterceptor{

  constructor(private authenticationService:AuthenticationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let token = this.authenticationService.getToken()

    if(this.authenticationService.isTokenValid()){

      req = req.clone({
        setHeaders: {
          Authorization: ''+ token
        }
      });
    }

    return next.handle(req);

  }



}
