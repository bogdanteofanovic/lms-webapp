import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { Role } from 'src/model/role/role';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoleService extends GenericService<Role>{

  constructor(httpClient: HttpClient) { super(httpClient, "role"); }
}
