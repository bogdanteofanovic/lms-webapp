import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { Faculty } from 'src/model/faculty/faculty';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FacultyService extends GenericService<Faculty>{

  constructor(httpClient: HttpClient) { super(httpClient, "faculty"); } // "faculty" name of the route | "faculties" not important, will be deleted later
}
