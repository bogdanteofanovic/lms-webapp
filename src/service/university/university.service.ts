import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { University } from 'src/model/university/university';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UniversityService extends GenericService<University>{

  constructor(httpClient: HttpClient) { super(httpClient, "university") }
}
