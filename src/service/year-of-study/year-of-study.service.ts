import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { HttpClient } from '@angular/common/http';
import { YearOfStudy } from 'src/model/year-of-study/year-of-study';

@Injectable({
  providedIn: 'root'
})
export class YearOfStudyService extends GenericService<YearOfStudy> {

  constructor(httpClient: HttpClient) {
    super(httpClient, "yearOfStudy");
   }
}
