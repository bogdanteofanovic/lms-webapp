import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { ClassType } from 'src/model/class-type/class-type';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClassTypeService extends GenericService<ClassType>{

  constructor(httpClient: HttpClient) { super(httpClient, "classType"); }
}
