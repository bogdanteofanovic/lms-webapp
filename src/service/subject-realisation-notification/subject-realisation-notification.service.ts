import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { SubjectRealisationNotification } from 'src/model/subject-realisation-notification/subject-realisation-notification';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubjectRealisationNotificationService extends GenericService<SubjectRealisationNotification>{

  constructor(httpClient: HttpClient) { super(httpClient, "subjectRealisationNotification"); }
}
