import { TestBed } from '@angular/core/testing';

import { SubjectRealisationNotificationService } from './subject-realisation-notification.service';

describe('SubjectRealisationNotificationService', () => {
  let service: SubjectRealisationNotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubjectRealisationNotificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
