import { Injectable } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {EntranceExam} from '../../model/entrance-exam/entrance-exam';
import {GenericService} from '../generic-service/generic-service';

@Injectable({
  providedIn: 'root'
})
export class EntranceExamService extends GenericService<EntranceExam>{

  constructor(httpClient: HttpClient) {
        super(httpClient, "entranceExam");

  }
}
