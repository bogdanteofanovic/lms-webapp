import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { UserRole } from 'src/model/user-role/user-role';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserRoleService extends GenericService<UserRole>{

  constructor(httpClient: HttpClient) { super(httpClient, "user_role"); }
}
