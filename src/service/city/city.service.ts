import { Injectable } from '@angular/core';
import {GenericService} from '../generic-service/generic-service';
import {HttpClient} from '@angular/common/http';
import { City } from 'src/model/city/city';

@Injectable({
  providedIn: 'root'
})
export class CityService extends GenericService<City>{

  constructor(httpClient: HttpClient) { super(httpClient, "city"); } // "city" : Name of route | "cities" : Unimportant, will be deleted later
}
