import { TestBed } from '@angular/core/testing';

import { SubjectExaminationDetailsService } from './subject-examination-details.service';

describe('SubjectExaminationDetailsService', () => {
  let service: SubjectExaminationDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubjectExaminationDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
