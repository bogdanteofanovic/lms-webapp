import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { SubjectExamination } from 'src/model/subject-examination/subject-examination';
import { HttpClient } from '@angular/common/http';
import { SubjectExaminationDetails } from 'src/model/subject-examination-details/subject-examination-details';

@Injectable({
  providedIn: 'root'
})
export class SubjectExaminationDetailsService extends GenericService<SubjectExaminationDetails>{

  constructor(httpClient: HttpClient) { super(httpClient, "subjectExaminationDetails"); }
}
