import { TestBed } from '@angular/core/testing';

import { FsFileService } from './fs-file.service';

describe('FsFileService', () => {
  let service: FsFileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FsFileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
