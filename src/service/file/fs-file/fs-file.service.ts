import { Injectable } from '@angular/core';
import {FsFile} from '../../../model/file/fs-file/fs-file';
import {HttpClient} from '@angular/common/http';
import {GenericService} from '../../generic-service/generic-service';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FsFileService extends GenericService<FsFile> {

  constructor(httpClient: HttpClient) {
    super(httpClient, 'FSfile');
  }

}
