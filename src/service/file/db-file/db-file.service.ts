import { Injectable } from '@angular/core';
import {DbFile} from '../../../model/file/db-file/db-file';
import {HttpClient} from '@angular/common/http';
import {GenericService} from '../../generic-service/generic-service';

@Injectable({
  providedIn: 'root'
})
export class DbFileService extends GenericService<DbFile> {

  constructor(httpClient: HttpClient) {
    super(httpClient, 'DBfile');
  }
}
