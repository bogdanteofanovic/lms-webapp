import { TestBed } from '@angular/core/testing';

import { DbFileService } from './db-file.service';

describe('DbFileService', () => {
  let service: DbFileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DbFileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
