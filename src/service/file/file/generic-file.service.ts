import {AbstractFile} from '../../../model/file/abstract-file/abstract-file';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';


export abstract class GenericFileService<T extends AbstractFile> {

  apiUrl: string = 'http://localhost:8080/api';
  entityName: string;

  httpClient: HttpClient;

  constructor(private http: HttpClient, entityName) {

    this.httpClient = http;
    this.entityName = entityName;

  }
    // get all where subject or professor_portal??
  // getAll(): Observable<T[]> {
  //   return this.http.get<T[]>(this.apiUrl + '/' + this.entityName);
  // }

  getOne(id: number): Observable<T> {
    return this.http.get<T>(this.apiUrl + '/' + this.entityName + '/' + id);
  }

  addOne(multipartFile: FormData, employeeid: number): Observable<T> {
    return this.http.post<T>(this.apiUrl + '/' + this.entityName + '/uploadFile/' + employeeid, multipartFile);
  }

  addMultiple(multipartFiles: FormData[], employeeid: number): Observable<T> {
    return this.http.post<T>(this.apiUrl + '/' + this.entityName + '/uploadFiles/' + employeeid, multipartFiles);
  }

  updateOne(id: number, multipartFile: FormData, employeeid: number): Observable<T> {
    return this.http.put<T>(this.apiUrl + '/' + this.entityName + '/' + id + '/' +  + employeeid, multipartFile);
  }

  deleteOne(id: number): Observable<T> {
    return this.http.delete<T>(this.apiUrl + '/' + this.entityName + '/' + id);
  }

}
