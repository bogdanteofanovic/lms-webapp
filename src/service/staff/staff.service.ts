import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { StaffType } from 'src/model/staff-type/staff-type';
import { Staff } from 'src/model/staff/staff';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StaffService extends GenericService<Staff>{

  constructor(httpClient: HttpClient) { super(httpClient, "staff"); }
}
