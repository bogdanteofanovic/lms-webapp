import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { EmployeeType } from 'src/model/employee-type/employee-type';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeTypeService extends GenericService<EmployeeType>{

  constructor(httpClient: HttpClient) {
    super(httpClient, "employeeType");
   }
}
