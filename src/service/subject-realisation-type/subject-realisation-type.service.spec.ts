import { TestBed } from '@angular/core/testing';

import { SubjectRealisationTypeService } from './subject-realisation-type.service';

describe('SubjectRealisationTypeService', () => {
  let service: SubjectRealisationTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubjectRealisationTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
