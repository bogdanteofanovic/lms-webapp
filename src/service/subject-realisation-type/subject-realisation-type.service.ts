import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { SubjectRealisationType } from 'src/model/subject-realisation-type/subject-realisation-type';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubjectRealisationTypeService extends GenericService<SubjectRealisationType>{

  constructor(httpClient: HttpClient) { super(httpClient, "subjectRealisationType"); }
}
