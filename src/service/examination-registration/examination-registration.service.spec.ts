import { TestBed } from '@angular/core/testing';

import { ExaminationRegistrationService } from './examination-registration.service';

describe('ExaminationRegistrationService', () => {
  let service: ExaminationRegistrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExaminationRegistrationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
