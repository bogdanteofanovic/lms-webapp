import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { ExaminationRegistration } from 'src/model/examination-registration/examination-registration';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExaminationRegistrationService extends GenericService<ExaminationRegistration>{

  constructor(httpClient: HttpClient) { super(httpClient, "examinationRegistration"); }
}
