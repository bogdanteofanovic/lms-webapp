import { Injectable } from '@angular/core';
import {GenericService} from "../generic-service/generic-service";
import {HttpClient} from "@angular/common/http";
import { Student } from 'src/model/student/student';

@Injectable({
  providedIn: 'root'
})
export class StudentService extends GenericService<Student>{

  constructor(http:HttpClient) {
    super(http, "student");
  }
}
