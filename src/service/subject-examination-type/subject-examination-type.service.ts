import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { SubjectExaminationType } from 'src/model/subject-examination-type/subject-examination-type';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubjectExaminationTypeService extends GenericService<SubjectExaminationType>{

  constructor(httpClient: HttpClient) { super(httpClient, "subjectExaminationType"); }
}
