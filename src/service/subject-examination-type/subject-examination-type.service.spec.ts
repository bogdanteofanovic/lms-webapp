import { TestBed } from '@angular/core/testing';

import { SubjectExaminationTypeService } from './subject-examination-type.service';

describe('SubjectExaminationTypeService', () => {
  let service: SubjectExaminationTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubjectExaminationTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
