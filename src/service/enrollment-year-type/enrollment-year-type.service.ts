import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { EnrollmentYearType } from 'src/model/enrollment-year-type/enrollment-year-type';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EnrollmentYearTypeService extends GenericService<EnrollmentYearType>{

  constructor(httpClient: HttpClient) { super(httpClient, "enrollmentYearType"); }
}
