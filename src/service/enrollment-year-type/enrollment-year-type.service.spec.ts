import { TestBed } from '@angular/core/testing';

import { EnrollmentYearTypeService } from './enrollment-year-type.service';

describe('EnrollmentYearTypeService', () => {
  let service: EnrollmentYearTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnrollmentYearTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
