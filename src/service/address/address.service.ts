import { Injectable } from '@angular/core';
import {GenericService} from "../generic-service/generic-service";

import {HttpClient} from "@angular/common/http";
import { Address } from 'src/model/adress/address';


@Injectable({
  providedIn: 'root'
})
export class AddressService extends GenericService<Address>{

  constructor(http:HttpClient) { super(http, "address")};
}
