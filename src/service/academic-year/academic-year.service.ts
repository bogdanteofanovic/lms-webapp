import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { AcademicYear } from 'src/model/academic-year/academic-year';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AcademicYearService extends GenericService<AcademicYear>{

  constructor(httpClient: HttpClient) { super(httpClient, "academicYear"); }
}
