import { TestBed } from '@angular/core/testing';

import { ClassroomTypeService } from './classroom-type.service';

describe('ClassroomTypeService', () => {
  let service: ClassroomTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClassroomTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
