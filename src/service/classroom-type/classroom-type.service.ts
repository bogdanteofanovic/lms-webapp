import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { ClassroomType } from 'src/model/classroom-type/classroom-type';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClassroomTypeService extends GenericService<ClassroomType>{

  constructor(httpClient: HttpClient) { super(httpClient, "classroomType"); }
}
