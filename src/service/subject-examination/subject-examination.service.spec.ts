import { TestBed } from '@angular/core/testing';

import { SubjectExaminationService } from './subject-examination.service';

describe('SubjectExaminationService', () => {
  let service: SubjectExaminationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubjectExaminationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
