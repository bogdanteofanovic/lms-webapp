import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { SubjectExamination } from 'src/model/subject-examination/subject-examination';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubjectExaminationService extends GenericService<SubjectExamination>{

  constructor(httpClient: HttpClient) { super(httpClient, "subjectExamination"); }
}
