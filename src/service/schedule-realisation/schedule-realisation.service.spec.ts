import { TestBed } from '@angular/core/testing';

import { ScheduleRealisationService } from './schedule-realisation.service';

describe('ScheduleRealisationService', () => {
  let service: ScheduleRealisationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScheduleRealisationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
