import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { ScheduleRealisation } from 'src/model/schedule-realisation/schedule-realisation';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ScheduleRealisationService extends GenericService<ScheduleRealisation>{

  constructor(httpClient: HttpClient) { super(httpClient, "scheduleRealisation"); }
}
