import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { Employee } from 'src/model/employee/employee';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService extends GenericService<Employee> {

  constructor(httpClient: HttpClient) {
    super(httpClient, "employee");
   }
}
