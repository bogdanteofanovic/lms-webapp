import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { StaffType } from 'src/model/staff-type/staff-type';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StaffTypeService extends GenericService<StaffType>{

  constructor(httpClient: HttpClient) { super(httpClient, "staffType"); }
}
