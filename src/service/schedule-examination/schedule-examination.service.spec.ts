import { TestBed } from '@angular/core/testing';

import { ScheduleExaminationService } from './schedule-examination.service';

describe('ScheduleExaminationService', () => {
  let service: ScheduleExaminationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScheduleExaminationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
