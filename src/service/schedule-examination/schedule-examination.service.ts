import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { ScheduleExamination } from 'src/model/schedule-examination/schedule-examination';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ScheduleExaminationService extends GenericService<ScheduleExamination>{

  constructor(httpClient: HttpClient) { super(httpClient, "scheduleExamination"); }
}
