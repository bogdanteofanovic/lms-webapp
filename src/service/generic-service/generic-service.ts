
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injector} from '@angular/core';
import { AbstractEntity } from 'src/model/abstract-entity/abstract-entity';


export abstract class GenericService<T extends AbstractEntity> {

  apiUrl : string = 'http://localhost:8080/api';
  entityName: string;

  httpClient: HttpClient;

  constructor(private http: HttpClient, entityName) {

    this.httpClient = http;
    this.entityName = entityName;

  }
  getAll(): Observable<T[]> {
    return this.http.get<T[]>(this.apiUrl + '/' + this.entityName);
  }

  getAllNotDeleted(): Observable<T[]> {
    return this.http.get<T[]>(this.apiUrl + '/' + this.entityName + '/notDeleted');
  }

  getOne(id:number): Observable<T> {
    return this.http.get<T>(this.apiUrl + '/' + this.entityName + '/' + id);
  }

  addOne(entity: T | FormData): Observable<T | FormData> {
    return this.http.post<T | FormData>(this.apiUrl + '/' + this.entityName , entity);
  }

  updateOne(id : number, entity: T | FormData): Observable<T | FormData> {
    return this.http.put<T | FormData>(this.apiUrl + '/' + this.entityName + '/' + id, entity);
  }

  deleteOne(id: number): Observable<T> {
    return this.http.delete<T>(this.apiUrl + '/' + this.entityName + '/' + id);
  }

  restoreOne(id: number): Observable<T> {
    console.log("restore")
    return this.http.put<T>(this.apiUrl + '/' + this.entityName + '/restore/' + id, {});
  }

  physicalDeleteOne(id: number): Observable<T> {
    return this.http.delete<T>(this.apiUrl + '/' + this.entityName + '/delete/' + id);
  }

}
