import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { Subject } from 'src/model/subject/subject';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubjectService extends GenericService<Subject>{

  constructor(httpClient: HttpClient) { super(httpClient, "subject") }
}
