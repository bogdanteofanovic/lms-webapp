import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { EnrolledSubject } from 'src/model/enrolled-subject/enrolled-subject';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EnrolledSubjectService extends GenericService<EnrolledSubject>{

  constructor(httpClient: HttpClient) { super(httpClient, "enrolledSubject"); }
}
