import { TestBed } from '@angular/core/testing';

import { EnrolledSubjectService } from './enrolled-subject.service';

describe('EnrolledSubjectService', () => {
  let service: EnrolledSubjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnrolledSubjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
