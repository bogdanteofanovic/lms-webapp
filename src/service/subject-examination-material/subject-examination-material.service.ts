import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { SubjectExaminationMaterial } from 'src/model/subject-examination-material/subject-examination-material';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs";
import {FsFile} from "../../model/file/fs-file/fs-file";
import {SubjectRealisationMaterial} from "../../model/subject-realisation-material/subject-realisation-material";

@Injectable({
  providedIn: 'root'
})
export class SubjectExaminationMaterialService extends GenericService<SubjectExaminationMaterial>{

  constructor(httpClient: HttpClient) { super(httpClient, 'subjectExaminationMaterial'); }

  getForExaminationDetails(id: number): Observable<SubjectExaminationMaterial[]> {
    return this.httpClient.get<SubjectExaminationMaterial[]>(this.apiUrl + '/' + this.entityName + '/subjectExaminationDetails/' + id);
  }
}
