import { TestBed } from '@angular/core/testing';

import { SubjectExaminationMaterialService } from './subject-examination-material.service';

describe('SubjectExaminationMaterialService', () => {
  let service: SubjectExaminationMaterialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubjectExaminationMaterialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
