import { Injectable } from '@angular/core';
import {GenericService} from '../generic-service/generic-service';

import {HttpClient} from '@angular/common/http';
import { Country } from 'src/model/country/country';

@Injectable({
  providedIn: 'root'
})
export class CountryService extends GenericService<Country>{

  constructor(httpClient: HttpClient) { super(httpClient, "country"); }
}
