import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { Classroom } from 'src/model/classroom/classroom';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClassroomService extends GenericService<Classroom>{

  constructor(httpClient: HttpClient) { super(httpClient, "classroom"); }
}
