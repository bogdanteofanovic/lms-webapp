import { Injectable } from '@angular/core';
import { StudyProgram } from 'src/model/study-program/study-program';
import { GenericService } from '../generic-service/generic-service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StudyProgramService extends GenericService<StudyProgram>{

  constructor(httpClient: HttpClient) {
    super(httpClient, "studyProgram");
   }
}
