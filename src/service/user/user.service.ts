import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import { GenericService } from '../generic-service/generic-service';
import {User} from '../../model/user/user';


@Injectable({
  providedIn: 'root'
})
export class UserService extends GenericService<User>{

  constructor(http:HttpClient) {
    super(http, "user");
  }


  findFreeUsers():Observable<User[]>{
    return this.httpClient.get<User[]>(this.apiUrl+'/'+this.entityName + '/free');
  }

  getByLoggedInUser():Observable<User>{
    return this.httpClient.get<User>(this.apiUrl+'/'+this.entityName + '/getByLoggedInUser');
  }

  changeUserPassword(user:User):Observable<User>{
    return this.httpClient.put<User>(this.apiUrl+'/'+this.entityName + '/changeLoggedInPassword', user);
  }

  changeUserEmail(user:User):Observable<User>{
    return this.httpClient.put<User>(this.apiUrl+'/'+this.entityName + '/changeLoggedInEmail', user);
  }

}
