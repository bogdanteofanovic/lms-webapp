import {HostListener, Injectable} from '@angular/core';
import {Refresh} from "../view/refresh";
import {KeyEventBus} from "../model/key-event-bus";

@Injectable({
  providedIn: 'root'
})
export class KeyEventBusService {

  protected observers: KeyEventBus[] = [];


  constructor() {
  }


  attach = (o:KeyEventBus): void => {
    this.observers.push(o);
  }

  dettach = (e: KeyEventBus): void => {
    this.observers = this.observers.filter(o => o !== e);
  }

  notify = (e): void => {
    this.observers.forEach(observer => {
      observer.onKeyEvent(e);
    });
  }
}
