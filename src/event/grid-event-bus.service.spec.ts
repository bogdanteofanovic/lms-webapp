import { TestBed } from '@angular/core/testing';

import { GridEventBusService } from './grid-event-bus.service';

describe('GridEventBusService', () => {
  let service: GridEventBusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GridEventBusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
