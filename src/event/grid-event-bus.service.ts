import { Injectable } from '@angular/core';
import {webSocket} from "rxjs/webSocket";
import {Refresh} from "../view/refresh";

export interface Observer {
  name:string;
  entity: Refresh;
}

@Injectable({
  providedIn: 'root'
})

export class GridEventBusService{

  protected observers: Observer[] = [];
  private ws;
  private observer: Observer;

  constructor() {
    this.ws = webSocket("ws://localhost:8080/gridEvents");
    this.ws.subscribe((msg)=>this.reciveNotification(msg), (err) => console.log(err), (finish) => console.log(finish));
  }


  attach = (observer:Observer): void => {
    this.observers.push(observer);
  }

  dettach = (e: Observer): void => {
    this.observers = this.observers.filter(o => o !== e);
  }

  notify = (entity): void => {
    this.ws.next({msg:entity})

  }

  reciveNotification(msg:any){
    this.observers.forEach(observer => {
      if(msg.msg == observer.name)
        observer.entity.refreshGrid();
    });
  }


}
