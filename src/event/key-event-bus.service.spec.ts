import { TestBed } from '@angular/core/testing';

import { KeyEventBusService } from './key-event-bus.service';

describe('KeyEventBusService', () => {
  let service: KeyEventBusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KeyEventBusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
