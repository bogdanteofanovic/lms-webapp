import {Refresh} from '../view/refresh';

export class Observer {

  protected observers: Refresh[] = [];

  attach = (o: Refresh): void => {
    this.observers.push(o);
  }

  dettach = (e: Refresh): void => {
    this.observers = this.observers.filter(o => o !== e);
  }

  notify = (): void => {
    this.observers.forEach(observer => {
      observer.refreshGrid();
    });
  }


}
