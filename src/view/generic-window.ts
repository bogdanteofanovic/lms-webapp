import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output} from '@angular/core';
import {AbstractEntity} from '../model/abstract-entity/abstract-entity';
import {GenericService} from '../service/generic-service/generic-service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {GridEventBusService} from '../event/grid-event-bus.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Country} from "../model/country/country";
import {MatSnackBar} from "@angular/material/snack-bar";

export abstract class GenericWindow<T extends AbstractEntity, V extends GenericService<T>> implements OnInit{

  entity:T;

  emptyEntity: T;

  service: V;

  add:boolean;

  data:any;

  dialogRef:MatDialogRef<GenericWindow<T, V>>;

  gridEventBusService:GridEventBusService;

  formGroup:FormGroup;

  titleAlert: string = 'This field is required';

  snackBar: MatSnackBar;

  enableCbAdd: boolean = false;
  enableCbEdit: boolean = false;
  disabledCb: boolean = false;

  constructor(service:V, @Optional() @Inject(MAT_DIALOG_DATA) data: T, dialogRef: MatDialogRef<GenericWindow<T, V>>,
              gridEventBusService:GridEventBusService, matSnackBar:MatSnackBar, emptyEntity: T) {

    this.service = service;
    this.entity = data;
    this.dialogRef = dialogRef;
    this.snackBar = matSnackBar;
    this.gridEventBusService = gridEventBusService;
    this.emptyEntity = emptyEntity;
  }

  ngOnInit(): void {


    this.createForm();

    if(!this.entity){
      this.add = true;
      this.entity = this.emptyEntity;
    }else {
      this.add = false;
    }

    this.init();

    this.initFormFields();

  }

  abstract init();

  submitForm():void{
    console.log(this.entity)
    this.bindBeforeSending();

    if(this.add){
      this.service.addOne(this.entity).subscribe(e=> this.successSubmit(e), error => this.errorSubmit(error));

    }
    else {
      this.service.updateOne(this.entity.id, this.entity).subscribe(e=>this.successSubmit(e), error => this.errorSubmit(error));

    }

  }

  protected errorSubmit(error){
    this.dialogRef.close({data:null});
    this.gridEventBusService.notify(this.service.entityName);
    this.snackBar.open(error.message, 'close',{
      duration: 2000,
    });
  }

  protected successSubmit(entity){
    this.gridEventBusService.notify(this.service.entityName);
    this.dialogRef.close({data:entity});
  }


  abstract createForm(): void;


  //bind values from form to object
  protected bindBeforeSending(){

    for (const attr of Object.keys(this.emptyEntity)){

      if (attr !== 'id' && attr !== 'version' && attr !== 'deleted'){
        this.entity[attr] = this.formGroup.get(attr).value;
      }

    }

  }

  //bind values from object to form
  initFormFields(){
    console.log(this.formGroup);
    for(const attr of Object.keys(this.emptyEntity)){
      if(attr !== 'id' && attr !== 'version' && attr !== 'deleted') {
        this.formGroup.get(attr).setValue(this.entity[attr]);
      }

    }

  }

}
