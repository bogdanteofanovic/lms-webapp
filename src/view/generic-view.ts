import {AbstractEntity} from '../model/abstract-entity/abstract-entity';
import {GenericService} from '../service/generic-service/generic-service';
import {OnDestroy, OnInit, Optional, TemplateRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ComponentType} from '@angular/cdk/overlay';
import {GenericWindow} from './generic-window';
import {GridEventBusService} from '../event/grid-event-bus.service';
import {Refresh} from './refresh';
import {MatTableDataSource} from '@angular/material/table';
import {ColumnDef} from '../model/column-def';
import {ConfirmationDialogComponent} from '../app/utils/confirmation-dialog/confirmation-dialog.component';
import {KeyEventBus} from "../model/key-event-bus";
import {KeyEventBusService} from "../event/key-event-bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {City} from "../model/city/city";
import {Address} from "../model/adress/address";
import {GenericTableComponent} from "../app/utils/generic-table/generic-table.component";
import {AuthenticationService} from '../service/security/authentication.service';
import * as jwt_decode from 'jwt-decode';

export abstract class GenericView<T extends AbstractEntity, V extends GenericService<T>> implements OnInit, OnDestroy, Refresh, KeyEventBus {

  service: V;

  entities: T[];

  entity: T;

  dialogRef: MatDialogRef<GenericWindow<T, V>>;

  dialog: MatDialog;

  editWindow: ComponentType<GenericWindow<T, V>>;

  columnDefinitions: ColumnDef[];

  gridEventBusService: GridEventBusService;

  dataSource = new MatTableDataSource();

  keyEventBusService: KeyEventBusService;

  matSnackBar: MatSnackBar;

  actionNew:boolean = false;
  actionEdit:boolean = false;
  actionDelete:boolean = false;
  actionExport:boolean = false;
  actionRestore:boolean = false;


  protected constructor(service: V, editWindow: ComponentType<GenericWindow<T, V>>, dialog: MatDialog,
                        gridEventBusService: GridEventBusService, keyEventBusService: KeyEventBusService, snackBar: MatSnackBar) {

    this.service = service;
    this.editWindow = editWindow;
    this.dialog = dialog;
    this.gridEventBusService = gridEventBusService;
    this.keyEventBusService = keyEventBusService;
    this.matSnackBar = snackBar;


  }

  ngOnInit(): void {



    this.keyEventBusService.attach(this);

    this.gridEventBusService.attach({name:this.service.entityName, entity:this});

    this.columnDefinitions = this.initTableColumns();

    this.doFirst();

    this.getAll();

    this.doLast();

    if(this.isAdmin()){
      this.columnDefinitions.push({name: 'deleted', title: 'Deleted'});
    }

  }


  //do first in onInit method
  abstract doFirst(): void;


  //do last in onInit method
  abstract doLast(): void;

  abstract initTableColumns(): ColumnDef [];


  enableNew(){
    this.actionNew = true;
  }
  enableEdit(){
    this.actionEdit = true;
  }
  enableDelete(){
    this.actionDelete = true;
  }
  enableExport(){
    this.actionExport = true;
  }
  enableRestore(){
    this.actionRestore = true;
  }

  isAdmin(): boolean {
    const token = jwt_decode(localStorage.getItem('lms_token'));
    for (const role of token.roles) {
      if (role.authority === 'ROLE_ADMIN') {
        return true;
      }
    }
    return false;
  }

  getAll(): void {



    if(this.isAdmin()){
     this.service.getAll().subscribe(entities => this.dataSource.data = entities);
    }else{
      this.service.getAllNotDeleted().subscribe(entities => this.dataSource.data = entities);
    }


  }

  editEntity(selected: T) {

    this.entity = selected;
    this.dialogRef = this.dialog.open(this.editWindow, {data: this.entity});
    this.dialogRef.afterClosed().subscribe(() => this.afterEditClose());

  }

  afterEditClose(){
    this.gridEventBusService.notify(this.service.entityName);
  }

  selectEntity(selected: T) {

    this.entity = selected;

  }

  addNew() {

    this.dialogRef = this.dialog.open(this.editWindow);

  }

  deleteEntity(entity: T) {
    if(entity){
      this.entity = entity;
    }
    if(this.isAdmin() && entity.deleted){
      this.dialog.open(ConfirmationDialogComponent, {disableClose: true, data: {message: 'Physical delete entity! Are you sure?'}})
        .afterClosed().subscribe(e => e.data === 'ok' ? this.physicalDelete() : console.log(e));
    }else{
      this.dialog.open(ConfirmationDialogComponent, {disableClose: true, data: {message: 'Delete entity! Are you sure?'}})
        .afterClosed().subscribe(e => e.data === 'ok' ? this.delete() : console.log(e));
    }


  }

  physicalDelete() {
    this.service.physicalDeleteOne(this.entity.id).subscribe(() => this.gridEventBusService.notify(this.service.entityName),
      error => this.dialog.open(ConfirmationDialogComponent, {data: {message: error.message, type: 'error'}}));
    this.entity = null;
  }


  delete() {
    this.service.deleteOne(this.entity.id).subscribe(() => this.gridEventBusService.notify(this.service.entityName),
      error => this.dialog.open(ConfirmationDialogComponent, {data: {message: error.message, type: 'error'}}));
    this.entity = null;
  }

  restoreEntity(entity: T) {
    if(entity){
      this.entity = entity;
    }
    this.dialog.open(ConfirmationDialogComponent, {disableClose: true, data: {message: 'Restore entity! Are you sure?'}})
      .afterClosed().subscribe(e => e.data === 'ok' ? this.restore() : console.log(e));

  }

  restore() {
    this.service.restoreOne(this.entity.id).subscribe(() => this.gridEventBusService.notify(this.service.entityName),
      error => this.dialog.open(ConfirmationDialogComponent, {data: {message: error.message, type: 'error'}}));
    this.entity = null;
  }

  ngOnDestroy(): void {

    this.gridEventBusService.dettach({name:this.service.entityName, entity:this});
    this.keyEventBusService.dettach(this);

  }

  refreshGrid(): void {

    this.getAll();

  }

  onKeyEvent(event: KeyboardEvent) {

    if (event.altKey && event.key.toLocaleLowerCase() == 'n') {
      this.addNew();
    }

    if (event.key.toLocaleLowerCase() == 'delete') {
      try{
        if (this.entity.id != 0) {
          this.deleteEntity(this.entity);
        }
      }catch (e) {
        this.matSnackBar.open(e, null, {
          duration: 2000,
        });
      }
    }
  }



}
